<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4b5114aa-4529-41b5-bcac-afa5092e0caa(Playground.BankAccount)">
  <persistence version="9" />
  <languages>
    <devkit ref="2a15b00e-495f-4bce-aabb-bf2bffd958aa(MetaJade.devkit.java)" />
  </languages>
  <imports />
  <registry>
    <language id="2f7e2e35-6e74-4c43-9fa5-2465d68f5996" name="org.iets3.core.expr.collections">
      <concept id="2156530943179783739" name="org.iets3.core.expr.collections.structure.ListWithOp" flags="ng" index="2iGZtc" />
      <concept id="890442848561696122" name="org.iets3.core.expr.collections.structure.ListWithoutOp" flags="ng" index="2t5v1R" />
      <concept id="8694548031077039769" name="org.iets3.core.expr.collections.structure.ElementTypeConstraintSingle" flags="ng" index="ygwf7">
        <child id="8694548031077039770" name="typeConstraint" index="ygwf4" />
      </concept>
      <concept id="5291952221900249273" name="org.iets3.core.expr.collections.structure.IListOneArgOp" flags="ng" index="1bLd8V">
        <child id="527291771311128762" name="arg" index="26Ft6C" />
      </concept>
      <concept id="7554398283340715406" name="org.iets3.core.expr.collections.structure.WhereOp" flags="ng" index="3izCyS" />
      <concept id="7554398283340020764" name="org.iets3.core.expr.collections.structure.OneArgCollectionOp" flags="ng" index="3iAY4E">
        <child id="7554398283340020765" name="arg" index="3iAY4F" />
      </concept>
      <concept id="7554398283339853806" name="org.iets3.core.expr.collections.structure.LastOp" flags="ng" index="3iB7bo" />
      <concept id="7554398283339796915" name="org.iets3.core.expr.collections.structure.SizeOp" flags="ng" index="3iB8M5" />
      <concept id="7554398283339749509" name="org.iets3.core.expr.collections.structure.CollectionType" flags="ng" index="3iBWmN">
        <child id="7554398283339749510" name="baseType" index="3iBWmK" />
      </concept>
      <concept id="7554398283339759319" name="org.iets3.core.expr.collections.structure.ListLiteral" flags="ng" index="3iBYfx">
        <child id="8694548031077041593" name="typeConstraint" index="ygBzB" />
      </concept>
      <concept id="7554398283339757344" name="org.iets3.core.expr.collections.structure.ListType" flags="ng" index="3iBYCm" />
      <concept id="24388123216554083" name="org.iets3.core.expr.collections.structure.FindFirstOp" flags="ng" index="1HmgMX" />
    </language>
    <language id="390edf25-bfa2-48c0-b67b-fc8fbd32bfbc" name="MetaJade.core">
      <concept id="6398777375045966262" name="MetaJade.core.structure.Stop" flags="ng" index="2cDHYC" />
      <concept id="943645189038563415" name="MetaJade.core.structure.IDefinePerformative" flags="ng" index="2NJI0r">
        <property id="943645189038563416" name="performative" index="2NJI0k" />
      </concept>
      <concept id="2457439430822670749" name="MetaJade.core.structure.Module" flags="ng" index="198UmT" />
      <concept id="2569313154175320625" name="MetaJade.core.structure.IChunkDep" flags="ng" index="1cWWE2">
        <reference id="39011061268502169" name="chunk" index="21GGV3" />
      </concept>
      <concept id="2253134785231795" name="MetaJade.core.structure.UnitType" flags="ng" index="3iyjE5" />
      <concept id="2460144669873447797" name="MetaJade.core.structure.Log" flags="ng" index="3lL9fZ">
        <child id="2460144669875212724" name="value" index="3lRU4Y" />
      </concept>
      <concept id="8295235836482040281" name="MetaJade.core.structure.AbstractChunk" flags="ng" index="1yxazi">
        <child id="2457439430830142521" name="imports" index="19Gq0t" />
      </concept>
      <concept id="4428678242934220321" name="MetaJade.core.structure.ITopLevelExprContainer" flags="ng" index="3FVrl4">
        <child id="1741182739292157392" name="contents" index="3KDhAD" />
      </concept>
      <concept id="1741182739291547872" name="MetaJade.core.structure.ModuleDependency" flags="ng" index="3KE$ip" />
      <concept id="4316580858990611837" name="MetaJade.core.structure.AIDType" flags="ng" index="3NxlSm" />
    </language>
    <language id="7b68d745-a7b8-48b9-bd9c-05c0f8725a35" name="org.iets3.core.base">
      <concept id="7831630342157089621" name="org.iets3.core.base.structure.IDetectNeedToRunManually" flags="ng" index="0Rz4o">
        <property id="7831630342157089649" name="__hash" index="0Rz4W" />
      </concept>
      <concept id="229512757698888199" name="org.iets3.core.base.structure.IOptionallyNamed" flags="ng" index="pfQq$">
        <child id="229512757698888936" name="optionalName" index="pfQ1b" />
      </concept>
      <concept id="229512757698888202" name="org.iets3.core.base.structure.OptionalNameSpecifier" flags="ng" index="pfQqD">
        <property id="229512757698888203" name="optionalName" index="pfQqC" />
      </concept>
    </language>
    <language id="cfaa4966-b7d5-4b69-b66a-309a6e1a7290" name="org.iets3.core.expr.base">
      <concept id="7971844778466793051" name="org.iets3.core.expr.base.structure.AltOption" flags="ng" index="2fGnzd">
        <child id="7971844778466793072" name="then" index="2fGnzA" />
        <child id="7971844778466793070" name="when" index="2fGnzS" />
      </concept>
      <concept id="7971844778466793028" name="org.iets3.core.expr.base.structure.AlternativesExpression" flags="ng" index="2fGnzi">
        <child id="7971844778466793162" name="alternatives" index="2fGnxs" />
      </concept>
      <concept id="606861080870797309" name="org.iets3.core.expr.base.structure.IfElseSection" flags="ng" index="pf3Wd">
        <child id="606861080870797310" name="expr" index="pf3We" />
      </concept>
      <concept id="7089558164908491660" name="org.iets3.core.expr.base.structure.EmptyExpression" flags="ng" index="2zH6wq" />
      <concept id="7089558164905593724" name="org.iets3.core.expr.base.structure.IOptionallyTyped" flags="ng" index="2zM23E">
        <child id="7089558164905593725" name="type" index="2zM23F" />
      </concept>
      <concept id="7071042522334260296" name="org.iets3.core.expr.base.structure.ITyped" flags="ng" index="2_iKZX">
        <child id="8811147530085329321" name="type" index="2S399n" />
      </concept>
      <concept id="2807135271608145920" name="org.iets3.core.expr.base.structure.IsSomeExpression" flags="ng" index="UmaEC">
        <child id="2807135271608145921" name="expr" index="UmaED" />
      </concept>
      <concept id="2807135271608265973" name="org.iets3.core.expr.base.structure.NoneLiteral" flags="ng" index="UmHTt" />
      <concept id="2807135271607939856" name="org.iets3.core.expr.base.structure.OptionType" flags="ng" index="Uns6S">
        <child id="2807135271607939857" name="baseType" index="Uns6T" />
      </concept>
      <concept id="5115872837156802409" name="org.iets3.core.expr.base.structure.UnaryExpression" flags="ng" index="30czhk">
        <child id="5115872837156802411" name="expr" index="30czhm" />
      </concept>
      <concept id="5115872837156761033" name="org.iets3.core.expr.base.structure.EqualsExpression" flags="ng" index="30cPrO" />
      <concept id="5115872837156687890" name="org.iets3.core.expr.base.structure.LessExpression" flags="ng" index="30d6GJ" />
      <concept id="5115872837156687764" name="org.iets3.core.expr.base.structure.GreaterExpression" flags="ng" index="30d7iD" />
      <concept id="5115872837156723899" name="org.iets3.core.expr.base.structure.LogicalOrExpression" flags="ng" index="30deu6" />
      <concept id="5115872837156652453" name="org.iets3.core.expr.base.structure.MinusExpression" flags="ng" index="30dvUo" />
      <concept id="5115872837156578546" name="org.iets3.core.expr.base.structure.PlusExpression" flags="ng" index="30dDZf" />
      <concept id="5115872837156576277" name="org.iets3.core.expr.base.structure.BinaryExpression" flags="ng" index="30dEsC">
        <child id="5115872837156576280" name="right" index="30dEs_" />
        <child id="5115872837156576278" name="left" index="30dEsF" />
      </concept>
      <concept id="7849560302565679722" name="org.iets3.core.expr.base.structure.IfExpression" flags="ng" index="39w5ZF">
        <child id="606861080870797304" name="elseSection" index="pf3W8" />
        <child id="7849560302565679723" name="condition" index="39w5ZE" />
        <child id="7849560302565679725" name="thenPart" index="39w5ZG" />
      </concept>
      <concept id="9002563722476995145" name="org.iets3.core.expr.base.structure.DotExpression" flags="ng" index="1QScDb">
        <child id="9002563722476995147" name="target" index="1QScD9" />
      </concept>
      <concept id="1059200196223309235" name="org.iets3.core.expr.base.structure.SomeValExpr" flags="ng" index="1ZmhP4">
        <reference id="1059200196223309236" name="someQuery" index="1ZmhP3" />
      </concept>
    </language>
    <language id="6b277d9a-d52d-416f-a209-1919bd737f50" name="org.iets3.core.expr.simpleTypes">
      <concept id="7971844778467001950" name="org.iets3.core.expr.simpleTypes.structure.OtherwiseLiteral" flags="ng" index="2fHqz8" />
      <concept id="4513425716319387765" name="org.iets3.core.expr.simpleTypes.structure.StringToIntTarget" flags="ng" index="2zXAyN" />
      <concept id="5115872837157252552" name="org.iets3.core.expr.simpleTypes.structure.StringLiteral" flags="ng" index="30bdrP">
        <property id="5115872837157252555" name="value" index="30bdrQ" />
      </concept>
      <concept id="5115872837157252551" name="org.iets3.core.expr.simpleTypes.structure.StringType" flags="ng" index="30bdrU" />
      <concept id="5115872837157054284" name="org.iets3.core.expr.simpleTypes.structure.RealType" flags="ng" index="30bXLL" />
      <concept id="5115872837157054169" name="org.iets3.core.expr.simpleTypes.structure.IntegerType" flags="ng" index="30bXR$" />
      <concept id="5115872837157054170" name="org.iets3.core.expr.simpleTypes.structure.NumberLiteral" flags="ng" index="30bXRB">
        <property id="5115872837157054173" name="value" index="30bXRw" />
      </concept>
    </language>
    <language id="71934284-d7d1-45ee-a054-8c072591085f" name="org.iets3.core.expr.toplevel">
      <concept id="7061117989422575313" name="org.iets3.core.expr.toplevel.structure.EnumLiteral" flags="ng" index="5mgYR">
        <child id="4577412849438473348" name="value" index="Y$80S" />
      </concept>
      <concept id="7061117989422575278" name="org.iets3.core.expr.toplevel.structure.EnumDeclaration" flags="ng" index="5mgZ8">
        <child id="7061117989422575348" name="literals" index="5mgYi" />
        <child id="3213836461276467746" name="type" index="3c3ckp" />
      </concept>
      <concept id="7061117989422575803" name="org.iets3.core.expr.toplevel.structure.EnumType" flags="ng" index="5mh7t">
        <reference id="7061117989422575859" name="enum" index="5mh6l" />
      </concept>
      <concept id="7061117989422577349" name="org.iets3.core.expr.toplevel.structure.EnumLiteralRef" flags="ng" index="5mhuz">
        <reference id="7061117989422577417" name="literal" index="5mhpJ" />
      </concept>
      <concept id="7089558164906249676" name="org.iets3.core.expr.toplevel.structure.Constant" flags="ng" index="2zPypq">
        <child id="7089558164906249715" name="value" index="2zPyp_" />
      </concept>
      <concept id="543569365051789113" name="org.iets3.core.expr.toplevel.structure.ConstantRef" flags="ng" index="_emDc">
        <reference id="543569365051789114" name="constant" index="_emDf" />
      </concept>
      <concept id="543569365052765011" name="org.iets3.core.expr.toplevel.structure.EmptyToplevelContent" flags="ng" index="_ixoA" />
      <concept id="8811147530085329320" name="org.iets3.core.expr.toplevel.structure.RecordLiteral" flags="ng" index="2S399m">
        <child id="8811147530085329323" name="memberValues" index="2S399l" />
      </concept>
      <concept id="602952467877559919" name="org.iets3.core.expr.toplevel.structure.IRecordDeclaration" flags="ng" index="S5Q1W">
        <child id="602952467877562565" name="members" index="S5Trm" />
      </concept>
      <concept id="8811147530084018370" name="org.iets3.core.expr.toplevel.structure.RecordType" flags="ng" index="2Ss9cW">
        <reference id="8811147530084018371" name="record" index="2Ss9cX" />
      </concept>
      <concept id="8811147530084018361" name="org.iets3.core.expr.toplevel.structure.RecordMember" flags="ng" index="2Ss9d7" />
      <concept id="4577412849441593498" name="org.iets3.core.expr.toplevel.structure.EnumValueAccessor" flags="ng" index="YK6gA" />
      <concept id="4790956042240570348" name="org.iets3.core.expr.toplevel.structure.FunctionCall" flags="ng" index="1af_rf" />
      <concept id="4790956042240148643" name="org.iets3.core.expr.toplevel.structure.Function" flags="ng" index="1aga60" />
    </language>
    <language id="0be7c48a-9600-4a9c-a497-e40de8415ebd" name="MetaJade.behaviour">
      <concept id="39011061266900173" name="MetaJade.behaviour.structure.ActivateSequentialBehaviour" flags="ng" index="21_nMn" />
      <concept id="39011061267007073" name="MetaJade.behaviour.structure.IBehavioursContainer" flags="ng" index="21ELSV">
        <child id="39011061266908585" name="behaviours" index="21EDRN" />
      </concept>
      <concept id="1363295496955285700" name="MetaJade.behaviour.structure.ActivateBehaviour" flags="ng" index="p0Vrc" />
      <concept id="1363295496955285701" name="MetaJade.behaviour.structure.AbstractBehaviourUnaryExpression" flags="ng" index="p0Vrd">
        <child id="4145021229450611113" name="behaviour" index="3QnlHL" />
      </concept>
      <concept id="3401921042422620142" name="MetaJade.behaviour.structure.Behaviour" flags="ng" index="3oGdb7">
        <child id="2058408588520789632" name="action" index="2dXv$O" />
        <child id="266893304458096888" name="behaviourType" index="3u$SbN" />
        <child id="4289783338601281164" name="inputParams" index="3BfUAV" />
      </concept>
      <concept id="4289783338601963020" name="MetaJade.behaviour.structure.BehaviourArgument" flags="ng" index="3Biw4V" />
      <concept id="4289783338602023683" name="MetaJade.behaviour.structure.BehaviourArgRef" flags="ng" index="3BiLgO">
        <reference id="4289783338602023686" name="arg" index="3BiLgL" />
      </concept>
      <concept id="3737182289224794353" name="MetaJade.behaviour.structure.CyclicBehaviour" flags="ng" index="3DZiec" />
      <concept id="3737182289224794352" name="MetaJade.behaviour.structure.OneShotBehaviour" flags="ng" index="3DZied" />
      <concept id="1741182739291547053" name="MetaJade.behaviour.structure.BehaviourDependency" flags="ng" index="3KE$Bk" />
      <concept id="4145021229450567818" name="MetaJade.behaviour.structure.BehaviourReference" flags="ng" index="3Qnb9i">
        <reference id="4145021229450567819" name="behaviour" index="3Qnb9j" />
        <child id="4145021229450567821" name="inputParams" index="3Qnb9l" />
      </concept>
    </language>
    <language id="fbba5118-5fc6-49ff-9c3b-0b4469830440" name="org.iets3.core.expr.mutable">
      <concept id="4255172619715417408" name="org.iets3.core.expr.mutable.structure.UpdateItExpression" flags="ng" index="3j5BQN" />
      <concept id="4255172619709548950" name="org.iets3.core.expr.mutable.structure.BoxType" flags="ng" index="3sNe5_">
        <child id="4255172619709548951" name="baseType" index="3sNe5$" />
      </concept>
      <concept id="4255172619711277794" name="org.iets3.core.expr.mutable.structure.BoxUpdateTarget" flags="ng" index="3sPC8h">
        <child id="4255172619711277798" name="value" index="3sPC8l" />
      </concept>
      <concept id="4255172619710841704" name="org.iets3.core.expr.mutable.structure.BoxValueTarget" flags="ng" index="3sQ2Ir" />
      <concept id="4255172619710740510" name="org.iets3.core.expr.mutable.structure.BoxExpression" flags="ng" index="3sRH3H">
        <child id="4255172619710740514" name="value" index="3sRH3h" />
      </concept>
    </language>
    <language id="68d558ed-66cf-46ac-b353-d5fddcc21f72" name="MetaJade.aclmessage">
      <concept id="8771781395562772371" name="MetaJade.aclmessage.structure.SenderMatch" flags="ng" index="CnpsB">
        <child id="8771781395562772413" name="value" index="Cnps9" />
      </concept>
      <concept id="8771781395562722383" name="MetaJade.aclmessage.structure.PerformativeMatch" flags="ng" index="CnHjV" />
      <concept id="8771781395564539185" name="MetaJade.aclmessage.structure.GetContentOp" flags="ng" index="FICQ5" />
      <concept id="4949964338174156104" name="MetaJade.aclmessage.structure.LogicalOperator" flags="ng" index="2J2hcu">
        <child id="8872038906290818185" name="left" index="1DXLNC" />
        <child id="8872038906290818187" name="right" index="1DXLNE" />
      </concept>
      <concept id="4949964338176784763" name="MetaJade.aclmessage.structure.SendMessage" flags="ng" index="2JkjsH">
        <child id="2658052393945773747" name="content" index="2rofCy" />
        <child id="2658052393945773748" name="receiver" index="2rofC_" />
        <child id="2658052393945773750" name="type" index="2rofCB" />
      </concept>
      <concept id="8895927397244351935" name="MetaJade.aclmessage.structure.ReceiveMessage" flags="ng" index="2R7DHE">
        <child id="7567840236832936936" name="condition" index="0kSPL" />
        <child id="5091083777057681387" name="onReceive" index="3NSspj" />
        <child id="5091083777057681390" name="arg" index="3NSspm" />
      </concept>
      <concept id="8825853532287002529" name="MetaJade.aclmessage.structure.AndMatcher" flags="ng" index="3dV5MB" />
      <concept id="140811118312068270" name="MetaJade.aclmessage.structure.ReplyToMessage" flags="ng" index="1eBHpp">
        <child id="140811118312068276" name="reply" index="1eBHp3" />
        <child id="140811118312068275" name="type" index="1eBHp4" />
        <child id="140811118312068272" name="content" index="1eBHp7" />
      </concept>
      <concept id="4946621191997514448" name="MetaJade.aclmessage.structure.ReceivedMessageArg" flags="ng" index="1fMSWk">
        <child id="2695020384284204564" name="type" index="2uPfNm" />
      </concept>
      <concept id="4946621191998473754" name="MetaJade.aclmessage.structure.ReceivedMessageArgRef" flags="ng" index="1fQeJu">
        <reference id="4946621191998473788" name="arg" index="1fQeJS" />
      </concept>
      <concept id="1816631675487503527" name="MetaJade.aclmessage.structure.MessageType" flags="ng" index="1g4zL5">
        <child id="2695020384283400037" name="baseType" index="2uKbmB" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="f3eafff0-30d2-46d6-9150-f0f3b880ce27" name="org.iets3.core.expr.path">
      <concept id="7814222126786013807" name="org.iets3.core.expr.path.structure.PathElement" flags="ng" index="3o_JK">
        <reference id="7814222126786013810" name="member" index="3o_JH" />
      </concept>
    </language>
    <language id="ff6c34ae-1ca3-455f-96b3-8f28007794c9" name="MetaJade.ontology">
      <concept id="5525388950973568655" name="MetaJade.ontology.structure.Ontology" flags="ng" index="2C2W_Q">
        <child id="543569365052711058" name="contents" index="_iOnB" />
      </concept>
      <concept id="8320844951296934877" name="MetaJade.ontology.structure.ConceptSchema" flags="ng" index="2MkpuS" />
      <concept id="8320844951296934876" name="MetaJade.ontology.structure.AgentActionSchema" flags="ng" index="2MkpuT" />
    </language>
    <language id="9464fa06-5ab9-409b-9274-64ab29588457" name="org.iets3.core.expr.lambda">
      <concept id="5096753237482793709" name="org.iets3.core.expr.lambda.structure.ModifyEffectTag" flags="ng" index="2lgajX" />
      <concept id="4790956042240983401" name="org.iets3.core.expr.lambda.structure.BlockExpression" flags="ng" index="1aduha">
        <child id="4790956042240983402" name="expressions" index="1aduh9" />
      </concept>
      <concept id="4790956042241105569" name="org.iets3.core.expr.lambda.structure.ValRef" flags="ng" index="1adzI2">
        <reference id="4790956042241106533" name="val" index="1adwt6" />
      </concept>
      <concept id="4790956042241053102" name="org.iets3.core.expr.lambda.structure.ValExpression" flags="ng" index="1adJid">
        <child id="4790956042241053105" name="expr" index="1adJii" />
      </concept>
      <concept id="4790956042240407469" name="org.iets3.core.expr.lambda.structure.ArgRef" flags="ng" index="1afdae">
        <reference id="4790956042240460422" name="arg" index="1afue_" />
      </concept>
      <concept id="4790956042240522396" name="org.iets3.core.expr.lambda.structure.IFunctionCall" flags="ng" index="1afhQZ">
        <reference id="4790956042240522408" name="function" index="1afhQb" />
        <child id="4790956042240522406" name="args" index="1afhQ5" />
      </concept>
      <concept id="4790956042240100911" name="org.iets3.core.expr.lambda.structure.IFunctionLike" flags="ng" index="1ahQWc">
        <child id="3880322347437217307" name="effect" index="28QfE6" />
        <child id="4790956042240100927" name="args" index="1ahQWs" />
        <child id="4790956042240100950" name="body" index="1ahQXP" />
      </concept>
      <concept id="4790956042240100929" name="org.iets3.core.expr.lambda.structure.FunctionArgument" flags="ng" index="1ahQXy" />
      <concept id="7554398283340542342" name="org.iets3.core.expr.lambda.structure.ExecOp" flags="ng" index="3iwYMK">
        <child id="7554398283340567898" name="args" index="3iwOxG" />
      </concept>
      <concept id="7554398283340370581" name="org.iets3.core.expr.lambda.structure.LambdaArgRef" flags="ng" index="3ix4Yz">
        <reference id="7554398283340370582" name="arg" index="3ix4Yw" />
      </concept>
      <concept id="7554398283340318470" name="org.iets3.core.expr.lambda.structure.LambdaExpression" flags="ng" index="3ix9CK">
        <child id="7554398283340319555" name="expression" index="3ix9pP" />
        <child id="7554398283340318471" name="args" index="3ix9CL" />
      </concept>
      <concept id="7554398283340318478" name="org.iets3.core.expr.lambda.structure.LambdaArg" flags="ng" index="3ix9CS" />
      <concept id="7554398283340318473" name="org.iets3.core.expr.lambda.structure.IArgument" flags="ng" index="3ix9CZ">
        <child id="7554398283340318476" name="type" index="3ix9CU" />
      </concept>
      <concept id="7554398283340741814" name="org.iets3.core.expr.lambda.structure.ShortLambdaExpression" flags="ng" index="3izI60">
        <child id="7554398283340741815" name="expression" index="3izI61" />
      </concept>
      <concept id="7554398283340826520" name="org.iets3.core.expr.lambda.structure.ShortLambdaItExpression" flags="ng" index="3izPEI" />
      <concept id="7554398283340107702" name="org.iets3.core.expr.lambda.structure.FunctionType" flags="ng" index="3iA5a0">
        <child id="7791618349055797023" name="effect" index="WKSi4" />
        <child id="7554398283340107703" name="argumentTypes" index="3iA5a1" />
        <child id="7554398283340107705" name="returnType" index="3iA5af" />
      </concept>
    </language>
    <language id="32367449-d150-4018-8690-20f09bf1abe2" name="MetaJade.agent">
      <concept id="8894738314219139740" name="MetaJade.agent.structure.AgentArgRef" flags="ng" index="2IpGv2">
        <reference id="8894738314219139741" name="arg" index="2IpGv3" />
      </concept>
      <concept id="943645189045937631" name="MetaJade.agent.structure.AgentArgument" flags="ng" index="2M3Amj">
        <child id="1410812809236658286" name="value" index="1wyKUC" />
      </concept>
      <concept id="3702022420523728372" name="MetaJade.agent.structure.Agent" flags="ng" index="3eIggM">
        <child id="943645189045937532" name="inputParams" index="2M3AkK" />
        <child id="943645189045937530" name="setup" index="2M3AkQ" />
      </concept>
      <concept id="2133980169836146545" name="MetaJade.agent.structure.SearchService" flags="ng" index="1xkGjp" />
      <concept id="2133980169836146546" name="MetaJade.agent.structure.RegisterService" flags="ng" index="1xkGjq" />
      <concept id="1741182739291004155" name="MetaJade.agent.structure.AbstractDfExpression" flags="ng" index="3KkF22">
        <child id="4316580858993931092" name="service" index="3NsI0Z" />
      </concept>
      <concept id="1741182739291546925" name="MetaJade.agent.structure.AgentDependency" flags="ng" index="3KE$_k" />
    </language>
  </registry>
  <node concept="3eIggM" id="3o3xT4d0Bfp">
    <property role="TrG5h" value="BankClient" />
    <property role="3GE5qa" value="client" />
    <node concept="3KE$_k" id="3o3xT4d0EiY" role="19Gq0t">
      <ref role="21GGV3" node="2aA5E1wW8z" />
    </node>
    <node concept="3KE$_k" id="3o3xT4d0Ej1" role="19Gq0t">
      <ref role="21GGV3" node="7AVCbhi6SHs" />
    </node>
    <node concept="3KE$_k" id="3o3xT4d0ExF" role="19Gq0t">
      <ref role="21GGV3" node="1MgVVihsuI2" />
    </node>
    <node concept="3KE$_k" id="5ZGapTFd3Lo" role="19Gq0t">
      <ref role="21GGV3" node="5ZGapTFd3CB" />
    </node>
    <node concept="2M3Amj" id="22Cof6W8JN3" role="2M3AkK">
      <property role="TrG5h" value="accountId" />
      <node concept="_emDc" id="22Cof6WD_gQ" role="1wyKUC">
        <ref role="_emDf" node="22Cof6WD$eh" />
      </node>
      <node concept="30bdrU" id="22Cof6W8K7A" role="3ix9CU" />
    </node>
    <node concept="2M3Amj" id="22Cof6W8K7S" role="2M3AkK">
      <property role="TrG5h" value="accountName" />
      <node concept="30bdrU" id="22Cof6W8K8b" role="3ix9CU" />
      <node concept="_emDc" id="22Cof6WD_1Q" role="1wyKUC">
        <ref role="_emDf" node="22Cof6WD$d2" />
      </node>
    </node>
    <node concept="2M3Amj" id="22Cof6W8K8w" role="2M3AkK">
      <property role="TrG5h" value="amount" />
      <node concept="_emDc" id="22Cof6WD$Ml" role="1wyKUC">
        <ref role="_emDf" node="22Cof6WD$fw" />
      </node>
      <node concept="30bdrU" id="22Cof6W8K8Q" role="3ix9CU" />
    </node>
    <node concept="2M3Amj" id="22Cof6W8Kck" role="2M3AkK">
      <property role="TrG5h" value="command" />
      <node concept="30bdrU" id="22Cof6W8KcB" role="3ix9CU" />
      <node concept="_emDc" id="22Cof6WD$rM" role="1wyKUC">
        <ref role="_emDf" node="22Cof6WD$hr" />
      </node>
    </node>
    <node concept="1aduha" id="22Cof6W84Ai" role="2M3AkQ">
      <node concept="1adJid" id="22Cof6W8bCv" role="1aduh9">
        <property role="TrG5h" value="serverAIDOpt" />
        <node concept="1xkGjp" id="22Cof6W8bDv" role="1adJii">
          <node concept="_emDc" id="22Cof6WDzLO" role="3NsI0Z">
            <ref role="_emDf" node="22Cof6WDzJw" />
          </node>
        </node>
      </node>
      <node concept="39w5ZF" id="22Cof6W8bFp" role="1aduh9">
        <node concept="pf3Wd" id="22Cof6W8bFq" role="pf3W8">
          <node concept="1aduha" id="22Cof6W8bJ5" role="pf3We">
            <node concept="3lL9fZ" id="22Cof6W8bJY" role="1aduh9">
              <node concept="30bdrP" id="22Cof6W8bKS" role="3lRU4Y">
                <property role="30bdrQ" value="bank server not found" />
              </node>
            </node>
          </node>
        </node>
        <node concept="UmaEC" id="22Cof6W8bGe" role="39w5ZE">
          <node concept="1adzI2" id="22Cof6W8bH1" role="UmaED">
            <ref role="1adwt6" node="22Cof6W8bCv" resolve="serverAIDOpt" />
          </node>
          <node concept="pfQqD" id="22Cof6W8bHw" role="pfQ1b">
            <property role="pfQqC" value="serverAID" />
          </node>
        </node>
        <node concept="1aduha" id="22Cof6W8bIi" role="39w5ZG">
          <node concept="21_nMn" id="6$6nD_D2o31" role="1aduh9">
            <node concept="3Qnb9i" id="22Cof6W84B2" role="21EDRN">
              <ref role="3Qnb9j" node="1MgVVihsuI2" />
              <node concept="1QScDb" id="22Cof6W8M7Q" role="3Qnb9l">
                <node concept="2zXAyN" id="22Cof6W8Mg9" role="1QScD9" />
                <node concept="2IpGv2" id="22Cof6W8Lvo" role="30czhm">
                  <ref role="2IpGv3" node="22Cof6W8JN3" resolve="accountId" />
                </node>
              </node>
              <node concept="2IpGv2" id="22Cof6W8Lzp" role="3Qnb9l">
                <ref role="2IpGv3" node="22Cof6W8K7S" resolve="accountName" />
              </node>
              <node concept="1QScDb" id="22Cof6W8MnM" role="3Qnb9l">
                <node concept="2zXAyN" id="22Cof6WHHwy" role="1QScD9" />
                <node concept="2IpGv2" id="22Cof6W8LAV" role="30czhm">
                  <ref role="2IpGv3" node="22Cof6W8K8w" resolve="amount" />
                </node>
              </node>
              <node concept="1af_rf" id="22Cof6W8LER" role="3Qnb9l">
                <ref role="1afhQb" node="2aA5E111OR" />
                <node concept="2IpGv2" id="22Cof6W8LLN" role="1afhQ5">
                  <ref role="2IpGv3" node="22Cof6W8Kck" resolve="command" />
                </node>
              </node>
              <node concept="1ZmhP4" id="22Cof6W8c1d" role="3Qnb9l">
                <ref role="1ZmhP3" node="22Cof6W8bGe" resolve="serverAID" />
              </node>
            </node>
            <node concept="3Qnb9i" id="5ZGapTFd3Lw" role="21EDRN">
              <ref role="3Qnb9j" node="5ZGapTFd3CB" />
              <node concept="1ZmhP4" id="5ZGapTFd3YF" role="3Qnb9l">
                <ref role="1ZmhP3" node="22Cof6W8bGe" resolve="serverAID" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3oGdb7" id="5ZGapTFd3CB">
    <property role="3GE5qa" value="client" />
    <property role="TrG5h" value="HandleConfirmation" />
    <node concept="3Biw4V" id="5ZGapTFd3WW" role="3BfUAV">
      <property role="TrG5h" value="serverAID" />
      <node concept="3NxlSm" id="5ZGapTFd3X3" role="3ix9CU" />
    </node>
    <node concept="1aduha" id="5ZGapTFd3UF" role="2dXv$O">
      <node concept="2R7DHE" id="5ZGapTFd3Uq" role="1aduh9">
        <node concept="1fMSWk" id="5ZGapTFd3Us" role="3NSspm">
          <property role="TrG5h" value="confMsg" />
          <node concept="1g4zL5" id="5ZGapTFd3Uu" role="2uPfNm">
            <node concept="2Ss9cW" id="6uhyzz02cW1" role="2uKbmB">
              <ref role="2Ss9cX" node="2aA5E1$kmQ" />
            </node>
          </node>
        </node>
        <node concept="1aduha" id="5ZGapTFd3XM" role="3NSspj">
          <node concept="3lL9fZ" id="6uhyzz02cX9" role="1aduh9">
            <node concept="1QScDb" id="6uhyzz02cYp" role="3lRU4Y">
              <node concept="FICQ5" id="6uhyzz02d0n" role="1QScD9" />
              <node concept="1fQeJu" id="6uhyzz02cXM" role="30czhm">
                <ref role="1fQeJS" node="5ZGapTFd3Us" resolve="confMsg" />
              </node>
            </node>
          </node>
          <node concept="2cDHYC" id="6RPFlHCAVEk" role="1aduh9" />
        </node>
        <node concept="3dV5MB" id="6RPFlHCAWlc" role="0kSPL">
          <node concept="CnHjV" id="6RPFlHCAWld" role="1DXLNC">
            <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
          </node>
          <node concept="CnpsB" id="6RPFlHCAWle" role="1DXLNE">
            <node concept="3BiLgO" id="6RPFlHCAWlf" role="Cnps9">
              <ref role="3BiLgL" node="5ZGapTFd3WW" resolve="serverAID" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2zH6wq" id="5ZGapTFd4fo" role="1aduh9" />
      <node concept="2R7DHE" id="6uhyzz02d6v" role="1aduh9">
        <node concept="1fMSWk" id="6uhyzz02d6x" role="3NSspm">
          <property role="TrG5h" value="probMsg" />
          <node concept="1g4zL5" id="6uhyzz02d6z" role="2uPfNm">
            <node concept="2Ss9cW" id="6uhyzz02d9T" role="2uKbmB">
              <ref role="2Ss9cX" node="2aA5E1_HHf" />
            </node>
          </node>
        </node>
        <node concept="1aduha" id="6uhyzz02daT" role="3NSspj">
          <node concept="3lL9fZ" id="6uhyzz02dcN" role="1aduh9">
            <node concept="30bdrP" id="6uhyzz02ddN" role="3lRU4Y">
              <property role="30bdrQ" value="a problem was found" />
            </node>
          </node>
          <node concept="3lL9fZ" id="6uhyzz02dje" role="1aduh9">
            <node concept="1QScDb" id="6uhyzz02dnm" role="3lRU4Y">
              <node concept="FICQ5" id="6uhyzz02dpS" role="1QScD9" />
              <node concept="1fQeJu" id="6uhyzz02dma" role="30czhm">
                <ref role="1fQeJS" node="6uhyzz02d6x" resolve="probMsg" />
              </node>
            </node>
          </node>
          <node concept="2cDHYC" id="6RPFlHCAVN0" role="1aduh9" />
        </node>
        <node concept="3dV5MB" id="6RPFlHCAWiB" role="0kSPL">
          <node concept="CnHjV" id="6RPFlHCAWiC" role="1DXLNC">
            <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
          </node>
          <node concept="CnpsB" id="6RPFlHCAWiD" role="1DXLNE">
            <node concept="3BiLgO" id="6RPFlHCAWiE" role="Cnps9">
              <ref role="3BiLgL" node="5ZGapTFd3WW" resolve="serverAID" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2zH6wq" id="6uhyzz02d5p" role="1aduh9" />
      <node concept="2R7DHE" id="5ZGapTFd4gc" role="1aduh9">
        <node concept="1fMSWk" id="5ZGapTFd4ge" role="3NSspm">
          <property role="TrG5h" value="confMsg" />
          <node concept="1g4zL5" id="5ZGapTFd4gg" role="2uPfNm">
            <node concept="2Ss9cW" id="6uhyzz02d4_" role="2uKbmB">
              <ref role="2Ss9cX" node="5ZGapTFaVSq" />
            </node>
          </node>
        </node>
        <node concept="1aduha" id="5ZGapTFdmjo" role="3NSspj">
          <node concept="3lL9fZ" id="5ZGapTFdmmX" role="1aduh9">
            <node concept="30bdrP" id="5ZGapTFdmnP" role="3lRU4Y">
              <property role="30bdrQ" value="received the result of requesting the list of operations" />
            </node>
          </node>
          <node concept="3lL9fZ" id="6uhyzz02d19" role="1aduh9">
            <node concept="1QScDb" id="6uhyzz02d1V" role="3lRU4Y">
              <node concept="FICQ5" id="6uhyzz02d3C" role="1QScD9" />
              <node concept="1fQeJu" id="6uhyzz02d1_" role="30czhm">
                <ref role="1fQeJS" node="5ZGapTFd4ge" resolve="confMsg" />
              </node>
            </node>
          </node>
          <node concept="2cDHYC" id="6RPFlHCAVSV" role="1aduh9" />
        </node>
        <node concept="3dV5MB" id="6RPFlHCAWau" role="0kSPL">
          <node concept="CnHjV" id="6RPFlHCAWd6" role="1DXLNC">
            <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
          </node>
          <node concept="CnpsB" id="6RPFlHCAWfE" role="1DXLNE">
            <node concept="3BiLgO" id="6RPFlHCAWit" role="Cnps9">
              <ref role="3BiLgL" node="5ZGapTFd3WW" resolve="serverAID" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2zH6wq" id="6IoGK7QxYLo" role="1aduh9" />
      <node concept="2R7DHE" id="6IoGK7QxYO9" role="1aduh9">
        <node concept="1fMSWk" id="6IoGK7QxYOb" role="3NSspm">
          <property role="TrG5h" value="opMsg" />
          <node concept="1g4zL5" id="6IoGK7QxYOd" role="2uPfNm">
            <node concept="2Ss9cW" id="6IoGK7QxYQ2" role="2uKbmB">
              <ref role="2Ss9cX" node="2aA5E1_HGC" resolve="Operation" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3DZiec" id="6uhyzyZTURr" role="3u$SbN" />
    <node concept="3KE$Bk" id="5ZGapTFd3UD" role="19Gq0t">
      <ref role="21GGV3" node="2aA5E1wW8z" />
    </node>
  </node>
  <node concept="3oGdb7" id="1MgVVihsuI2">
    <property role="3GE5qa" value="client" />
    <property role="TrG5h" value="SendRequest" />
    <node concept="3Biw4V" id="2umCVQRsktt" role="3BfUAV">
      <property role="TrG5h" value="chosenAccountId" />
      <node concept="30bXR$" id="7yKokgJ83BM" role="3ix9CU" />
    </node>
    <node concept="3Biw4V" id="3I8nNvsLSR0" role="3BfUAV">
      <property role="TrG5h" value="chosenAccountName" />
      <node concept="30bdrU" id="3I8nNvsQ6h8" role="3ix9CU" />
    </node>
    <node concept="3Biw4V" id="3I8nNvsLTaq" role="3BfUAV">
      <property role="TrG5h" value="chosenAmount" />
      <node concept="30bXLL" id="3I8nNvsMvX9" role="3ix9CU" />
    </node>
    <node concept="3Biw4V" id="2aA5E10ZLg" role="3BfUAV">
      <property role="TrG5h" value="cmd" />
      <node concept="5mh7t" id="2aA5E1117v" role="3ix9CU">
        <ref role="5mh6l" node="1MgVVihszjp" />
      </node>
    </node>
    <node concept="3Biw4V" id="Dtlr0ZZQTu" role="3BfUAV">
      <property role="TrG5h" value="serverAID" />
      <node concept="3NxlSm" id="22Cof6W7YTR" role="3ix9CU" />
    </node>
    <node concept="3DZied" id="1MgVVihsuI4" role="3u$SbN" />
    <node concept="1aduha" id="1MgVVihsuI9" role="2dXv$O">
      <node concept="2fGnzi" id="1MgVVihszdx" role="1aduh9">
        <node concept="2fGnzd" id="1MgVVihszdz" role="2fGnxs">
          <node concept="30cPrO" id="1MgVVihszwT" role="2fGnzS">
            <node concept="5mhuz" id="1MgVVihszxW" role="30dEs_">
              <ref role="5mhpJ" node="1MgVVihszjz" />
            </node>
            <node concept="3BiLgO" id="2aA5E118JV" role="30dEsF">
              <ref role="3BiLgL" node="2aA5E10ZLg" resolve="cmd" />
            </node>
          </node>
          <node concept="1af_rf" id="1MgVVihs$Xh" role="2fGnzA">
            <ref role="1afhQb" node="1MgVVihs$_1" resolve="requestAccounCreation" />
            <node concept="3BiLgO" id="2umCVQRsjoD" role="1afhQ5">
              <ref role="3BiLgL" node="3I8nNvsLSR0" resolve="chosenAccountName" />
            </node>
          </node>
        </node>
        <node concept="2fGnzd" id="1MgVVihszAR" role="2fGnxs">
          <node concept="30deu6" id="1MgVVihtG_$" role="2fGnzS">
            <node concept="30cPrO" id="1MgVVihtG__" role="30dEsF">
              <node concept="3BiLgO" id="2aA5E1192U" role="30dEsF">
                <ref role="3BiLgL" node="2aA5E10ZLg" resolve="cmd" />
              </node>
              <node concept="5mhuz" id="1MgVVihszEo" role="30dEs_">
                <ref role="5mhpJ" node="1MgVVihszjB" />
              </node>
            </node>
            <node concept="30cPrO" id="1MgVVihtGIA" role="30dEs_">
              <node concept="5mhuz" id="1MgVVihtGNb" role="30dEs_">
                <ref role="5mhpJ" node="1MgVVihszjN" />
              </node>
              <node concept="3BiLgO" id="2aA5E119lN" role="30dEsF">
                <ref role="3BiLgL" node="2aA5E10ZLg" resolve="cmd" />
              </node>
            </node>
          </node>
          <node concept="1af_rf" id="1MgVVihxaW8" role="2fGnzA">
            <ref role="1afhQb" node="1MgVVihs$U3" resolve="requestOperation" />
            <node concept="3BiLgO" id="2umCVQRsE6_" role="1afhQ5">
              <ref role="3BiLgL" node="2umCVQRsktt" resolve="chosenAccountId" />
            </node>
            <node concept="3BiLgO" id="2umCVQRstHw" role="1afhQ5">
              <ref role="3BiLgL" node="2aA5E10ZLg" resolve="cmd" />
            </node>
            <node concept="3BiLgO" id="2umCVQRsEzk" role="1afhQ5">
              <ref role="3BiLgL" node="3I8nNvsLTaq" resolve="chosenAmount" />
            </node>
          </node>
        </node>
        <node concept="2fGnzd" id="1MgVVihszMM" role="2fGnxs">
          <node concept="30deu6" id="1MgVVihtH8G" role="2fGnzS">
            <node concept="30cPrO" id="1MgVVihtH8H" role="30dEsF">
              <node concept="3BiLgO" id="2aA5E119Dz" role="30dEsF">
                <ref role="3BiLgL" node="2aA5E10ZLg" resolve="cmd" />
              </node>
              <node concept="5mhuz" id="1MgVVihszTu" role="30dEs_">
                <ref role="5mhpJ" node="1MgVVihszjJ" />
              </node>
            </node>
            <node concept="30cPrO" id="1MgVVihtHht" role="30dEs_">
              <node concept="5mhuz" id="1MgVVihtHlG" role="30dEs_">
                <ref role="5mhpJ" node="1MgVVihszjR" />
              </node>
              <node concept="3BiLgO" id="2aA5E119Wu" role="30dEsF">
                <ref role="3BiLgL" node="2aA5E10ZLg" resolve="cmd" />
              </node>
            </node>
          </node>
          <node concept="1af_rf" id="1MgVVihxbdx" role="2fGnzA">
            <ref role="1afhQb" node="1MgVVihyjg8" resolve="queryInformation" />
            <node concept="3BiLgO" id="2umCVQRsEkv" role="1afhQ5">
              <ref role="3BiLgL" node="2umCVQRsktt" resolve="chosenAccountId" />
            </node>
            <node concept="3BiLgO" id="2umCVQRstX5" role="1afhQ5">
              <ref role="3BiLgL" node="2aA5E10ZLg" resolve="cmd" />
            </node>
          </node>
        </node>
        <node concept="2fGnzd" id="5ZGapTEJW8q" role="2fGnxs">
          <node concept="2fHqz8" id="5ZGapTEJWhv" role="2fGnzS" />
          <node concept="3lL9fZ" id="5ZGapTELsKY" role="2fGnzA">
            <node concept="30bdrP" id="5ZGapTELsU9" role="3lRU4Y">
              <property role="30bdrQ" value="Unknown command" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1aga60" id="1MgVVihs$_1" role="3KDhAD">
      <property role="TrG5h" value="requestAccounCreation" />
      <node concept="1ahQXy" id="2umCVQRscrQ" role="1ahQWs">
        <property role="TrG5h" value="name" />
        <node concept="30bdrU" id="2umCVQRsem8" role="3ix9CU" />
      </node>
      <node concept="2lgajX" id="76FbbBinonJ" role="28QfE6" />
      <node concept="1aduha" id="22Cof6WDZAR" role="1ahQXP">
        <node concept="3lL9fZ" id="22Cof6WDZNh" role="1aduh9">
          <node concept="30dDZf" id="22Cof6WE0$l" role="3lRU4Y">
            <node concept="1afdae" id="22Cof6WE0Fv" role="30dEs_">
              <ref role="1afue_" node="2umCVQRscrQ" resolve="name" />
            </node>
            <node concept="30bdrP" id="22Cof6WDZNt" role="30dEsF">
              <property role="30bdrQ" value="sent request to create an account: " />
            </node>
          </node>
        </node>
        <node concept="2JkjsH" id="7IIeq5wMSbL" role="1aduh9">
          <property role="2NJI0k" value="4MI7ZB$IHfN/Request" />
          <node concept="3BiLgO" id="7IIeq5wMSnZ" role="2rofC_">
            <ref role="3BiLgL" node="Dtlr0ZZQTu" resolve="serverAID" />
          </node>
          <node concept="1g4zL5" id="7IIeq5wMSbR" role="2rofCB">
            <node concept="2Ss9cW" id="7IIeq5wMSUp" role="2uKbmB">
              <ref role="2Ss9cX" node="2aA5E1_HGR" resolve="CreateAccount" />
            </node>
          </node>
          <node concept="2S399m" id="7IIeq5wMSzz" role="2rofCy">
            <node concept="2Ss9cW" id="7IIeq5wMSz$" role="2S399n">
              <ref role="2Ss9cX" node="2aA5E1_HGR" resolve="CreateAccount" />
            </node>
            <node concept="1afdae" id="7IIeq5wMSz_" role="2S399l">
              <ref role="1afue_" node="2umCVQRscrQ" resolve="name" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="_ixoA" id="22Cof6W84ea" role="3KDhAD" />
    <node concept="1aga60" id="1MgVVihs$U3" role="3KDhAD">
      <property role="TrG5h" value="requestOperation" />
      <node concept="2lgajX" id="1MgVVihxCX6" role="28QfE6" />
      <node concept="1ahQXy" id="2umCVQRsC9W" role="1ahQWs">
        <property role="TrG5h" value="id" />
        <node concept="30bXR$" id="7yKokgJ82TG" role="3ix9CU" />
      </node>
      <node concept="1ahQXy" id="2umCVQRsudm" role="1ahQWs">
        <property role="TrG5h" value="cmd" />
        <node concept="5mh7t" id="2umCVQRsusP" role="3ix9CU">
          <ref role="5mh6l" node="1MgVVihszjp" />
        </node>
      </node>
      <node concept="1ahQXy" id="2umCVQRsCF0" role="1ahQWs">
        <property role="TrG5h" value="amount" />
        <node concept="30bXLL" id="2umCVQRsD6t" role="3ix9CU" />
      </node>
      <node concept="1aduha" id="22Cof6WE0LG" role="1ahQXP">
        <node concept="3lL9fZ" id="22Cof6WE0Zk" role="1aduh9">
          <node concept="30dDZf" id="3o3xT4cP0pG" role="3lRU4Y">
            <node concept="30dDZf" id="3o3xT4cP0pH" role="30dEsF">
              <node concept="30dDZf" id="3o3xT4cP0pI" role="30dEsF">
                <node concept="30bdrP" id="3o3xT4cP0pJ" role="30dEsF">
                  <property role="30bdrQ" value="sent request to perform a " />
                </node>
                <node concept="2fGnzi" id="22Cof6WE6yo" role="30dEs_">
                  <node concept="2fGnzd" id="22Cof6WE6yp" role="2fGnxs">
                    <node concept="30cPrO" id="22Cof6WE66Q" role="2fGnzS">
                      <node concept="5mhuz" id="22Cof6WE6dC" role="30dEs_">
                        <ref role="5mhpJ" node="1MgVVihszjB" />
                      </node>
                      <node concept="1afdae" id="22Cof6WE60p" role="30dEsF">
                        <ref role="1afue_" node="2umCVQRsudm" resolve="cmd" />
                      </node>
                    </node>
                    <node concept="30bdrP" id="22Cof6WE95F" role="2fGnzA">
                      <property role="30bdrQ" value="deposit" />
                    </node>
                  </node>
                  <node concept="2fGnzd" id="22Cof6WE6yq" role="2fGnxs">
                    <node concept="30cPrO" id="22Cof6WE7pV" role="2fGnzS">
                      <node concept="5mhuz" id="22Cof6WE7xI" role="30dEs_">
                        <ref role="5mhpJ" node="1MgVVihszjN" />
                      </node>
                      <node concept="1afdae" id="22Cof6WE7i$" role="30dEsF">
                        <ref role="1afue_" node="2umCVQRsudm" resolve="cmd" />
                      </node>
                    </node>
                    <node concept="30bdrP" id="22Cof6WE9tB" role="2fGnzA">
                      <property role="30bdrQ" value="withdraw" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="30bdrP" id="3o3xT4cP0pK" role="30dEs_">
                <property role="30bdrQ" value=" from account with id " />
              </node>
            </node>
            <node concept="1afdae" id="3o3xT4cP0yk" role="30dEs_">
              <ref role="1afue_" node="2umCVQRsC9W" resolve="id" />
            </node>
          </node>
        </node>
        <node concept="2zH6wq" id="22Cof6WE8_p" role="1aduh9" />
        <node concept="2JkjsH" id="7IIeq5wMTJe" role="1aduh9">
          <property role="2NJI0k" value="4MI7ZB$IHfN/Request" />
          <node concept="3BiLgO" id="7IIeq5wMUeh" role="2rofC_">
            <ref role="3BiLgL" node="Dtlr0ZZQTu" resolve="serverAID" />
          </node>
          <node concept="1g4zL5" id="7IIeq5wMTJk" role="2rofCB">
            <node concept="2Ss9cW" id="7IIeq5wMTTP" role="2uKbmB">
              <ref role="2Ss9cX" node="2aA5E1_HGL" resolve="MakeOperation" />
            </node>
          </node>
          <node concept="2S399m" id="7IIeq5wMUoa" role="2rofCy">
            <node concept="1afdae" id="7IIeq5wMUob" role="2S399l">
              <ref role="1afue_" node="2umCVQRsC9W" resolve="id" />
            </node>
            <node concept="1QScDb" id="7IIeq5wMUoc" role="2S399l">
              <node concept="YK6gA" id="7IIeq5wMUod" role="1QScD9" />
              <node concept="1afdae" id="7IIeq5wMUoe" role="30czhm">
                <ref role="1afue_" node="2umCVQRsudm" resolve="cmd" />
              </node>
            </node>
            <node concept="1afdae" id="7IIeq5wMUof" role="2S399l">
              <ref role="1afue_" node="2umCVQRsCF0" resolve="amount" />
            </node>
            <node concept="2Ss9cW" id="7IIeq5wMUog" role="2S399n">
              <ref role="2Ss9cX" node="2aA5E1_HGL" resolve="MakeOperation" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="_ixoA" id="1MgVVihxIrL" role="3KDhAD" />
    <node concept="1aga60" id="1MgVVihyjg8" role="3KDhAD">
      <property role="TrG5h" value="queryInformation" />
      <node concept="2lgajX" id="3I8nNvsQbpU" role="28QfE6" />
      <node concept="1ahQXy" id="2umCVQRsDme" role="1ahQWs">
        <property role="TrG5h" value="id" />
        <node concept="30bXR$" id="7yKokgJ8317" role="3ix9CU" />
      </node>
      <node concept="1ahQXy" id="2umCVQRs$CK" role="1ahQWs">
        <property role="TrG5h" value="cmd" />
        <node concept="5mh7t" id="2umCVQRs_5V" role="3ix9CU">
          <ref role="5mh6l" node="1MgVVihszjp" />
        </node>
      </node>
      <node concept="1aduha" id="22Cof6WE866" role="1ahQXP">
        <node concept="3lL9fZ" id="22Cof6WEa46" role="1aduh9">
          <node concept="30dDZf" id="3o3xT4cP2Kh" role="3lRU4Y">
            <node concept="30dDZf" id="3o3xT4cP2Ki" role="30dEsF">
              <node concept="30dDZf" id="3o3xT4cP2Kj" role="30dEsF">
                <node concept="30bdrP" id="3o3xT4cP2Kk" role="30dEsF">
                  <property role="30bdrQ" value="sent request to get the " />
                </node>
                <node concept="2fGnzi" id="22Cof6WEa49" role="30dEs_">
                  <node concept="2fGnzd" id="22Cof6WEa4a" role="2fGnxs">
                    <node concept="30cPrO" id="22Cof6WEa4b" role="2fGnzS">
                      <node concept="5mhuz" id="22Cof6WEa4c" role="30dEs_">
                        <ref role="5mhpJ" node="1MgVVihszjR" />
                      </node>
                      <node concept="1afdae" id="22Cof6WEa4d" role="30dEsF">
                        <ref role="1afue_" node="2umCVQRs$CK" resolve="cmd" />
                      </node>
                    </node>
                    <node concept="30bdrP" id="22Cof6WEa4e" role="2fGnzA">
                      <property role="30bdrQ" value="list of operations" />
                    </node>
                  </node>
                  <node concept="2fGnzd" id="22Cof6WEa4f" role="2fGnxs">
                    <node concept="30cPrO" id="22Cof6WEa4g" role="2fGnzS">
                      <node concept="5mhuz" id="22Cof6WEa4h" role="30dEs_">
                        <ref role="5mhpJ" node="1MgVVihszjJ" />
                      </node>
                      <node concept="1afdae" id="22Cof6WEa4i" role="30dEsF">
                        <ref role="1afue_" node="2umCVQRs$CK" resolve="cmd" />
                      </node>
                    </node>
                    <node concept="30bdrP" id="22Cof6WEa4j" role="2fGnzA">
                      <property role="30bdrQ" value="balance" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="30bdrP" id="3o3xT4cP2Kl" role="30dEs_">
                <property role="30bdrQ" value=" from account with id " />
              </node>
            </node>
            <node concept="1afdae" id="3o3xT4cP2TH" role="30dEs_">
              <ref role="1afue_" node="2umCVQRsDme" resolve="id" />
            </node>
          </node>
        </node>
        <node concept="2zH6wq" id="22Cof6WE8dz" role="1aduh9" />
        <node concept="2JkjsH" id="7IIeq5wMUSv" role="1aduh9">
          <property role="2NJI0k" value="4MI7ZB$IHfN/Request" />
          <node concept="3BiLgO" id="7IIeq5wMVoy" role="2rofC_">
            <ref role="3BiLgL" node="Dtlr0ZZQTu" resolve="serverAID" />
          </node>
          <node concept="1g4zL5" id="7IIeq5wMUS_" role="2rofCB">
            <node concept="2Ss9cW" id="7IIeq5wMV3M" role="2uKbmB">
              <ref role="2Ss9cX" node="2aA5E1_HH6" resolve="InformationRequest" />
            </node>
          </node>
          <node concept="2S399m" id="7IIeq5wMVyy" role="2rofCy">
            <node concept="2Ss9cW" id="7IIeq5wMVyz" role="2S399n">
              <ref role="2Ss9cX" node="2aA5E1_HH6" resolve="InformationRequest" />
            </node>
            <node concept="1afdae" id="7IIeq5wMVy$" role="2S399l">
              <ref role="1afue_" node="2umCVQRsDme" resolve="id" />
            </node>
            <node concept="1QScDb" id="7IIeq5wMVy_" role="2S399l">
              <node concept="YK6gA" id="7IIeq5wMVyA" role="1QScD9" />
              <node concept="1afdae" id="7IIeq5wMVyB" role="30czhm">
                <ref role="1afue_" node="2umCVQRs$CK" resolve="cmd" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="_ixoA" id="3I8nNvsysxu" role="3KDhAD" />
    <node concept="3KE$Bk" id="2aA5E1sh8S" role="19Gq0t">
      <ref role="21GGV3" node="2aA5E1wW8z" />
    </node>
    <node concept="3KE$Bk" id="Dtlr0ZZP0l" role="19Gq0t">
      <ref role="21GGV3" node="7AVCbhi6SHs" />
    </node>
  </node>
  <node concept="3eIggM" id="3o3xT4d0Biv">
    <property role="TrG5h" value="BankServer" />
    <property role="3GE5qa" value="server" />
    <node concept="1aduha" id="3o3xT4d0EW6" role="2M3AkQ">
      <node concept="1adJid" id="22Cof6Wak6E" role="1aduh9">
        <property role="TrG5h" value="accounts" />
        <node concept="3sRH3H" id="22Cof6Wak73" role="1adJii">
          <node concept="3iBYfx" id="22Cof6Wak7q" role="3sRH3h">
            <node concept="ygwf7" id="22Cof6Wak7F" role="ygBzB">
              <node concept="2Ss9cW" id="22Cof6Wak8C" role="ygwf4">
                <ref role="2Ss9cX" node="2aA5E1$kmQ" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1adJid" id="22Cof6Wak9M" role="1aduh9">
        <property role="TrG5h" value="operations" />
        <node concept="3sRH3H" id="22Cof6WakbI" role="1adJii">
          <node concept="3iBYfx" id="22Cof6WakcO" role="3sRH3h">
            <node concept="ygwf7" id="22Cof6WakdL" role="ygBzB">
              <node concept="2Ss9cW" id="22Cof6Wakft" role="ygwf4">
                <ref role="2Ss9cX" node="2aA5E1_HGC" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2zH6wq" id="3o3xT4d0FKM" role="1aduh9" />
      <node concept="3lL9fZ" id="22Cof6WEc18" role="1aduh9">
        <node concept="30dDZf" id="22Cof6WEc3W" role="3lRU4Y">
          <node concept="_emDc" id="3o3xT4d0Fqb" role="30dEs_">
            <ref role="_emDf" node="22Cof6WDzJw" />
          </node>
          <node concept="30bdrP" id="22Cof6WEc1v" role="30dEsF">
            <property role="30bdrQ" value="registered service " />
          </node>
        </node>
      </node>
      <node concept="1xkGjq" id="1MgVVihpRqu" role="1aduh9">
        <node concept="_emDc" id="3o3xT4d0FtP" role="3NsI0Z">
          <ref role="_emDf" node="22Cof6WDzJw" />
        </node>
      </node>
      <node concept="2zH6wq" id="3o3xT4d0Fmx" role="1aduh9" />
      <node concept="p0Vrc" id="3o3xT4d0F94" role="1aduh9">
        <node concept="3Qnb9i" id="22Cof6WakoS" role="3QnlHL">
          <ref role="3Qnb9j" node="3o3xT4d0Haw" />
          <node concept="1adzI2" id="22Cof6WakqU" role="3Qnb9l">
            <ref role="1adwt6" node="22Cof6Wak6E" resolve="accounts" />
          </node>
          <node concept="1adzI2" id="22Cof6WakuP" role="3Qnb9l">
            <ref role="1adwt6" node="22Cof6Wak9M" resolve="operations" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3KE$_k" id="3o3xT4d0Fq6" role="19Gq0t">
      <ref role="21GGV3" node="2aA5E1wW8z" />
    </node>
    <node concept="3KE$_k" id="3o3xT4d0Fq8" role="19Gq0t">
      <ref role="21GGV3" node="7AVCbhi6SHs" />
    </node>
    <node concept="3KE$_k" id="3o3xT4d0Hj7" role="19Gq0t">
      <ref role="21GGV3" node="3o3xT4d0Haw" />
    </node>
  </node>
  <node concept="3oGdb7" id="1MgVVihrBlE">
    <property role="TrG5h" value="GiveInformation" />
    <property role="3GE5qa" value="server" />
    <node concept="1aga60" id="3o3xT4d5U2H" role="3KDhAD">
      <property role="TrG5h" value="sendResult" />
      <node concept="1aduha" id="3o3xT4d5XfN" role="1ahQXP">
        <node concept="1adJid" id="3o3xT4d5Y3y" role="1aduh9">
          <property role="TrG5h" value="accOpt" />
          <node concept="1QScDb" id="3o3xT4d5Y3z" role="1adJii">
            <node concept="1HmgMX" id="3o3xT4d5Y3$" role="1QScD9">
              <node concept="3izI60" id="3o3xT4d5Y3_" role="3iAY4F">
                <node concept="30cPrO" id="3o3xT4d5Y3A" role="3izI61">
                  <node concept="1afdae" id="3o3xT4d5Y3B" role="30dEs_">
                    <ref role="1afue_" node="3o3xT4d5UmR" resolve="id" />
                  </node>
                  <node concept="1QScDb" id="3o3xT4d5Y3C" role="30dEsF">
                    <node concept="3o_JK" id="3o3xT4d5Y3D" role="1QScD9">
                      <ref role="3o_JH" node="2aA5E1$kn1" />
                    </node>
                    <node concept="3izPEI" id="3o3xT4d5Y3E" role="30czhm" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3BiLgO" id="3o3xT4d5Y3F" role="30czhm">
              <ref role="3BiLgL" node="4FZ6_9O1UcI" resolve="storedAccounts" />
            </node>
          </node>
        </node>
        <node concept="39w5ZF" id="3o3xT4d5Y3G" role="1aduh9">
          <node concept="pf3Wd" id="3o3xT4d5Y3H" role="pf3W8">
            <node concept="1af_rf" id="3o3xT4d5Y3I" role="pf3We">
              <ref role="1afhQb" node="7IIeq5wNs0h" resolve="sendInfReqProblem" />
              <node concept="3BiLgO" id="3o3xT4d5Y3J" role="1afhQ5">
                <ref role="3BiLgL" node="4FZ6_9O1SLX" resolve="informationRequest" />
              </node>
              <node concept="5mhuz" id="3o3xT4d5Y3K" role="1afhQ5">
                <ref role="5mhpJ" node="2umCVQRk8Ye" />
              </node>
            </node>
          </node>
          <node concept="1QScDb" id="3o3xT4d5Z_J" role="39w5ZG">
            <node concept="3iwYMK" id="3o3xT4d5ZT7" role="1QScD9">
              <node concept="1ZmhP4" id="3o3xT4d60cY" role="3iwOxG">
                <ref role="1ZmhP3" node="3o3xT4d5Y3S" resolve="acc" />
              </node>
            </node>
            <node concept="1afdae" id="3o3xT4d5ZfV" role="30czhm">
              <ref role="1afue_" node="3o3xT4d5UPv" resolve="test" />
            </node>
          </node>
          <node concept="UmaEC" id="3o3xT4d5Y3S" role="39w5ZE">
            <node concept="1adzI2" id="3o3xT4d5Y3T" role="UmaED">
              <ref role="1adwt6" node="3o3xT4d5Y3y" resolve="accOpt" />
            </node>
            <node concept="pfQqD" id="3o3xT4d5Y3U" role="pfQ1b">
              <property role="pfQqC" value="acc" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1ahQXy" id="3o3xT4d5UmR" role="1ahQWs">
        <property role="TrG5h" value="id" />
        <node concept="30bXR$" id="3o3xT4d5Un6" role="3ix9CU" />
      </node>
      <node concept="1ahQXy" id="3o3xT4d5UPv" role="1ahQWs">
        <property role="TrG5h" value="test" />
        <node concept="3iA5a0" id="3o3xT4d5VyP" role="3ix9CU">
          <node concept="3iyjE5" id="3o3xT4d5VMo" role="3iA5af" />
          <node concept="2lgajX" id="3o3xT4d5W1B" role="WKSi4" />
          <node concept="2Ss9cW" id="3o3xT4d5WgV" role="3iA5a1">
            <ref role="2Ss9cX" node="2aA5E1$kmQ" />
          </node>
        </node>
      </node>
      <node concept="2lgajX" id="3o3xT4d60Rz" role="28QfE6" />
    </node>
    <node concept="_ixoA" id="3o3xT4d5XJ9" role="3KDhAD" />
    <node concept="3Biw4V" id="4FZ6_9O1SLX" role="3BfUAV">
      <property role="TrG5h" value="informationRequest" />
      <node concept="1g4zL5" id="2lACGaVEDeT" role="3ix9CU">
        <node concept="2Ss9cW" id="2lACGaVEDg1" role="2uKbmB">
          <ref role="2Ss9cX" node="2aA5E1_HH6" />
        </node>
      </node>
    </node>
    <node concept="3Biw4V" id="4FZ6_9O1UcI" role="3BfUAV">
      <property role="TrG5h" value="storedAccounts" />
      <node concept="3iBYCm" id="1SvJ3Hz0jSf" role="3ix9CU">
        <node concept="2Ss9cW" id="1SvJ3Hz0k8_" role="3iBWmK">
          <ref role="2Ss9cX" node="2aA5E1$kmQ" />
        </node>
      </node>
    </node>
    <node concept="3Biw4V" id="4FZ6_9O1Uez" role="3BfUAV">
      <property role="TrG5h" value="storedOperations" />
      <node concept="3iBYCm" id="5kA80xxLqBe" role="3ix9CU">
        <node concept="2Ss9cW" id="5kA80xxLqTs" role="3iBWmK">
          <ref role="2Ss9cX" node="2aA5E1_HGC" />
        </node>
      </node>
    </node>
    <node concept="3DZied" id="1MgVVihrBlG" role="3u$SbN" />
    <node concept="1aduha" id="Dtlr0ZZKpt" role="2dXv$O">
      <node concept="1adJid" id="Dtlr0ZZKtF" role="1aduh9">
        <property role="TrG5h" value="inf" />
        <node concept="2Ss9cW" id="Dtlr0ZZKBy" role="2zM23F">
          <ref role="2Ss9cX" node="2aA5E1_HH6" />
        </node>
        <node concept="1QScDb" id="Dtlr0ZZKd_" role="1adJii">
          <node concept="FICQ5" id="Dtlr0ZZKg0" role="1QScD9" />
          <node concept="3BiLgO" id="Dtlr0ZZKbr" role="30czhm">
            <ref role="3BiLgL" node="4FZ6_9O1SLX" resolve="informationRequest" />
          </node>
        </node>
      </node>
      <node concept="2fGnzi" id="Dtlr0ZZJIC" role="1aduh9">
        <node concept="2fGnzd" id="3o3xT4d5yq4" role="2fGnxs">
          <node concept="30cPrO" id="3o3xT4d61dt" role="2fGnzS">
            <node concept="1QScDb" id="3o3xT4d61P7" role="30dEs_">
              <node concept="YK6gA" id="3o3xT4d62a6" role="1QScD9" />
              <node concept="5mhuz" id="3o3xT4d61xC" role="30czhm">
                <ref role="5mhpJ" node="1MgVVihszjJ" />
              </node>
            </node>
            <node concept="1QScDb" id="3o3xT4d5yWI" role="30dEsF">
              <node concept="3o_JK" id="3o3xT4d5zeu" role="1QScD9">
                <ref role="3o_JH" node="2aA5E1_HJM" />
              </node>
              <node concept="1adzI2" id="3o3xT4d5yFs" role="30czhm">
                <ref role="1adwt6" node="Dtlr0ZZKtF" resolve="inf" />
              </node>
            </node>
          </node>
          <node concept="1af_rf" id="3o3xT4d5zwh" role="2fGnzA">
            <ref role="1afhQb" node="3o3xT4d5U2H" resolve="sendResult" />
            <node concept="1QScDb" id="3o3xT4d5$3I" role="1afhQ5">
              <node concept="3o_JK" id="3o3xT4d5$lG" role="1QScD9">
                <ref role="3o_JH" node="2aA5E1_HK3" />
              </node>
              <node concept="1adzI2" id="3o3xT4d5zMf" role="30czhm">
                <ref role="1adwt6" node="Dtlr0ZZKtF" resolve="inf" />
              </node>
            </node>
            <node concept="3ix9CK" id="3o3xT4d5REM" role="1afhQ5">
              <node concept="3ix9CS" id="3o3xT4d5REN" role="3ix9CL">
                <property role="TrG5h" value="acc" />
                <node concept="2Ss9cW" id="3o3xT4d5WwK" role="3ix9CU">
                  <ref role="2Ss9cX" node="2aA5E1$kmQ" />
                </node>
              </node>
              <node concept="1aduha" id="3o3xT4d5Tij" role="3ix9pP">
                <node concept="3lL9fZ" id="3o3xT4d5Ezv" role="1aduh9">
                  <node concept="30bdrP" id="3o3xT4d5Ezw" role="3lRU4Y">
                    <property role="30bdrQ" value="sent account information" />
                  </node>
                </node>
                <node concept="1eBHpp" id="3o3xT4d5Ezx" role="1aduh9">
                  <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
                  <node concept="1g4zL5" id="3o3xT4d5Ezz" role="1eBHp4">
                    <node concept="2Ss9cW" id="5ZGapTFcHB7" role="2uKbmB">
                      <ref role="2Ss9cX" node="2aA5E1$kmQ" />
                    </node>
                  </node>
                  <node concept="3BiLgO" id="3o3xT4d5Ez$" role="1eBHp3">
                    <ref role="3BiLgL" node="4FZ6_9O1SLX" resolve="informationRequest" />
                  </node>
                  <node concept="3ix4Yz" id="3o3xT4d5KYE" role="1eBHp7">
                    <ref role="3ix4Yw" node="3o3xT4d5REN" resolve="acc" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2fGnzd" id="3o3xT4d62LK" role="2fGnxs">
          <node concept="30cPrO" id="3o3xT4d62LL" role="2fGnzS">
            <node concept="1QScDb" id="3o3xT4d62LM" role="30dEs_">
              <node concept="YK6gA" id="3o3xT4d62LN" role="1QScD9" />
              <node concept="5mhuz" id="3o3xT4d62LO" role="30czhm">
                <ref role="5mhpJ" node="1MgVVihszjR" />
              </node>
            </node>
            <node concept="1QScDb" id="3o3xT4d62LP" role="30dEsF">
              <node concept="3o_JK" id="3o3xT4d62LQ" role="1QScD9">
                <ref role="3o_JH" node="2aA5E1_HJM" />
              </node>
              <node concept="1adzI2" id="3o3xT4d62LR" role="30czhm">
                <ref role="1adwt6" node="Dtlr0ZZKtF" resolve="inf" />
              </node>
            </node>
          </node>
          <node concept="1af_rf" id="3o3xT4d62LS" role="2fGnzA">
            <ref role="1afhQb" node="3o3xT4d5U2H" resolve="sendResult" />
            <node concept="1QScDb" id="3o3xT4d62LT" role="1afhQ5">
              <node concept="3o_JK" id="3o3xT4d62LU" role="1QScD9">
                <ref role="3o_JH" node="2aA5E1_HK3" />
              </node>
              <node concept="1adzI2" id="3o3xT4d62LV" role="30czhm">
                <ref role="1adwt6" node="Dtlr0ZZKtF" resolve="inf" />
              </node>
            </node>
            <node concept="3ix9CK" id="3o3xT4d62LW" role="1afhQ5">
              <node concept="3ix9CS" id="3o3xT4d62LX" role="3ix9CL">
                <property role="TrG5h" value="acc" />
                <node concept="2Ss9cW" id="3o3xT4d62LY" role="3ix9CU">
                  <ref role="2Ss9cX" node="2aA5E1$kmQ" />
                </node>
              </node>
              <node concept="1aduha" id="3o3xT4d62LZ" role="3ix9pP">
                <node concept="3lL9fZ" id="3o3xT4d64vw" role="1aduh9">
                  <node concept="30bdrP" id="3o3xT4d64vx" role="3lRU4Y">
                    <property role="30bdrQ" value="sent list of operations" />
                  </node>
                </node>
                <node concept="1eBHpp" id="3o3xT4d64vy" role="1aduh9">
                  <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
                  <node concept="1g4zL5" id="3o3xT4d64vz" role="1eBHp4">
                    <node concept="2Ss9cW" id="5ZGapTFcI3j" role="2uKbmB">
                      <ref role="2Ss9cX" node="5ZGapTFaVSq" />
                    </node>
                  </node>
                  <node concept="3BiLgO" id="3o3xT4d64v$" role="1eBHp3">
                    <ref role="3BiLgL" node="4FZ6_9O1SLX" resolve="informationRequest" />
                  </node>
                  <node concept="2S399m" id="5ZGapTFaWxS" role="1eBHp7">
                    <node concept="1QScDb" id="1LLgSqPjXsD" role="2S399l">
                      <node concept="3izCyS" id="1LLgSqPjXtR" role="1QScD9">
                        <node concept="3izI60" id="1LLgSqPjXtS" role="3iAY4F">
                          <node concept="30cPrO" id="1LLgSqPjXAE" role="3izI61">
                            <node concept="1QScDb" id="5ZGapTFcGWZ" role="30dEs_">
                              <node concept="3o_JK" id="5ZGapTFcHaQ" role="1QScD9">
                                <ref role="3o_JH" node="2aA5E1$kn1" />
                              </node>
                              <node concept="3ix4Yz" id="5ZGapTFcGJb" role="30czhm">
                                <ref role="3ix4Yw" node="3o3xT4d62LX" resolve="acc" />
                              </node>
                            </node>
                            <node concept="1QScDb" id="1LLgSqPjXwA" role="30dEsF">
                              <node concept="3o_JK" id="1LLgSqPjXyC" role="1QScD9">
                                <ref role="3o_JH" node="2aA5E1_HIu" />
                              </node>
                              <node concept="3izPEI" id="1LLgSqPjXtU" role="30czhm" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3BiLgO" id="3o3xT4d5r3b" role="30czhm">
                        <ref role="3BiLgL" node="4FZ6_9O1Uez" resolve="storedOperations" />
                      </node>
                    </node>
                    <node concept="2Ss9cW" id="5ZGapTFaWP9" role="2S399n">
                      <ref role="2Ss9cX" node="5ZGapTFaVSq" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3KE$Bk" id="4FZ6_9O1SMR" role="19Gq0t">
      <ref role="21GGV3" node="2aA5E1wW8z" />
    </node>
    <node concept="3KE$Bk" id="Dtlr0ZZKZ7" role="19Gq0t">
      <ref role="21GGV3" node="7AVCbhi6SHs" />
    </node>
  </node>
  <node concept="3oGdb7" id="3o3xT4d0Haw">
    <property role="3GE5qa" value="server" />
    <property role="TrG5h" value="HandleRequest" />
    <node concept="3Biw4V" id="4FZ6_9O1TNh" role="3BfUAV">
      <property role="TrG5h" value="inputAccounts" />
      <node concept="3sNe5_" id="4FZ6_9O1TNo" role="3ix9CU">
        <node concept="3iBYCm" id="1SvJ3Hz0Dcv" role="3sNe5$">
          <node concept="2Ss9cW" id="1SvJ3Hz0Dl6" role="3iBWmK">
            <ref role="2Ss9cX" node="2aA5E1$kmQ" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3Biw4V" id="4FZ6_9O1TPH" role="3BfUAV">
      <property role="TrG5h" value="inputOperations" />
      <node concept="3sNe5_" id="4FZ6_9O1TPO" role="3ix9CU">
        <node concept="3iBYCm" id="5kA80xxLs5N" role="3sNe5$">
          <node concept="2Ss9cW" id="5kA80xxLsdX" role="3iBWmK">
            <ref role="2Ss9cX" node="2aA5E1_HGC" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3KE$Bk" id="3o3xT4d0Hc6" role="19Gq0t">
      <ref role="21GGV3" node="2aA5E1wW8z" />
    </node>
    <node concept="3KE$Bk" id="3o3xT4d0SIo" role="19Gq0t">
      <ref role="21GGV3" node="7AVCbhi6SHs" />
    </node>
    <node concept="3KE$Bk" id="3o3xT4d6akt" role="19Gq0t">
      <ref role="21GGV3" node="1MgVVihrBlE" resolve="GiveInformation" />
    </node>
    <node concept="3DZiec" id="3o3xT4d0Hc3" role="3u$SbN" />
    <node concept="1aduha" id="4qBaryQ13RY" role="2dXv$O">
      <node concept="2R7DHE" id="4qBaryQ13E1" role="1aduh9">
        <node concept="1fMSWk" id="4qBaryQ13E3" role="3NSspm">
          <property role="TrG5h" value="acc" />
          <node concept="1g4zL5" id="4qBaryQ13E5" role="2uPfNm">
            <node concept="2Ss9cW" id="4qBaryQ142K" role="2uKbmB">
              <ref role="2Ss9cX" node="2aA5E1_HGR" />
            </node>
          </node>
        </node>
        <node concept="1aduha" id="4qBaryQ147G" role="3NSspj">
          <node concept="3lL9fZ" id="4qBaryQ1494" role="1aduh9">
            <node concept="30dDZf" id="4qBaryQ28FA" role="3lRU4Y">
              <node concept="1QScDb" id="4qBaryQ2aj$" role="30dEs_">
                <node concept="3o_JK" id="4qBaryQ2aGJ" role="1QScD9">
                  <ref role="3o_JH" node="2aA5E1_HJ_" />
                </node>
                <node concept="1QScDb" id="4qBaryQ29wV" role="30czhm">
                  <node concept="FICQ5" id="4qBaryQ29TL" role="1QScD9" />
                  <node concept="1fQeJu" id="4qBaryQ297o" role="30czhm">
                    <ref role="1fQeJS" node="4qBaryQ13E3" resolve="acc" />
                  </node>
                </node>
              </node>
              <node concept="30bdrP" id="4qBaryQ149P" role="30dEsF">
                <property role="30bdrQ" value="received request to create a new account with name " />
              </node>
            </node>
          </node>
          <node concept="39w5ZF" id="1SvJ3Hz8olA" role="1aduh9">
            <node concept="pf3Wd" id="1SvJ3Hz8olB" role="pf3W8">
              <node concept="1af_rf" id="3o3xT4d0Zzt" role="pf3We">
                <ref role="1afhQb" node="3o3xT4d0VD6" resolve="createAccount" />
                <node concept="30bXRB" id="3o3xT4d0ZKr" role="1afhQ5">
                  <property role="30bXRw" value="0" />
                </node>
                <node concept="1fQeJu" id="3o3xT4d10aS" role="1afhQ5">
                  <ref role="1fQeJS" node="4qBaryQ13E3" resolve="acc" />
                </node>
              </node>
            </node>
            <node concept="1af_rf" id="3o3xT4d0XQk" role="39w5ZG">
              <ref role="1afhQb" node="3o3xT4d0VD6" resolve="createAccount" />
              <node concept="30dDZf" id="1SvJ3HzaxIO" role="1afhQ5">
                <node concept="30bXRB" id="1SvJ3HzaxIP" role="30dEs_">
                  <property role="30bXRw" value="1" />
                </node>
                <node concept="1QScDb" id="1SvJ3HzaxIQ" role="30dEsF">
                  <node concept="3o_JK" id="1SvJ3HzaxIR" role="1QScD9">
                    <ref role="3o_JH" node="2aA5E1$kn1" />
                  </node>
                  <node concept="1QScDb" id="1SvJ3HzaxIS" role="30czhm">
                    <node concept="3iB7bo" id="1SvJ3HzaxIT" role="1QScD9" />
                    <node concept="1QScDb" id="1SvJ3HzaxIU" role="30czhm">
                      <node concept="3sQ2Ir" id="1SvJ3HzaxIV" role="1QScD9" />
                      <node concept="3BiLgO" id="1SvJ3HzayGQ" role="30czhm">
                        <ref role="3BiLgL" node="4FZ6_9O1TNh" resolve="inputAccounts" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="1fQeJu" id="3o3xT4d0Z1S" role="1afhQ5">
                <ref role="1fQeJS" node="4qBaryQ13E3" resolve="acc" />
              </node>
            </node>
            <node concept="30d7iD" id="1SvJ3HzaMBA" role="39w5ZE">
              <node concept="1QScDb" id="1SvJ3HzaMBB" role="30dEsF">
                <node concept="1QScDb" id="1SvJ3HzaMBC" role="30czhm">
                  <node concept="3BiLgO" id="1SvJ3Hz8tHZ" role="30czhm">
                    <ref role="3BiLgL" node="4FZ6_9O1TNh" resolve="inputAccounts" />
                  </node>
                  <node concept="3sQ2Ir" id="1SvJ3Hz8trd" role="1QScD9" />
                </node>
                <node concept="3iB8M5" id="1SvJ3Hz8trb" role="1QScD9" />
              </node>
              <node concept="30bXRB" id="1SvJ3HzaMBD" role="30dEs_">
                <property role="30bXRw" value="0" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2zH6wq" id="4qBaryQ13YK" role="1aduh9" />
      <node concept="2R7DHE" id="4qBaryQ13Zy" role="1aduh9">
        <node concept="1fMSWk" id="4qBaryQ13Z$" role="3NSspm">
          <property role="TrG5h" value="op" />
          <node concept="1g4zL5" id="4qBaryQ13ZA" role="2uPfNm">
            <node concept="2Ss9cW" id="4qBaryQ143Y" role="2uKbmB">
              <ref role="2Ss9cX" node="2aA5E1_HGL" />
            </node>
          </node>
        </node>
        <node concept="1aduha" id="4qBaryQ14aB" role="3NSspj">
          <node concept="3lL9fZ" id="4qBaryQ14c3" role="1aduh9">
            <node concept="30dDZf" id="4qBaryQ1Sr4" role="3lRU4Y">
              <node concept="30bdrP" id="4qBaryQ14cQ" role="30dEsF">
                <property role="30bdrQ" value="received request to perform a " />
              </node>
              <node concept="2fGnzi" id="4qBaryQ1SIV" role="30dEs_">
                <node concept="2fGnzd" id="4qBaryQ1SIW" role="2fGnxs">
                  <node concept="30cPrO" id="4qBaryQ1SIX" role="2fGnzS">
                    <node concept="1QScDb" id="4qBaryQ1UVk" role="30dEs_">
                      <node concept="YK6gA" id="4qBaryQ1Vji" role="1QScD9" />
                      <node concept="5mhuz" id="4qBaryQ1SIY" role="30czhm">
                        <ref role="5mhpJ" node="1MgVVihszjB" />
                      </node>
                    </node>
                    <node concept="1QScDb" id="4qBaryQ1UbC" role="30dEsF">
                      <node concept="3o_JK" id="4qBaryQ1UzW" role="1QScD9">
                        <ref role="3o_JH" node="2aA5E1_HJa" />
                      </node>
                      <node concept="1QScDb" id="4qBaryQ1Tte" role="30czhm">
                        <node concept="FICQ5" id="4qBaryQ1TOx" role="1QScD9" />
                        <node concept="1fQeJu" id="4qBaryQ1T5L" role="30czhm">
                          <ref role="1fQeJS" node="4qBaryQ13Z$" resolve="op" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="30bdrP" id="4qBaryQ1SJ0" role="2fGnzA">
                    <property role="30bdrQ" value="deposit" />
                  </node>
                </node>
                <node concept="2fGnzd" id="4qBaryQ1SJ1" role="2fGnxs">
                  <node concept="30cPrO" id="4qBaryQ1SJ2" role="2fGnzS">
                    <node concept="1QScDb" id="4qBaryQ1XpL" role="30dEs_">
                      <node concept="YK6gA" id="4qBaryQ1XKQ" role="1QScD9" />
                      <node concept="5mhuz" id="4qBaryQ1SJ3" role="30czhm">
                        <ref role="5mhpJ" node="1MgVVihszjN" />
                      </node>
                    </node>
                    <node concept="1QScDb" id="4qBaryQ1WG9" role="30dEsF">
                      <node concept="3o_JK" id="4qBaryQ1X3e" role="1QScD9">
                        <ref role="3o_JH" node="2aA5E1_HJa" />
                      </node>
                      <node concept="1QScDb" id="4qBaryQ1VZJ" role="30czhm">
                        <node concept="FICQ5" id="4qBaryQ1Wmm" role="1QScD9" />
                        <node concept="1fQeJu" id="4qBaryQ1VEb" role="30czhm">
                          <ref role="1fQeJS" node="4qBaryQ13Z$" resolve="op" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="30bdrP" id="4qBaryQ1SJ5" role="2fGnzA">
                    <property role="30bdrQ" value="withdraw" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3lL9fZ" id="5ZGapTF2Jiy" role="1aduh9">
            <node concept="30dDZf" id="5ZGapTF2Lmd" role="3lRU4Y">
              <node concept="1QScDb" id="5ZGapTF2MrT" role="30dEs_">
                <node concept="3o_JK" id="5ZGapTF2MtD" role="1QScD9">
                  <ref role="3o_JH" node="2aA5E1_HJo" />
                </node>
                <node concept="1QScDb" id="5ZGapTF2Mpb" role="30czhm">
                  <node concept="FICQ5" id="5ZGapTF2Mr2" role="1QScD9" />
                  <node concept="1fQeJu" id="5ZGapTF2Moy" role="30czhm">
                    <ref role="1fQeJS" node="4qBaryQ13Z$" resolve="op" />
                  </node>
                </node>
              </node>
              <node concept="30bdrP" id="5ZGapTF2KkK" role="30dEsF">
                <property role="30bdrQ" value="amount is " />
              </node>
            </node>
          </node>
          <node concept="3lL9fZ" id="5ZGapTF2Nxl" role="1aduh9">
            <node concept="30dDZf" id="5ZGapTF2PAz" role="3lRU4Y">
              <node concept="1QScDb" id="5ZGapTF2QI0" role="30dEs_">
                <node concept="3o_JK" id="5ZGapTF2QIO" role="1QScD9">
                  <ref role="3o_JH" node="2aA5E1_HIT" />
                </node>
                <node concept="1QScDb" id="5ZGapTF2QEm" role="30czhm">
                  <node concept="FICQ5" id="5ZGapTF2QH9" role="1QScD9" />
                  <node concept="1fQeJu" id="5ZGapTF2QDH" role="30czhm">
                    <ref role="1fQeJS" node="4qBaryQ13Z$" resolve="op" />
                  </node>
                </node>
              </node>
              <node concept="30bdrP" id="5ZGapTF2Ozm" role="30dEsF">
                <property role="30bdrQ" value="account id is " />
              </node>
            </node>
          </node>
          <node concept="2zH6wq" id="5ZGapTF2QJA" role="1aduh9" />
          <node concept="1af_rf" id="3o3xT4d4csW" role="1aduh9">
            <ref role="1afhQb" node="3o3xT4d19BA" resolve="performOperation" />
            <node concept="1fQeJu" id="3o3xT4d4diz" role="1afhQ5">
              <ref role="1fQeJS" node="4qBaryQ13Z$" resolve="op" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2zH6wq" id="4qBaryQ1404" role="1aduh9" />
      <node concept="2R7DHE" id="4qBaryQ1419" role="1aduh9">
        <node concept="1fMSWk" id="4qBaryQ141b" role="3NSspm">
          <property role="TrG5h" value="inf" />
          <node concept="1g4zL5" id="4qBaryQ141d" role="2uPfNm">
            <node concept="2Ss9cW" id="4qBaryQ145k" role="2uKbmB">
              <ref role="2Ss9cX" node="2aA5E1_HH6" />
            </node>
          </node>
        </node>
        <node concept="1aduha" id="4qBaryQ14dE" role="3NSspj">
          <node concept="3lL9fZ" id="4qBaryQ14fg" role="1aduh9">
            <node concept="30dDZf" id="4qBaryQ1ZcJ" role="3lRU4Y">
              <node concept="30bdrP" id="4qBaryQ14g8" role="30dEsF">
                <property role="30bdrQ" value="received request to show the " />
              </node>
              <node concept="2fGnzi" id="4qBaryQ1ZyW" role="30dEs_">
                <node concept="2fGnzd" id="4qBaryQ1ZyX" role="2fGnxs">
                  <node concept="30cPrO" id="4qBaryQ1ZyY" role="2fGnzS">
                    <node concept="1QScDb" id="4qBaryQ27tz" role="30dEs_">
                      <node concept="YK6gA" id="4qBaryQ27RC" role="1QScD9" />
                      <node concept="5mhuz" id="4qBaryQ1ZyZ" role="30czhm">
                        <ref role="5mhpJ" node="1MgVVihszjR" />
                      </node>
                    </node>
                    <node concept="1QScDb" id="4qBaryQ218H" role="30dEsF">
                      <node concept="3o_JK" id="4qBaryQ21yg" role="1QScD9">
                        <ref role="3o_JH" node="2aA5E1_HJM" />
                      </node>
                      <node concept="1QScDb" id="4qBaryQ20kQ" role="30czhm">
                        <node concept="FICQ5" id="4qBaryQ20IC" role="1QScD9" />
                        <node concept="1fQeJu" id="4qBaryQ1ZW6" role="30czhm">
                          <ref role="1fQeJS" node="4qBaryQ141b" resolve="inf" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="30bdrP" id="4qBaryQ1Zz1" role="2fGnzA">
                    <property role="30bdrQ" value="list of operations" />
                  </node>
                </node>
                <node concept="2fGnzd" id="4qBaryQ1Zz2" role="2fGnxs">
                  <node concept="30cPrO" id="4qBaryQ1Zz3" role="2fGnzS">
                    <node concept="1QScDb" id="4qBaryQ26Fp" role="30dEs_">
                      <node concept="YK6gA" id="4qBaryQ274F" role="1QScD9" />
                      <node concept="5mhuz" id="4qBaryQ1Zz4" role="30czhm">
                        <ref role="5mhpJ" node="1MgVVihszjJ" />
                      </node>
                    </node>
                    <node concept="1QScDb" id="4qBaryQ25UK" role="30dEsF">
                      <node concept="3o_JK" id="4qBaryQ26jm" role="1QScD9">
                        <ref role="3o_JH" node="2aA5E1_HJM" />
                      </node>
                      <node concept="1QScDb" id="4qBaryQ259b" role="30czhm">
                        <node concept="FICQ5" id="4qBaryQ25y0" role="1QScD9" />
                        <node concept="1fQeJu" id="4qBaryQ24KE" role="30czhm">
                          <ref role="1fQeJS" node="4qBaryQ141b" resolve="inf" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="30bdrP" id="4qBaryQ1Zz6" role="2fGnzA">
                    <property role="30bdrQ" value="balance" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2zH6wq" id="3o3xT4d6rdq" role="1aduh9" />
          <node concept="p0Vrc" id="3o3xT4d68BD" role="1aduh9">
            <node concept="3Qnb9i" id="3o3xT4d6cqw" role="3QnlHL">
              <ref role="3Qnb9j" node="1MgVVihrBlE" resolve="GiveInformation" />
              <node concept="1fQeJu" id="3o3xT4d6dHE" role="3Qnb9l">
                <ref role="1fQeJS" node="4qBaryQ141b" resolve="inf" />
              </node>
              <node concept="1QScDb" id="3o3xT4d6fqK" role="3Qnb9l">
                <node concept="3sQ2Ir" id="3o3xT4d6gin" role="1QScD9" />
                <node concept="3BiLgO" id="3o3xT4d6e$b" role="30czhm">
                  <ref role="3BiLgL" node="4FZ6_9O1TNh" resolve="inputAccounts" />
                </node>
              </node>
              <node concept="1QScDb" id="3o3xT4d6lAy" role="3Qnb9l">
                <node concept="3sQ2Ir" id="3o3xT4d6mws" role="1QScD9" />
                <node concept="3BiLgO" id="3o3xT4d6kHK" role="30czhm">
                  <ref role="3BiLgL" node="4FZ6_9O1TPH" resolve="inputOperations" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1aga60" id="3o3xT4d0VD6" role="3KDhAD">
      <property role="TrG5h" value="createAccount" />
      <node concept="1aduha" id="3o3xT4d10QM" role="1ahQXP">
        <node concept="39w5ZF" id="3o3xT4d3hr0" role="1aduh9">
          <node concept="pf3Wd" id="3o3xT4d3hr1" role="pf3W8">
            <node concept="UmHTt" id="3o3xT4d3q3e" role="pf3We" />
          </node>
          <node concept="UmaEC" id="3o3xT4d3ijc" role="39w5ZE">
            <node concept="1afdae" id="3o3xT4d3kUE" role="UmaED">
              <ref role="1afue_" node="3o3xT4d0WFV" resolve="id" />
            </node>
            <node concept="pfQqD" id="3o3xT4d3lMv" role="pfQ1b">
              <property role="pfQqC" value="counter" />
            </node>
          </node>
          <node concept="1aduha" id="3o3xT4d3nuD" role="39w5ZG">
            <node concept="1adJid" id="3o3xT4d33_8" role="1aduh9">
              <property role="TrG5h" value="newAcc" />
              <node concept="2S399m" id="6$6nD_D0GeN" role="1adJii">
                <node concept="1ZmhP4" id="3o3xT4d3rPs" role="2S399l">
                  <ref role="1ZmhP3" node="3o3xT4d3ijc" resolve="counter" />
                </node>
                <node concept="1QScDb" id="6$6nD_D0GeP" role="2S399l">
                  <node concept="3o_JK" id="6$6nD_D0GeQ" role="1QScD9">
                    <ref role="3o_JH" node="2aA5E1_HJ_" />
                  </node>
                  <node concept="1QScDb" id="6$6nD_D0Hzn" role="30czhm">
                    <node concept="FICQ5" id="3o3xT4d2E9y" role="1QScD9" />
                    <node concept="1afdae" id="6$6nD_DihKm" role="30czhm">
                      <ref role="1afue_" node="3o3xT4d0X0$" resolve="accReq" />
                    </node>
                  </node>
                </node>
                <node concept="_emDc" id="6$6nD_D0GeS" role="2S399l">
                  <ref role="_emDf" node="7AVCbhi0Vds" />
                </node>
                <node concept="2Ss9cW" id="6$6nD_D0GeT" role="2S399n">
                  <ref role="2Ss9cX" node="2aA5E1$kmQ" />
                </node>
              </node>
            </node>
            <node concept="1QScDb" id="3o3xT4d2$P5" role="1aduh9">
              <node concept="3sPC8h" id="3o3xT4d2_JE" role="1QScD9">
                <node concept="1QScDb" id="6$6nD_D0DVv" role="3sPC8l">
                  <node concept="3j5BQN" id="6$6nD_D0DVw" role="30czhm" />
                  <node concept="2iGZtc" id="6$6nD_D0DVx" role="1QScD9">
                    <node concept="1adzI2" id="3o3xT4d38DT" role="26Ft6C">
                      <ref role="1adwt6" node="3o3xT4d33_8" resolve="newAcc" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3BiLgO" id="3o3xT4d2zXv" role="30czhm">
                <ref role="3BiLgL" node="4FZ6_9O1TNh" resolve="inputAccounts" />
              </node>
            </node>
            <node concept="3lL9fZ" id="6$6nD_DjPEu" role="1aduh9">
              <node concept="30dDZf" id="6$6nD_DjPEw" role="3lRU4Y">
                <node concept="30bdrP" id="6$6nD_DjPEx" role="30dEsF">
                  <property role="30bdrQ" value="created new account with id " />
                </node>
                <node concept="1ZmhP4" id="3o3xT4d3qW_" role="30dEs_">
                  <ref role="1ZmhP3" node="3o3xT4d3ijc" resolve="counter" />
                </node>
              </node>
            </node>
            <node concept="3lL9fZ" id="5ZGapTF1Dhr" role="1aduh9">
              <node concept="30bdrP" id="5ZGapTF1Efr" role="3lRU4Y">
                <property role="30bdrQ" value="current accounts" />
              </node>
            </node>
            <node concept="3lL9fZ" id="5ZGapTF1KaT" role="1aduh9">
              <node concept="1QScDb" id="5ZGapTF1M8g" role="3lRU4Y">
                <node concept="3sQ2Ir" id="5ZGapTF1N8q" role="1QScD9" />
                <node concept="3BiLgO" id="5ZGapTF1La5" role="30czhm">
                  <ref role="3BiLgL" node="4FZ6_9O1TNh" resolve="inputAccounts" />
                </node>
              </node>
            </node>
            <node concept="2zH6wq" id="7IIeq5wN2fa" role="1aduh9" />
            <node concept="1eBHpp" id="7IIeq5wMXV4" role="1aduh9">
              <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
              <node concept="1adzI2" id="7IIeq5wN0wb" role="1eBHp7">
                <ref role="1adwt6" node="3o3xT4d33_8" resolve="newAcc" />
              </node>
              <node concept="1g4zL5" id="7IIeq5wMXV8" role="1eBHp4">
                <node concept="2Ss9cW" id="7IIeq5wN360" role="2uKbmB">
                  <ref role="2Ss9cX" node="2aA5E1$kmQ" resolve="Account" />
                </node>
              </node>
              <node concept="1afdae" id="7IIeq5wMYMl" role="1eBHp3">
                <ref role="1afue_" node="3o3xT4d0X0$" resolve="accReq" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1ahQXy" id="3o3xT4d0WFV" role="1ahQWs">
        <property role="TrG5h" value="id" />
        <node concept="Uns6S" id="3o3xT4d3jah" role="3ix9CU">
          <node concept="30bXR$" id="3o3xT4d3k2P" role="Uns6T" />
        </node>
      </node>
      <node concept="1ahQXy" id="3o3xT4d0X0$" role="1ahQWs">
        <property role="TrG5h" value="accReq" />
        <node concept="1g4zL5" id="3o3xT4d0Xk_" role="3ix9CU">
          <node concept="2Ss9cW" id="3o3xT4d0XkT" role="2uKbmB">
            <ref role="2Ss9cX" node="2aA5E1_HGR" />
          </node>
        </node>
      </node>
      <node concept="2lgajX" id="3o3xT4d2Kj5" role="28QfE6" />
    </node>
    <node concept="_ixoA" id="3o3xT4d193K" role="3KDhAD" />
    <node concept="1aga60" id="3o3xT4d19BA" role="3KDhAD">
      <property role="TrG5h" value="performOperation" />
      <node concept="39w5ZF" id="3o3xT4d1mgF" role="1ahQXP">
        <node concept="pf3Wd" id="3o3xT4d1mgG" role="pf3W8">
          <node concept="1af_rf" id="3o3xT4d1BUK" role="pf3We">
            <ref role="1afhQb" node="3o3xT4d1nnm" resolve="checkAccount" />
            <node concept="1afdae" id="3o3xT4d1C$H" role="1afhQ5">
              <ref role="1afue_" node="3o3xT4d19TI" resolve="opReq" />
            </node>
          </node>
        </node>
        <node concept="30d6GJ" id="3o3xT4d1mm3" role="39w5ZE">
          <node concept="30bXRB" id="3o3xT4d1mm4" role="30dEs_">
            <property role="30bXRw" value="0" />
          </node>
          <node concept="1QScDb" id="3o3xT4d1mm5" role="30dEsF">
            <node concept="3o_JK" id="3o3xT4d1mm6" role="1QScD9">
              <ref role="3o_JH" node="2aA5E1_HJo" />
            </node>
            <node concept="1QScDb" id="3o3xT4d1mm7" role="30czhm">
              <node concept="FICQ5" id="3o3xT4d1mm8" role="1QScD9" />
              <node concept="1afdae" id="3o3xT4d1mm9" role="30czhm">
                <ref role="1afue_" node="3o3xT4d19TI" resolve="opReq" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1af_rf" id="3o3xT4d1myd" role="39w5ZG">
          <ref role="1afhQb" node="7IIeq5wNeJC" resolve="sendOpReqProblem" />
          <node concept="1afdae" id="3o3xT4d1mye" role="1afhQ5">
            <ref role="1afue_" node="3o3xT4d19TI" resolve="opReq" />
          </node>
          <node concept="5mhuz" id="3o3xT4d1myf" role="1afhQ5">
            <ref role="5mhpJ" node="2umCVQRk8Yc" />
          </node>
        </node>
      </node>
      <node concept="1ahQXy" id="3o3xT4d19TI" role="1ahQWs">
        <property role="TrG5h" value="opReq" />
        <node concept="1g4zL5" id="3o3xT4d19U0" role="3ix9CU">
          <node concept="2Ss9cW" id="3o3xT4d19Uk" role="2uKbmB">
            <ref role="2Ss9cX" node="2aA5E1_HGL" />
          </node>
        </node>
      </node>
      <node concept="2lgajX" id="3o3xT4d22_s" role="28QfE6" />
    </node>
    <node concept="_ixoA" id="3o3xT4d1mNl" role="3KDhAD" />
    <node concept="1aga60" id="3o3xT4d1nnm" role="3KDhAD">
      <property role="TrG5h" value="checkAccount" />
      <node concept="1aduha" id="3o3xT4d1wl_" role="1ahQXP">
        <node concept="1adJid" id="3o3xT4d1ydO" role="1aduh9">
          <property role="TrG5h" value="accOpt" />
          <node concept="1QScDb" id="2umCVQRj54J" role="1adJii">
            <node concept="1HmgMX" id="5kA80xxLz08" role="1QScD9">
              <node concept="3izI60" id="5kA80xxLz09" role="3iAY4F">
                <node concept="30cPrO" id="5kA80xxL_6w" role="3izI61">
                  <node concept="1QScDb" id="5kA80xxLzOg" role="30dEsF">
                    <node concept="3o_JK" id="5kA80xxL$AY" role="1QScD9">
                      <ref role="3o_JH" node="2aA5E1$kn1" />
                    </node>
                    <node concept="3izPEI" id="5kA80xxLz0b" role="30czhm" />
                  </node>
                  <node concept="1QScDb" id="1SvJ3HzoEWP" role="30dEs_">
                    <node concept="3o_JK" id="1SvJ3HzoFF2" role="1QScD9">
                      <ref role="3o_JH" node="2aA5E1_HIT" />
                    </node>
                    <node concept="1QScDb" id="3o3xT4cYbXv" role="30czhm">
                      <node concept="FICQ5" id="3o3xT4cYd2N" role="1QScD9" />
                      <node concept="1afdae" id="1SvJ3HzoCN_" role="30czhm">
                        <ref role="1afue_" node="3o3xT4d1rK2" resolve="opReq" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="1QScDb" id="3o3xT4d7KNf" role="30czhm">
              <node concept="3sQ2Ir" id="3o3xT4d7LGv" role="1QScD9" />
              <node concept="3BiLgO" id="5ZGapTFe7OD" role="30czhm">
                <ref role="3BiLgL" node="4FZ6_9O1TNh" resolve="inputAccounts" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39w5ZF" id="3o3xT4d1o8Z" role="1aduh9">
          <node concept="pf3Wd" id="3o3xT4d1o90" role="pf3W8">
            <node concept="1af_rf" id="3o3xT4d1snw" role="pf3We">
              <ref role="1afhQb" node="7IIeq5wNeJC" resolve="sendOpReqProblem" />
              <node concept="1afdae" id="3o3xT4d1sYg" role="1afhQ5">
                <ref role="1afue_" node="3o3xT4d1rK2" resolve="opReq" />
              </node>
              <node concept="5mhuz" id="3o3xT4d1ujN" role="1afhQ5">
                <ref role="5mhpJ" node="2umCVQRk8Ye" />
              </node>
            </node>
          </node>
          <node concept="1af_rf" id="3o3xT4d1De_" role="39w5ZG">
            <ref role="1afhQb" node="3o3xT4d1Bgj" resolve="checkFunds" />
            <node concept="1afdae" id="3o3xT4d1DSK" role="1afhQ5">
              <ref role="1afue_" node="3o3xT4d1rK2" resolve="opReq" />
            </node>
            <node concept="1ZmhP4" id="3o3xT4d3wjS" role="1afhQ5">
              <ref role="1ZmhP3" node="1SvJ3Hzmi8D" resolve="acc" />
            </node>
          </node>
          <node concept="UmaEC" id="1SvJ3Hzmi8D" role="39w5ZE">
            <node concept="pfQqD" id="1SvJ3HzmX_V" role="pfQ1b">
              <property role="pfQqC" value="acc" />
            </node>
            <node concept="1adzI2" id="3o3xT4d1_kY" role="UmaED">
              <ref role="1adwt6" node="3o3xT4d1ydO" resolve="accOpt" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1ahQXy" id="3o3xT4d1rK2" role="1ahQWs">
        <property role="TrG5h" value="opReq" />
        <node concept="1g4zL5" id="3o3xT4d1smJ" role="3ix9CU">
          <node concept="2Ss9cW" id="3o3xT4d1smZ" role="2uKbmB">
            <ref role="2Ss9cX" node="2aA5E1_HGL" />
          </node>
        </node>
      </node>
      <node concept="2lgajX" id="3o3xT4d300I" role="28QfE6" />
    </node>
    <node concept="_ixoA" id="3o3xT4d1a12" role="3KDhAD" />
    <node concept="1aga60" id="3o3xT4d1Bgj" role="3KDhAD">
      <property role="TrG5h" value="checkFunds" />
      <node concept="1aduha" id="3o3xT4d1E$w" role="1ahQXP">
        <node concept="1adJid" id="3o3xT4deO2X" role="1aduh9">
          <property role="TrG5h" value="opContent" />
          <node concept="1QScDb" id="3o3xT4deSjb" role="1adJii">
            <node concept="FICQ5" id="3o3xT4deTp2" role="1QScD9" />
            <node concept="1afdae" id="3o3xT4deRez" role="30czhm">
              <ref role="1afue_" node="3o3xT4d1BUa" resolve="opReq" />
            </node>
          </node>
        </node>
        <node concept="1adJid" id="1SvJ3HzlDeO" role="1aduh9">
          <property role="TrG5h" value="balance" />
          <node concept="39w5ZF" id="1SvJ3Hzm3Pb" role="1adJii">
            <node concept="pf3Wd" id="1SvJ3Hzm3Pc" role="pf3W8">
              <node concept="30dDZf" id="1SvJ3Hzm9dC" role="pf3We">
                <node concept="1QScDb" id="3o3xT4d3_GK" role="30dEsF">
                  <node concept="3o_JK" id="3o3xT4d3AAo" role="1QScD9">
                    <ref role="3o_JH" node="2aA5E1$knz" />
                  </node>
                  <node concept="1afdae" id="1SvJ3Hzm9dG" role="30czhm">
                    <ref role="1afue_" node="3o3xT4d3sLi" resolve="account" />
                  </node>
                </node>
                <node concept="1QScDb" id="1SvJ3HzoRrs" role="30dEs_">
                  <node concept="3o_JK" id="1SvJ3HzoSaf" role="1QScD9">
                    <ref role="3o_JH" node="2aA5E1_HJo" />
                  </node>
                  <node concept="1adzI2" id="3o3xT4deYFz" role="30czhm">
                    <ref role="1adwt6" node="3o3xT4deO2X" resolve="opContent" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="30cPrO" id="1SvJ3Hzm6uU" role="39w5ZE">
              <node concept="1QScDb" id="1SvJ3Hzmbjy" role="30dEs_">
                <node concept="YK6gA" id="1SvJ3HzmbZM" role="1QScD9" />
                <node concept="5mhuz" id="1SvJ3Hzm79S" role="30czhm">
                  <ref role="5mhpJ" node="1MgVVihszjN" />
                </node>
              </node>
              <node concept="1QScDb" id="1SvJ3Hzm59m" role="30dEsF">
                <node concept="3o_JK" id="1SvJ3Hzm5Oo" role="1QScD9">
                  <ref role="3o_JH" node="2aA5E1_HJa" />
                </node>
                <node concept="1adzI2" id="3o3xT4deVxe" role="30czhm">
                  <ref role="1adwt6" node="3o3xT4deO2X" resolve="opContent" />
                </node>
              </node>
            </node>
            <node concept="1aduha" id="1SvJ3HzqpzQ" role="39w5ZG">
              <node concept="1af_rf" id="1SvJ3HzlF4f" role="1aduh9">
                <ref role="1afhQb" node="1SvJ3HzqYCv" resolve="getAmount" />
                <node concept="1QScDb" id="3o3xT4d3yYt" role="1afhQ5">
                  <node concept="3o_JK" id="3o3xT4d3zTh" role="1QScD9">
                    <ref role="3o_JH" node="2aA5E1$knz" />
                  </node>
                  <node concept="1afdae" id="3o3xT4d3y53" role="30czhm">
                    <ref role="1afue_" node="3o3xT4d3sLi" resolve="account" />
                  </node>
                </node>
                <node concept="1QScDb" id="1SvJ3HzoOyT" role="1afhQ5">
                  <node concept="3o_JK" id="1SvJ3HzoPhR" role="1QScD9">
                    <ref role="3o_JH" node="2aA5E1_HJo" />
                  </node>
                  <node concept="1adzI2" id="3o3xT4deW_0" role="30czhm">
                    <ref role="1adwt6" node="3o3xT4deO2X" resolve="opContent" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2zH6wq" id="3o3xT4d1KCc" role="1aduh9" />
        <node concept="39w5ZF" id="3o3xT4d1FVz" role="1aduh9">
          <node concept="pf3Wd" id="3o3xT4d1FV$" role="pf3W8">
            <node concept="1af_rf" id="3o3xT4d1HV8" role="pf3We">
              <ref role="1afhQb" node="7IIeq5wNeJC" resolve="sendOpReqProblem" />
              <node concept="1afdae" id="3o3xT4d1I_1" role="1afhQ5">
                <ref role="1afue_" node="3o3xT4d1BUa" resolve="opReq" />
              </node>
              <node concept="5mhuz" id="3o3xT4d1JTI" role="1afhQ5">
                <ref role="5mhpJ" node="2umCVQRk8Yg" />
              </node>
            </node>
          </node>
          <node concept="UmaEC" id="3o3xT4d1GAo" role="39w5ZE">
            <node concept="1adzI2" id="3o3xT4d1XGg" role="UmaED">
              <ref role="1adwt6" node="1SvJ3HzlDeO" resolve="balance" />
            </node>
            <node concept="pfQqD" id="3o3xT4d2f_b" role="pfQ1b">
              <property role="pfQqC" value="bal" />
            </node>
          </node>
          <node concept="1aduha" id="3o3xT4d1Hg8" role="39w5ZG">
            <node concept="1adJid" id="3o3xT4d4muS" role="1aduh9">
              <property role="TrG5h" value="op" />
              <node concept="2S399m" id="48m2ln4b6Dg" role="1adJii">
                <node concept="2Ss9cW" id="48m2ln4b6Do" role="2S399n">
                  <ref role="2Ss9cX" node="2aA5E1_HGC" />
                </node>
                <node concept="1QScDb" id="48m2ln4b6E4" role="2S399l">
                  <node concept="3o_JK" id="48m2ln4b6EC" role="1QScD9">
                    <ref role="3o_JH" node="2aA5E1_HJa" />
                  </node>
                  <node concept="1adzI2" id="3o3xT4df2QP" role="30czhm">
                    <ref role="1adwt6" node="3o3xT4deO2X" resolve="opContent" />
                  </node>
                </node>
                <node concept="1QScDb" id="48m2ln4b6YY" role="2S399l">
                  <node concept="3o_JK" id="48m2ln4b78K" role="1QScD9">
                    <ref role="3o_JH" node="2aA5E1_HJo" />
                  </node>
                  <node concept="1adzI2" id="3o3xT4df0LW" role="30czhm">
                    <ref role="1adwt6" node="3o3xT4deO2X" resolve="opContent" />
                  </node>
                </node>
                <node concept="1ZmhP4" id="3o3xT4d3YfJ" role="2S399l">
                  <ref role="1ZmhP3" node="3o3xT4d1GAo" resolve="bal" />
                </node>
                <node concept="1QScDb" id="48m2ln4b7Ut" role="2S399l">
                  <node concept="3o_JK" id="48m2ln4b84w" role="1QScD9">
                    <ref role="3o_JH" node="2aA5E1_HIT" />
                  </node>
                  <node concept="1adzI2" id="3o3xT4df4VZ" role="30czhm">
                    <ref role="1adwt6" node="3o3xT4deO2X" resolve="opContent" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1af_rf" id="3o3xT4d26_5" role="1aduh9">
              <ref role="1afhQb" node="1LLgSqPjtmN" resolve="updateAccount" />
              <node concept="1afdae" id="3o3xT4d3LqL" role="1afhQ5">
                <ref role="1afue_" node="3o3xT4d3sLi" resolve="account" />
              </node>
              <node concept="1ZmhP4" id="3o3xT4d2e$A" role="1afhQ5">
                <ref role="1ZmhP3" node="3o3xT4d1GAo" resolve="bal" />
              </node>
            </node>
            <node concept="1QScDb" id="5ZGapTFfmc7" role="1aduh9">
              <node concept="3sPC8h" id="5ZGapTFfnav" role="1QScD9">
                <node concept="1QScDb" id="5ZGapTFfr2V" role="3sPC8l">
                  <node concept="2iGZtc" id="5ZGapTFfs4d" role="1QScD9">
                    <node concept="1adzI2" id="5ZGapTFft3u" role="26Ft6C">
                      <ref role="1adwt6" node="3o3xT4d4muS" resolve="op" />
                    </node>
                  </node>
                  <node concept="3j5BQN" id="5ZGapTFfq4i" role="30czhm" />
                </node>
              </node>
              <node concept="3BiLgO" id="5ZGapTFfleU" role="30czhm">
                <ref role="3BiLgL" node="4FZ6_9O1TPH" resolve="inputOperations" />
              </node>
            </node>
            <node concept="2zH6wq" id="5ZGapTFfY5B" role="1aduh9" />
            <node concept="3lL9fZ" id="5ZGapTF1O8v" role="1aduh9">
              <node concept="30bdrP" id="5ZGapTF1P8z" role="3lRU4Y">
                <property role="30bdrQ" value="updated operations" />
              </node>
            </node>
            <node concept="3lL9fZ" id="5ZGapTF1R6Z" role="1aduh9">
              <node concept="1QScDb" id="5ZGapTFfUnQ" role="3lRU4Y">
                <node concept="3sQ2Ir" id="5ZGapTFfVk8" role="1QScD9" />
                <node concept="3BiLgO" id="5ZGapTFfTtu" role="30czhm">
                  <ref role="3BiLgL" node="4FZ6_9O1TPH" resolve="inputOperations" />
                </node>
              </node>
            </node>
            <node concept="3lL9fZ" id="5ZGapTF3o9Q" role="1aduh9">
              <node concept="30bdrP" id="5ZGapTF3o9R" role="3lRU4Y">
                <property role="30bdrQ" value="updated accounts" />
              </node>
            </node>
            <node concept="3lL9fZ" id="5ZGapTF3o9S" role="1aduh9">
              <node concept="1QScDb" id="5ZGapTF3o9T" role="3lRU4Y">
                <node concept="3sQ2Ir" id="5ZGapTF3o9U" role="1QScD9" />
                <node concept="3BiLgO" id="5ZGapTF78L5" role="30czhm">
                  <ref role="3BiLgL" node="4FZ6_9O1TNh" resolve="inputAccounts" />
                </node>
              </node>
            </node>
            <node concept="2zH6wq" id="5ZGapTF1V9M" role="1aduh9" />
            <node concept="1eBHpp" id="22Cof6W8Wfk" role="1aduh9">
              <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
              <node concept="1g4zL5" id="22Cof6W8Wfo" role="1eBHp4">
                <node concept="2Ss9cW" id="22Cof6W8WfK" role="2uKbmB">
                  <ref role="2Ss9cX" node="2aA5E1_HGC" />
                </node>
              </node>
              <node concept="1afdae" id="3o3xT4d4k3z" role="1eBHp3">
                <ref role="1afue_" node="3o3xT4d1BUa" resolve="opReq" />
              </node>
              <node concept="1adzI2" id="3o3xT4d4sWK" role="1eBHp7">
                <ref role="1adwt6" node="3o3xT4d4muS" resolve="op" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1ahQXy" id="3o3xT4d1BUa" role="1ahQWs">
        <property role="TrG5h" value="opReq" />
        <node concept="1g4zL5" id="3o3xT4d1DdL" role="3ix9CU">
          <node concept="2Ss9cW" id="3o3xT4d1De1" role="2uKbmB">
            <ref role="2Ss9cX" node="2aA5E1_HGL" />
          </node>
        </node>
      </node>
      <node concept="1ahQXy" id="3o3xT4d3sLi" role="1ahQWs">
        <property role="TrG5h" value="account" />
        <node concept="2Ss9cW" id="3o3xT4d3uyu" role="3ix9CU">
          <ref role="2Ss9cX" node="2aA5E1$kmQ" />
        </node>
      </node>
      <node concept="2lgajX" id="3o3xT4d2XrA" role="28QfE6" />
    </node>
    <node concept="_ixoA" id="3o3xT4d1r4A" role="3KDhAD" />
    <node concept="1aga60" id="1SvJ3HzqYCv" role="3KDhAD">
      <property role="TrG5h" value="getAmount" />
      <node concept="1aduha" id="1SvJ3Hzr5So" role="1ahQXP">
        <node concept="1adJid" id="1SvJ3Hzr6U4" role="1aduh9">
          <property role="TrG5h" value="newBalance" />
          <node concept="30dvUo" id="1SvJ3Hzrda1" role="1adJii">
            <node concept="1afdae" id="1SvJ3HzrdFg" role="30dEs_">
              <ref role="1afue_" node="1SvJ3Hzr3kE" resolve="amountToWithdraw" />
            </node>
            <node concept="1afdae" id="1SvJ3HzrcBX" role="30dEsF">
              <ref role="1afue_" node="1SvJ3Hzr1N3" resolve="oldBalance" />
            </node>
          </node>
        </node>
        <node concept="39w5ZF" id="1SvJ3Hzr5nx" role="1aduh9">
          <node concept="pf3Wd" id="1SvJ3Hzr5ny" role="pf3W8">
            <node concept="1adzI2" id="1SvJ3Hzrc6T" role="pf3We">
              <ref role="1adwt6" node="1SvJ3Hzr6U4" resolve="newBalance" />
            </node>
          </node>
          <node concept="30d6GJ" id="1SvJ3Hzrb1e" role="39w5ZE">
            <node concept="30bXRB" id="1SvJ3Hzrb1k" role="30dEs_">
              <property role="30bXRw" value="0" />
            </node>
            <node concept="1adzI2" id="1SvJ3Hzrawn" role="30dEsF">
              <ref role="1adwt6" node="1SvJ3Hzr6U4" resolve="newBalance" />
            </node>
          </node>
          <node concept="UmHTt" id="1SvJ3Hzrbyy" role="39w5ZG" />
        </node>
      </node>
      <node concept="1ahQXy" id="1SvJ3Hzr1N3" role="1ahQWs">
        <property role="TrG5h" value="oldBalance" />
        <node concept="30bXLL" id="1SvJ3Hzr3kC" role="3ix9CU" />
      </node>
      <node concept="1ahQXy" id="1SvJ3Hzr3kE" role="1ahQWs">
        <property role="TrG5h" value="amountToWithdraw" />
        <node concept="30bXLL" id="1SvJ3HzreRm" role="3ix9CU" />
      </node>
    </node>
    <node concept="_ixoA" id="3o3xT4dc0c$" role="3KDhAD" />
    <node concept="1aga60" id="1LLgSqPjtmN" role="3KDhAD">
      <property role="TrG5h" value="updateAccount" />
      <property role="0Rz4W" value="522300505" />
      <node concept="1aduha" id="1LLgSqPjtmO" role="1ahQXP">
        <node concept="1adJid" id="1LLgSqPjtmP" role="1aduh9">
          <property role="TrG5h" value="accountOpt" />
          <property role="0Rz4W" value="509522570" />
          <node concept="Uns6S" id="1LLgSqPjtmQ" role="2zM23F">
            <node concept="2Ss9cW" id="1LLgSqPjtmR" role="Uns6T">
              <ref role="2Ss9cX" node="2aA5E1$kmQ" />
            </node>
          </node>
          <node concept="1QScDb" id="1LLgSqPjtmW" role="1adJii">
            <node concept="1QScDb" id="1LLgSqPjtn8" role="30czhm">
              <node concept="3sQ2Ir" id="5ZGapTFdgqc" role="1QScD9" />
              <node concept="3BiLgO" id="5ZGapTFe2PW" role="30czhm">
                <ref role="3BiLgL" node="4FZ6_9O1TNh" resolve="inputAccounts" />
              </node>
            </node>
            <node concept="1HmgMX" id="5kA80xxF5jo" role="1QScD9">
              <node concept="3izI60" id="5kA80xxF5jp" role="3iAY4F">
                <node concept="30cPrO" id="5kA80xxFeHa" role="3izI61">
                  <node concept="1QScDb" id="3o3xT4d1fSp" role="30dEs_">
                    <node concept="3o_JK" id="3o3xT4d1glW" role="1QScD9">
                      <ref role="3o_JH" node="2aA5E1$kn1" />
                    </node>
                    <node concept="1afdae" id="5kA80xxFg29" role="30czhm">
                      <ref role="1afue_" node="1LLgSqPjtnC" resolve="oldAccount" />
                    </node>
                  </node>
                  <node concept="1QScDb" id="5kA80xxFbYQ" role="30dEsF">
                    <node concept="3o_JK" id="5kA80xxFdiI" role="1QScD9">
                      <ref role="3o_JH" node="2aA5E1$kn1" />
                    </node>
                    <node concept="3izPEI" id="5kA80xxF5jr" role="30czhm" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="39w5ZF" id="1LLgSqPjtnb" role="1aduh9">
          <node concept="UmaEC" id="1LLgSqPjtnc" role="39w5ZE">
            <node concept="1adzI2" id="1LLgSqPjtnd" role="UmaED">
              <ref role="1adwt6" node="1LLgSqPjtmP" resolve="accountOpt" />
            </node>
            <node concept="pfQqD" id="1LLgSqPjtne" role="pfQ1b">
              <property role="pfQqC" value="account" />
            </node>
          </node>
          <node concept="1aduha" id="1LLgSqPjtnf" role="39w5ZG">
            <node concept="1QScDb" id="5ZGapTF6h6P" role="1aduh9">
              <node concept="3sPC8h" id="5ZGapTF6idW" role="1QScD9">
                <node concept="1QScDb" id="5ZGapTF6krE" role="3sPC8l">
                  <node concept="2t5v1R" id="5ZGapTF6lAG" role="1QScD9">
                    <node concept="1afdae" id="5ZGapTF6mIN" role="26Ft6C">
                      <ref role="1afue_" node="1LLgSqPjtnC" resolve="oldAccount" />
                    </node>
                  </node>
                  <node concept="3j5BQN" id="5ZGapTF6jkh" role="30czhm" />
                </node>
              </node>
              <node concept="3BiLgO" id="5ZGapTF6g0U" role="30czhm">
                <ref role="3BiLgL" node="4FZ6_9O1TNh" resolve="inputAccounts" />
              </node>
            </node>
            <node concept="1QScDb" id="5ZGapTF6rab" role="1aduh9">
              <node concept="3sPC8h" id="5ZGapTF6siP" role="1QScD9">
                <node concept="1QScDb" id="5ZGapTF6u$w" role="3sPC8l">
                  <node concept="2iGZtc" id="5ZGapTF6vJc" role="1QScD9">
                    <node concept="2S399m" id="3o3xT4cQYL0" role="26Ft6C">
                      <node concept="2Ss9cW" id="3o3xT4cQYL1" role="2S399n">
                        <ref role="2Ss9cX" node="2aA5E1$kmQ" />
                      </node>
                      <node concept="1QScDb" id="3o3xT4cQYL2" role="2S399l">
                        <node concept="3o_JK" id="3o3xT4cQYL3" role="1QScD9">
                          <ref role="3o_JH" node="2aA5E1$kn1" />
                        </node>
                        <node concept="1ZmhP4" id="3o3xT4cR2vw" role="30czhm">
                          <ref role="1ZmhP3" node="1LLgSqPjtnc" resolve="account" />
                        </node>
                      </node>
                      <node concept="1QScDb" id="3o3xT4cQYL5" role="2S399l">
                        <node concept="3o_JK" id="3o3xT4cQYL6" role="1QScD9">
                          <ref role="3o_JH" node="2aA5E1$kni" />
                        </node>
                        <node concept="1ZmhP4" id="3o3xT4cR3Hs" role="30czhm">
                          <ref role="1ZmhP3" node="1LLgSqPjtnc" resolve="account" />
                        </node>
                      </node>
                      <node concept="1afdae" id="3o3xT4cQYL8" role="2S399l">
                        <ref role="1afue_" node="3o3xT4d1eYW" resolve="newBalance" />
                      </node>
                    </node>
                  </node>
                  <node concept="3j5BQN" id="5ZGapTF6tr_" role="30czhm" />
                </node>
              </node>
              <node concept="3BiLgO" id="5ZGapTF6q3A" role="30czhm">
                <ref role="3BiLgL" node="4FZ6_9O1TNh" resolve="inputAccounts" />
              </node>
            </node>
          </node>
          <node concept="pf3Wd" id="5tXI$wGzO7D" role="pf3W8">
            <node concept="UmHTt" id="5tXI$wGzOBl" role="pf3We" />
          </node>
        </node>
      </node>
      <node concept="1ahQXy" id="1LLgSqPjtnC" role="1ahQWs">
        <property role="TrG5h" value="oldAccount" />
        <node concept="2Ss9cW" id="3o3xT4d1dFP" role="3ix9CU">
          <ref role="2Ss9cX" node="2aA5E1$kmQ" />
        </node>
      </node>
      <node concept="1ahQXy" id="3o3xT4d1eYW" role="1ahQWs">
        <property role="TrG5h" value="newBalance" />
        <node concept="30bXLL" id="3o3xT4d3GX6" role="3ix9CU" />
      </node>
      <node concept="2lgajX" id="1LLgSqPjtnL" role="28QfE6" />
    </node>
    <node concept="_ixoA" id="3o3xT4d1iov" role="3KDhAD" />
    <node concept="_ixoA" id="3o3xT4d1ks5" role="3KDhAD" />
    <node concept="_ixoA" id="3o3xT4d1a13" role="3KDhAD" />
  </node>
  <node concept="2C2W_Q" id="2aA5E1wW8z">
    <property role="TrG5h" value="BankOntology" />
    <property role="3GE5qa" value="" />
    <node concept="2MkpuS" id="2aA5E1$kmQ" role="_iOnB">
      <property role="TrG5h" value="Account" />
      <node concept="2Ss9d7" id="2aA5E1$kn1" role="S5Trm">
        <property role="TrG5h" value="id" />
        <node concept="30bXR$" id="1LLgSqPiR3h" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="2aA5E1$kni" role="S5Trm">
        <property role="TrG5h" value="name" />
        <node concept="30bdrU" id="2aA5E1$knm" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="2aA5E1$knz" role="S5Trm">
        <property role="TrG5h" value="balance" />
        <node concept="30bXLL" id="2aA5E1$knB" role="2S399n" />
      </node>
    </node>
    <node concept="2MkpuS" id="2aA5E1_HGC" role="_iOnB">
      <property role="TrG5h" value="Operation" />
      <node concept="2Ss9d7" id="2aA5E1_HHB" role="S5Trm">
        <property role="TrG5h" value="type" />
        <node concept="30bXR$" id="2aA5E1_HHF" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="2aA5E1_HHS" role="S5Trm">
        <property role="TrG5h" value="amount" />
        <node concept="30bXLL" id="2aA5E1_HHW" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="2aA5E1_HId" role="S5Trm">
        <property role="TrG5h" value="balance" />
        <node concept="30bXLL" id="2aA5E1_HIh" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="2aA5E1_HIu" role="S5Trm">
        <property role="TrG5h" value="accountId" />
        <node concept="30bXR$" id="5kA80xxBN_a" role="2S399n" />
      </node>
    </node>
    <node concept="2MkpuS" id="5ZGapTFaVSq" role="_iOnB">
      <property role="TrG5h" value="OperationList" />
      <node concept="2Ss9d7" id="5ZGapTFaWdE" role="S5Trm">
        <property role="TrG5h" value="opList" />
        <node concept="3iBYCm" id="5ZGapTFaWdI" role="2S399n">
          <node concept="2Ss9cW" id="5ZGapTFaWe0" role="3iBWmK">
            <ref role="2Ss9cX" node="2aA5E1_HGC" resolve="Operation" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2MkpuS" id="2aA5E1_HHf" role="_iOnB">
      <property role="TrG5h" value="Problem" />
      <node concept="2Ss9d7" id="2aA5E1_HKn" role="S5Trm">
        <property role="TrG5h" value="number" />
        <node concept="30bXR$" id="2aA5E1_HKr" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="2aA5E1_HKC" role="S5Trm">
        <property role="TrG5h" value="msg" />
        <node concept="30bdrU" id="2aA5E1_HKG" role="2S399n" />
      </node>
    </node>
    <node concept="2MkpuT" id="2aA5E1_HGL" role="_iOnB">
      <property role="TrG5h" value="MakeOperation" />
      <node concept="2Ss9d7" id="2aA5E1_HIT" role="S5Trm">
        <property role="TrG5h" value="accountId" />
        <node concept="30bXR$" id="5kA80xxBNAG" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="2aA5E1_HJa" role="S5Trm">
        <property role="TrG5h" value="type" />
        <node concept="30bXR$" id="2aA5E1_HJe" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="2aA5E1_HJo" role="S5Trm">
        <property role="TrG5h" value="amount" />
        <node concept="30bXLL" id="2aA5E1_HJs" role="2S399n" />
      </node>
    </node>
    <node concept="2MkpuT" id="2aA5E1_HGR" role="_iOnB">
      <property role="TrG5h" value="CreateAccount" />
      <node concept="2Ss9d7" id="2aA5E1_HJ_" role="S5Trm">
        <property role="TrG5h" value="name" />
        <node concept="30bdrU" id="2aA5E1_HJD" role="2S399n" />
      </node>
    </node>
    <node concept="2MkpuT" id="2aA5E1_HH6" role="_iOnB">
      <property role="TrG5h" value="InformationRequest" />
      <node concept="2Ss9d7" id="2aA5E1_HK3" role="S5Trm">
        <property role="TrG5h" value="accountId" />
        <node concept="30bXR$" id="5kA80xxBNAA" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="2aA5E1_HJM" role="S5Trm">
        <property role="TrG5h" value="type" />
        <node concept="30bXR$" id="2aA5E1_HJQ" role="2S399n" />
      </node>
    </node>
  </node>
  <node concept="198UmT" id="7AVCbhi6SHs">
    <property role="TrG5h" value="BankUtils" />
    <node concept="5mgZ8" id="1MgVVihszjp" role="3KDhAD">
      <property role="TrG5h" value="command" />
      <node concept="5mgYR" id="1MgVVihszjz" role="5mgYi">
        <property role="TrG5h" value="new_account" />
        <node concept="30bXRB" id="2aA5E1137P" role="Y$80S">
          <property role="30bXRw" value="1" />
        </node>
      </node>
      <node concept="5mgYR" id="1MgVVihszjB" role="5mgYi">
        <property role="TrG5h" value="deposit" />
        <node concept="30bXRB" id="2aA5E113aA" role="Y$80S">
          <property role="30bXRw" value="2" />
        </node>
      </node>
      <node concept="5mgYR" id="1MgVVihszjJ" role="5mgYi">
        <property role="TrG5h" value="balance" />
        <node concept="30bXRB" id="2aA5E113ck" role="Y$80S">
          <property role="30bXRw" value="3" />
        </node>
      </node>
      <node concept="5mgYR" id="1MgVVihszjN" role="5mgYi">
        <property role="TrG5h" value="withdraw" />
        <node concept="30bXRB" id="2aA5E113ew" role="Y$80S">
          <property role="30bXRw" value="4" />
        </node>
      </node>
      <node concept="5mgYR" id="1MgVVihszjR" role="5mgYi">
        <property role="TrG5h" value="operationList" />
        <node concept="30bXRB" id="2aA5E113eX" role="Y$80S">
          <property role="30bXRw" value="5" />
        </node>
      </node>
      <node concept="5mgYR" id="5ZGapTEKjQo" role="5mgYi">
        <property role="TrG5h" value="unknown" />
        <node concept="30bXRB" id="5ZGapTEKjY8" role="Y$80S">
          <property role="30bXRw" value="-1" />
        </node>
      </node>
      <node concept="30bXR$" id="2aA5E1136G" role="3c3ckp" />
    </node>
    <node concept="_ixoA" id="3o3xT4d0BJj" role="3KDhAD" />
    <node concept="5mgZ8" id="2umCVQRk8Yb" role="3KDhAD">
      <property role="TrG5h" value="errorCodes" />
      <node concept="5mgYR" id="2umCVQRk8Yc" role="5mgYi">
        <property role="TrG5h" value="invalidAmount" />
        <node concept="2S399m" id="2umCVQRkclJ" role="Y$80S">
          <node concept="2Ss9cW" id="2umCVQRkcmt" role="2S399n">
            <ref role="2Ss9cX" node="2aA5E1_HHf" resolve="Problem" />
          </node>
          <node concept="30bXRB" id="2umCVQRkcni" role="2S399l">
            <property role="30bXRw" value="1" />
          </node>
          <node concept="30bdrP" id="2umCVQRkcoB" role="2S399l">
            <property role="30bdrQ" value="invalid amount" />
          </node>
        </node>
      </node>
      <node concept="5mgYR" id="2umCVQRk8Ye" role="5mgYi">
        <property role="TrG5h" value="accountNotFound" />
        <node concept="2S399m" id="2umCVQRkcs8" role="Y$80S">
          <node concept="2Ss9cW" id="2umCVQRkctb" role="2S399n">
            <ref role="2Ss9cX" node="2aA5E1_HHf" resolve="Problem" />
          </node>
          <node concept="30bXRB" id="2umCVQRkmaW" role="2S399l">
            <property role="30bXRw" value="2" />
          </node>
          <node concept="30bdrP" id="2umCVQRkmdl" role="2S399l">
            <property role="30bdrQ" value="account not found" />
          </node>
        </node>
      </node>
      <node concept="5mgYR" id="2umCVQRk8Yg" role="5mgYi">
        <property role="TrG5h" value="insufficientFunds" />
        <node concept="2S399m" id="2umCVQRkm7V" role="Y$80S">
          <node concept="2Ss9cW" id="2umCVQRkm8N" role="2S399n">
            <ref role="2Ss9cX" node="2aA5E1_HHf" resolve="Problem" />
          </node>
          <node concept="30bXRB" id="2umCVQRkm9M" role="2S399l">
            <property role="30bXRw" value="3" />
          </node>
          <node concept="30bdrP" id="2umCVQRkmkM" role="2S399l">
            <property role="30bdrQ" value="insufficient funds" />
          </node>
        </node>
      </node>
      <node concept="2Ss9cW" id="2umCVQRkcjB" role="3c3ckp">
        <ref role="2Ss9cX" node="2aA5E1_HHf" resolve="Problem" />
      </node>
    </node>
    <node concept="_ixoA" id="2umCVQRsiNB" role="3KDhAD" />
    <node concept="1aga60" id="2aA5E111OR" role="3KDhAD">
      <property role="TrG5h" value="parseCommand" />
      <node concept="1aduha" id="2aA5E111T_" role="1ahQXP">
        <node concept="1adJid" id="2aA5E116ym" role="1aduh9">
          <property role="TrG5h" value="cmd" />
          <node concept="1QScDb" id="2aA5E116KQ" role="1adJii">
            <node concept="2zXAyN" id="2aA5E116PT" role="1QScD9" />
            <node concept="1afdae" id="2aA5E116FK" role="30czhm">
              <ref role="1afue_" node="2aA5E111T7" resolve="commandString" />
            </node>
          </node>
          <node concept="30bXR$" id="2aA5E117fX" role="2zM23F" />
        </node>
        <node concept="2fGnzi" id="2aA5E114fF" role="1aduh9">
          <node concept="2fGnzd" id="2aA5E114fG" role="2fGnxs">
            <node concept="30cPrO" id="2aA5E114l8" role="2fGnzS">
              <node concept="1QScDb" id="2aA5E114sH" role="30dEs_">
                <node concept="YK6gA" id="2aA5E114vA" role="1QScD9" />
                <node concept="5mhuz" id="2aA5E114nE" role="30czhm">
                  <ref role="5mhpJ" node="1MgVVihszjz" resolve="new_account" />
                </node>
              </node>
              <node concept="1adzI2" id="2aA5E117q0" role="30dEsF">
                <ref role="1adwt6" node="2aA5E116ym" resolve="cmd" />
              </node>
            </node>
            <node concept="5mhuz" id="2aA5E114qr" role="2fGnzA">
              <ref role="5mhpJ" node="1MgVVihszjz" resolve="new_account" />
            </node>
          </node>
          <node concept="2fGnzd" id="2aA5E114fH" role="2fGnxs">
            <node concept="30cPrO" id="2aA5E114$Y" role="2fGnzS">
              <node concept="1QScDb" id="2aA5E114Ed" role="30dEs_">
                <node concept="YK6gA" id="2aA5E114H4" role="1QScD9" />
                <node concept="5mhuz" id="2aA5E114Bx" role="30czhm">
                  <ref role="5mhpJ" node="1MgVVihszjB" resolve="deposit" />
                </node>
              </node>
              <node concept="1adzI2" id="2aA5E117v3" role="30dEsF">
                <ref role="1adwt6" node="2aA5E116ym" resolve="cmd" />
              </node>
            </node>
            <node concept="5mhuz" id="2aA5E114K0" role="2fGnzA">
              <ref role="5mhpJ" node="1MgVVihszjB" resolve="deposit" />
            </node>
          </node>
          <node concept="2fGnzd" id="2aA5E114MM" role="2fGnxs">
            <node concept="30cPrO" id="2aA5E114Tc" role="2fGnzS">
              <node concept="1QScDb" id="2aA5E11502" role="30dEs_">
                <node concept="YK6gA" id="2aA5E1153n" role="1QScD9" />
                <node concept="5mhuz" id="2aA5E114Wm" role="30czhm">
                  <ref role="5mhpJ" node="1MgVVihszjJ" resolve="balance" />
                </node>
              </node>
              <node concept="1adzI2" id="2aA5E117CM" role="30dEsF">
                <ref role="1adwt6" node="2aA5E116ym" resolve="cmd" />
              </node>
            </node>
            <node concept="5mhuz" id="2aA5E1156M" role="2fGnzA">
              <ref role="5mhpJ" node="1MgVVihszjJ" resolve="balance" />
            </node>
          </node>
          <node concept="2fGnzd" id="2aA5E1156O" role="2fGnxs">
            <node concept="30cPrO" id="2aA5E115ec" role="2fGnzS">
              <node concept="1QScDb" id="2aA5E115lp" role="30dEs_">
                <node concept="YK6gA" id="2aA5E115pJ" role="1QScD9" />
                <node concept="5mhuz" id="2aA5E115hJ" role="30czhm">
                  <ref role="5mhpJ" node="1MgVVihszjN" resolve="withdraw" />
                </node>
              </node>
              <node concept="1adzI2" id="2aA5E117Mr" role="30dEsF">
                <ref role="1adwt6" node="2aA5E116ym" resolve="cmd" />
              </node>
            </node>
            <node concept="5mhuz" id="2aA5E115tz" role="2fGnzA">
              <ref role="5mhpJ" node="1MgVVihszjN" resolve="withdraw" />
            </node>
          </node>
          <node concept="2fGnzd" id="2aA5E115xk" role="2fGnxs">
            <node concept="30cPrO" id="2aA5E115DK" role="2fGnzS">
              <node concept="1QScDb" id="2aA5E115LY" role="30dEs_">
                <node concept="YK6gA" id="2aA5E115QN" role="1QScD9" />
                <node concept="5mhuz" id="2aA5E115HP" role="30czhm">
                  <ref role="5mhpJ" node="1MgVVihszjR" resolve="operationList" />
                </node>
              </node>
              <node concept="1adzI2" id="2aA5E117Wm" role="30dEsF">
                <ref role="1adwt6" node="2aA5E116ym" resolve="cmd" />
              </node>
            </node>
            <node concept="5mhuz" id="2aA5E115V8" role="2fGnzA">
              <ref role="5mhpJ" node="1MgVVihszjR" resolve="operationList" />
            </node>
          </node>
          <node concept="2fGnzd" id="5ZGapTEKj$l" role="2fGnxs">
            <node concept="2fHqz8" id="5ZGapTEKjDA" role="2fGnzS" />
            <node concept="5mhuz" id="5ZGapTEKkx2" role="2fGnzA">
              <ref role="5mhpJ" node="5ZGapTEKjQo" resolve="unknown" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1ahQXy" id="2aA5E111T7" role="1ahQWs">
        <property role="TrG5h" value="commandString" />
        <node concept="30bdrU" id="2aA5E116p5" role="3ix9CU" />
      </node>
      <node concept="5mh7t" id="2aA5E11bPT" role="2zM23F">
        <ref role="5mh6l" node="1MgVVihszjp" resolve="command" />
      </node>
    </node>
    <node concept="_ixoA" id="3o3xT4d0CpD" role="3KDhAD" />
    <node concept="1aga60" id="7IIeq5wNeJC" role="3KDhAD">
      <property role="TrG5h" value="sendOpReqProblem" />
      <node concept="1ahQXy" id="7IIeq5wNeZY" role="1ahQWs">
        <property role="TrG5h" value="msg" />
        <node concept="1g4zL5" id="7IIeq5wNeZZ" role="3ix9CU">
          <node concept="2Ss9cW" id="7IIeq5wNf00" role="2uKbmB">
            <ref role="2Ss9cX" node="2aA5E1_HGL" resolve="MakeOperation" />
          </node>
        </node>
      </node>
      <node concept="1ahQXy" id="7IIeq5wNf4g" role="1ahQWs">
        <property role="TrG5h" value="error" />
        <node concept="5mh7t" id="7IIeq5wNf8m" role="3ix9CU">
          <ref role="5mh6l" node="2umCVQRk8Yb" resolve="errorCodes" />
        </node>
      </node>
      <node concept="1aduha" id="7IIeq5wNeUe" role="1ahQXP">
        <node concept="3lL9fZ" id="7IIeq5wNeUq" role="1aduh9">
          <node concept="30dDZf" id="7IIeq5wNeUr" role="3lRU4Y">
            <node concept="30bdrP" id="7IIeq5wNeUs" role="30dEsF">
              <property role="30bdrQ" value="problem found: " />
            </node>
            <node concept="1QScDb" id="7IIeq5wNeUt" role="30dEs_">
              <node concept="3o_JK" id="7IIeq5wNeUu" role="1QScD9">
                <ref role="3o_JH" node="2aA5E1_HKC" resolve="msg" />
              </node>
              <node concept="1QScDb" id="7IIeq5wNeUv" role="30czhm">
                <node concept="YK6gA" id="7IIeq5wNeUw" role="1QScD9" />
                <node concept="1afdae" id="7IIeq5wNeUx" role="30czhm">
                  <ref role="1afue_" node="7IIeq5wNf4g" resolve="error" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1eBHpp" id="7IIeq5wNeUy" role="1aduh9">
          <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
          <node concept="1g4zL5" id="7IIeq5wNeUA" role="1eBHp4">
            <node concept="2Ss9cW" id="7IIeq5wNeUB" role="2uKbmB">
              <ref role="2Ss9cX" node="2aA5E1_HHf" resolve="Problem" />
            </node>
          </node>
          <node concept="1afdae" id="7IIeq5wNeUC" role="1eBHp3">
            <ref role="1afue_" node="7IIeq5wNeZY" resolve="msg" />
          </node>
          <node concept="1QScDb" id="7IIeq5wNfxz" role="1eBHp7">
            <node concept="YK6gA" id="7IIeq5wNfAX" role="1QScD9" />
            <node concept="1afdae" id="7IIeq5wNftN" role="30czhm">
              <ref role="1afue_" node="7IIeq5wNf4g" resolve="error" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2lgajX" id="7IIeq5wNf8v" role="28QfE6" />
    </node>
    <node concept="_ixoA" id="3o3xT4d0C1K" role="3KDhAD" />
    <node concept="1aga60" id="7IIeq5wNs0h" role="3KDhAD">
      <property role="TrG5h" value="sendInfReqProblem" />
      <node concept="1ahQXy" id="7IIeq5wNs0i" role="1ahQWs">
        <property role="TrG5h" value="msg" />
        <node concept="1g4zL5" id="7IIeq5wNs0j" role="3ix9CU">
          <node concept="2Ss9cW" id="7IIeq5wNs0k" role="2uKbmB">
            <ref role="2Ss9cX" node="2aA5E1_HH6" resolve="InformationRequest" />
          </node>
        </node>
      </node>
      <node concept="1ahQXy" id="7IIeq5wNs0l" role="1ahQWs">
        <property role="TrG5h" value="error" />
        <node concept="5mh7t" id="7IIeq5wNs0m" role="3ix9CU">
          <ref role="5mh6l" node="2umCVQRk8Yb" resolve="errorCodes" />
        </node>
      </node>
      <node concept="1aduha" id="7IIeq5wNs0n" role="1ahQXP">
        <node concept="3lL9fZ" id="7IIeq5wNs0o" role="1aduh9">
          <node concept="30dDZf" id="7IIeq5wNs0p" role="3lRU4Y">
            <node concept="30bdrP" id="7IIeq5wNs0q" role="30dEsF">
              <property role="30bdrQ" value="problem found: " />
            </node>
            <node concept="1QScDb" id="7IIeq5wNs0r" role="30dEs_">
              <node concept="3o_JK" id="7IIeq5wNs0s" role="1QScD9">
                <ref role="3o_JH" node="2aA5E1_HKC" resolve="msg" />
              </node>
              <node concept="1QScDb" id="7IIeq5wNs0t" role="30czhm">
                <node concept="YK6gA" id="7IIeq5wNs0u" role="1QScD9" />
                <node concept="1afdae" id="7IIeq5wNs0v" role="30czhm">
                  <ref role="1afue_" node="7IIeq5wNs0l" resolve="error" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1eBHpp" id="7IIeq5wNs0w" role="1aduh9">
          <property role="2NJI0k" value="4MI7ZB$IHdc/Inform" />
          <node concept="1g4zL5" id="7IIeq5wNs0x" role="1eBHp4">
            <node concept="2Ss9cW" id="7IIeq5wNs0y" role="2uKbmB">
              <ref role="2Ss9cX" node="2aA5E1_HHf" resolve="Problem" />
            </node>
          </node>
          <node concept="1afdae" id="7IIeq5wNs0z" role="1eBHp3">
            <ref role="1afue_" node="7IIeq5wNs0i" resolve="msg" />
          </node>
          <node concept="1QScDb" id="7IIeq5wNs0$" role="1eBHp7">
            <node concept="YK6gA" id="7IIeq5wNs0_" role="1QScD9" />
            <node concept="1afdae" id="7IIeq5wNs0A" role="30czhm">
              <ref role="1afue_" node="7IIeq5wNs0l" resolve="error" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2lgajX" id="7IIeq5wNs0B" role="28QfE6" />
    </node>
    <node concept="_ixoA" id="7IIeq5wNscW" role="3KDhAD" />
    <node concept="_ixoA" id="7IIeq5wNrYl" role="3KDhAD" />
    <node concept="2zPypq" id="7AVCbhi0Vds" role="3KDhAD">
      <property role="TrG5h" value="startingBalance" />
      <node concept="30bXRB" id="7AVCbhi0VdU" role="2zPyp_">
        <property role="30bXRw" value="0.0" />
      </node>
      <node concept="30bXLL" id="7AVCbhi0Vev" role="2zM23F" />
    </node>
    <node concept="2zPypq" id="22Cof6WDzJw" role="3KDhAD">
      <property role="TrG5h" value="serviceName" />
      <node concept="30bdrP" id="22Cof6W8bE4" role="2zPyp_">
        <property role="30bdrQ" value="bank-server" />
      </node>
    </node>
    <node concept="2zPypq" id="22Cof6WD$d2" role="3KDhAD">
      <property role="TrG5h" value="defaultAccountName" />
      <node concept="30bdrP" id="22Cof6WD$d_" role="2zPyp_">
        <property role="30bdrQ" value="stanley" />
      </node>
    </node>
    <node concept="2zPypq" id="22Cof6WD$eh" role="3KDhAD">
      <property role="TrG5h" value="defaultAccountId" />
      <node concept="30bdrP" id="22Cof6WD$eS" role="2zPyp_">
        <property role="30bdrQ" value="1" />
      </node>
    </node>
    <node concept="2zPypq" id="22Cof6WD$fw" role="3KDhAD">
      <property role="TrG5h" value="defaultAmount" />
      <node concept="30bdrP" id="22Cof6WD$g9" role="2zPyp_">
        <property role="30bdrQ" value="42" />
      </node>
    </node>
    <node concept="2zPypq" id="22Cof6WD$hr" role="3KDhAD">
      <property role="TrG5h" value="defaultCommand" />
      <node concept="30bdrP" id="22Cof6WD$i1" role="2zPyp_">
        <property role="30bdrQ" value="1" />
      </node>
    </node>
    <node concept="_ixoA" id="22Cof6WD$gu" role="3KDhAD" />
    <node concept="_ixoA" id="1SvJ3Hz0TmI" role="3KDhAD" />
    <node concept="3KE$ip" id="3o3xT4d0BOV" role="19Gq0t">
      <ref role="21GGV3" node="2aA5E1wW8z" resolve="BankOntology" />
    </node>
  </node>
</model>

