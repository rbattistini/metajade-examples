<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:dc6e6140-bfa1-4434-a924-89c8f2c06bde(Playground.PingPong)">
  <persistence version="9" />
  <languages>
    <devkit ref="2a15b00e-495f-4bce-aabb-bf2bffd958aa(MetaJade.devkit.java)" />
  </languages>
  <imports />
  <registry>
    <language id="390edf25-bfa2-48c0-b67b-fc8fbd32bfbc" name="MetaJade.core">
      <concept id="6398777375045966262" name="MetaJade.core.structure.Stop" flags="ng" index="2cDHYC" />
      <concept id="943645189038563415" name="MetaJade.core.structure.IDefinePerformative" flags="ng" index="2NJI0r">
        <property id="943645189038563416" name="performative" index="2NJI0k" />
      </concept>
      <concept id="2569313154175320625" name="MetaJade.core.structure.IChunkDep" flags="ng" index="1cWWE2">
        <reference id="39011061268502169" name="chunk" index="21GGV3" />
      </concept>
      <concept id="2460144669873447797" name="MetaJade.core.structure.Log" flags="ng" index="3lL9fZ">
        <child id="2460144669875212724" name="value" index="3lRU4Y" />
      </concept>
      <concept id="8295235836482040281" name="MetaJade.core.structure.AbstractChunk" flags="ng" index="1yxazi">
        <child id="2457439430830142521" name="imports" index="19Gq0t" />
      </concept>
      <concept id="4316580858990611837" name="MetaJade.core.structure.AIDType" flags="ng" index="3NxlSm" />
    </language>
    <language id="7b68d745-a7b8-48b9-bd9c-05c0f8725a35" name="org.iets3.core.base">
      <concept id="229512757698888199" name="org.iets3.core.base.structure.IOptionallyNamed" flags="ng" index="pfQq$">
        <child id="229512757698888936" name="optionalName" index="pfQ1b" />
      </concept>
      <concept id="229512757698888202" name="org.iets3.core.base.structure.OptionalNameSpecifier" flags="ng" index="pfQqD">
        <property id="229512757698888203" name="optionalName" index="pfQqC" />
      </concept>
    </language>
    <language id="cfaa4966-b7d5-4b69-b66a-309a6e1a7290" name="org.iets3.core.expr.base">
      <concept id="606861080870797309" name="org.iets3.core.expr.base.structure.IfElseSection" flags="ng" index="pf3Wd">
        <child id="606861080870797310" name="expr" index="pf3We" />
      </concept>
      <concept id="7089558164908491660" name="org.iets3.core.expr.base.structure.EmptyExpression" flags="ng" index="2zH6wq" />
      <concept id="7089558164905593724" name="org.iets3.core.expr.base.structure.IOptionallyTyped" flags="ng" index="2zM23E">
        <child id="7089558164905593725" name="type" index="2zM23F" />
      </concept>
      <concept id="7071042522334260296" name="org.iets3.core.expr.base.structure.ITyped" flags="ng" index="2_iKZX">
        <child id="8811147530085329321" name="type" index="2S399n" />
      </concept>
      <concept id="2807135271608145920" name="org.iets3.core.expr.base.structure.IsSomeExpression" flags="ng" index="UmaEC">
        <child id="2807135271608145921" name="expr" index="UmaED" />
      </concept>
      <concept id="2807135271607939856" name="org.iets3.core.expr.base.structure.OptionType" flags="ng" index="Uns6S">
        <child id="2807135271607939857" name="baseType" index="Uns6T" />
      </concept>
      <concept id="5115872837156802409" name="org.iets3.core.expr.base.structure.UnaryExpression" flags="ng" index="30czhk">
        <child id="5115872837156802411" name="expr" index="30czhm" />
      </concept>
      <concept id="5115872837156687890" name="org.iets3.core.expr.base.structure.LessExpression" flags="ng" index="30d6GJ" />
      <concept id="5115872837156578546" name="org.iets3.core.expr.base.structure.PlusExpression" flags="ng" index="30dDZf" />
      <concept id="5115872837156576277" name="org.iets3.core.expr.base.structure.BinaryExpression" flags="ng" index="30dEsC">
        <child id="5115872837156576280" name="right" index="30dEs_" />
        <child id="5115872837156576278" name="left" index="30dEsF" />
      </concept>
      <concept id="7849560302565679722" name="org.iets3.core.expr.base.structure.IfExpression" flags="ng" index="39w5ZF">
        <child id="606861080870797304" name="elseSection" index="pf3W8" />
        <child id="7849560302565679723" name="condition" index="39w5ZE" />
        <child id="7849560302565679725" name="thenPart" index="39w5ZG" />
      </concept>
      <concept id="9002563722476995145" name="org.iets3.core.expr.base.structure.DotExpression" flags="ng" index="1QScDb">
        <child id="9002563722476995147" name="target" index="1QScD9" />
      </concept>
      <concept id="1059200196223309235" name="org.iets3.core.expr.base.structure.SomeValExpr" flags="ng" index="1ZmhP4">
        <reference id="1059200196223309236" name="someQuery" index="1ZmhP3" />
      </concept>
    </language>
    <language id="6b277d9a-d52d-416f-a209-1919bd737f50" name="org.iets3.core.expr.simpleTypes">
      <concept id="4513425716319387765" name="org.iets3.core.expr.simpleTypes.structure.StringToIntTarget" flags="ng" index="2zXAyN" />
      <concept id="5115872837157252552" name="org.iets3.core.expr.simpleTypes.structure.StringLiteral" flags="ng" index="30bdrP">
        <property id="5115872837157252555" name="value" index="30bdrQ" />
      </concept>
      <concept id="5115872837157252551" name="org.iets3.core.expr.simpleTypes.structure.StringType" flags="ng" index="30bdrU" />
      <concept id="5115872837157054169" name="org.iets3.core.expr.simpleTypes.structure.IntegerType" flags="ng" index="30bXR$" />
      <concept id="5115872837157054170" name="org.iets3.core.expr.simpleTypes.structure.NumberLiteral" flags="ng" index="30bXRB">
        <property id="5115872837157054173" name="value" index="30bXRw" />
      </concept>
    </language>
    <language id="71934284-d7d1-45ee-a054-8c072591085f" name="org.iets3.core.expr.toplevel">
      <concept id="8811147530085329320" name="org.iets3.core.expr.toplevel.structure.RecordLiteral" flags="ng" index="2S399m">
        <child id="8811147530085329323" name="memberValues" index="2S399l" />
      </concept>
      <concept id="602952467877559919" name="org.iets3.core.expr.toplevel.structure.IRecordDeclaration" flags="ng" index="S5Q1W">
        <child id="602952467877562565" name="members" index="S5Trm" />
      </concept>
      <concept id="8811147530084018370" name="org.iets3.core.expr.toplevel.structure.RecordType" flags="ng" index="2Ss9cW">
        <reference id="8811147530084018371" name="record" index="2Ss9cX" />
      </concept>
      <concept id="8811147530084018361" name="org.iets3.core.expr.toplevel.structure.RecordMember" flags="ng" index="2Ss9d7" />
    </language>
    <language id="0be7c48a-9600-4a9c-a497-e40de8415ebd" name="MetaJade.behaviour">
      <concept id="39011061266900173" name="MetaJade.behaviour.structure.ActivateSequentialBehaviour" flags="ng" index="21_nMn" />
      <concept id="39011061267007073" name="MetaJade.behaviour.structure.IBehavioursContainer" flags="ng" index="21ELSV">
        <child id="39011061266908585" name="behaviours" index="21EDRN" />
      </concept>
      <concept id="1363295496955285700" name="MetaJade.behaviour.structure.ActivateBehaviour" flags="ng" index="p0Vrc" />
      <concept id="1363295496955285701" name="MetaJade.behaviour.structure.AbstractBehaviourUnaryExpression" flags="ng" index="p0Vrd">
        <child id="4145021229450611113" name="behaviour" index="3QnlHL" />
      </concept>
      <concept id="3401921042422620142" name="MetaJade.behaviour.structure.Behaviour" flags="ng" index="3oGdb7">
        <child id="2058408588520789632" name="action" index="2dXv$O" />
        <child id="266893304458096888" name="behaviourType" index="3u$SbN" />
        <child id="4289783338601281164" name="inputParams" index="3BfUAV" />
      </concept>
      <concept id="4289783338601963020" name="MetaJade.behaviour.structure.BehaviourArgument" flags="ng" index="3Biw4V" />
      <concept id="4289783338602023683" name="MetaJade.behaviour.structure.BehaviourArgRef" flags="ng" index="3BiLgO">
        <reference id="4289783338602023686" name="arg" index="3BiLgL" />
      </concept>
      <concept id="3737182289224794353" name="MetaJade.behaviour.structure.CyclicBehaviour" flags="ng" index="3DZiec" />
      <concept id="3737182289224794352" name="MetaJade.behaviour.structure.OneShotBehaviour" flags="ng" index="3DZied" />
      <concept id="1741182739291547053" name="MetaJade.behaviour.structure.BehaviourDependency" flags="ng" index="3KE$Bk" />
      <concept id="4145021229450567818" name="MetaJade.behaviour.structure.BehaviourReference" flags="ng" index="3Qnb9i">
        <reference id="4145021229450567819" name="behaviour" index="3Qnb9j" />
        <child id="4145021229450567821" name="inputParams" index="3Qnb9l" />
      </concept>
    </language>
    <language id="fbba5118-5fc6-49ff-9c3b-0b4469830440" name="org.iets3.core.expr.mutable">
      <concept id="4255172619715417408" name="org.iets3.core.expr.mutable.structure.UpdateItExpression" flags="ng" index="3j5BQN" />
      <concept id="4255172619709548950" name="org.iets3.core.expr.mutable.structure.BoxType" flags="ng" index="3sNe5_">
        <child id="4255172619709548951" name="baseType" index="3sNe5$" />
      </concept>
      <concept id="4255172619711277794" name="org.iets3.core.expr.mutable.structure.BoxUpdateTarget" flags="ng" index="3sPC8h">
        <child id="4255172619711277798" name="value" index="3sPC8l" />
      </concept>
      <concept id="4255172619710841704" name="org.iets3.core.expr.mutable.structure.BoxValueTarget" flags="ng" index="3sQ2Ir" />
      <concept id="4255172619710740510" name="org.iets3.core.expr.mutable.structure.BoxExpression" flags="ng" index="3sRH3H">
        <child id="4255172619710740514" name="value" index="3sRH3h" />
      </concept>
    </language>
    <language id="68d558ed-66cf-46ac-b353-d5fddcc21f72" name="MetaJade.aclmessage">
      <concept id="8771781395564137494" name="MetaJade.aclmessage.structure.GetSenderOp" flags="ng" index="CgaMy" />
      <concept id="8771781395564539185" name="MetaJade.aclmessage.structure.GetContentOp" flags="ng" index="FICQ5" />
      <concept id="4949964338176784763" name="MetaJade.aclmessage.structure.SendMessage" flags="ng" index="2JkjsH">
        <child id="2658052393945773747" name="content" index="2rofCy" />
        <child id="2658052393945773748" name="receiver" index="2rofC_" />
        <child id="2658052393945773750" name="type" index="2rofCB" />
      </concept>
      <concept id="8895927397244351935" name="MetaJade.aclmessage.structure.ReceiveMessage" flags="ng" index="2R7DHE">
        <child id="5091083777057681387" name="onReceive" index="3NSspj" />
        <child id="5091083777057681390" name="arg" index="3NSspm" />
      </concept>
      <concept id="4946621191997514448" name="MetaJade.aclmessage.structure.ReceivedMessageArg" flags="ng" index="1fMSWk">
        <child id="2695020384284204564" name="type" index="2uPfNm" />
      </concept>
      <concept id="4946621191998473754" name="MetaJade.aclmessage.structure.ReceivedMessageArgRef" flags="ng" index="1fQeJu">
        <reference id="4946621191998473788" name="arg" index="1fQeJS" />
      </concept>
      <concept id="1816631675487503527" name="MetaJade.aclmessage.structure.MessageType" flags="ng" index="1g4zL5">
        <child id="2695020384283400037" name="baseType" index="2uKbmB" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="f3eafff0-30d2-46d6-9150-f0f3b880ce27" name="org.iets3.core.expr.path">
      <concept id="7814222126786013807" name="org.iets3.core.expr.path.structure.PathElement" flags="ng" index="3o_JK">
        <reference id="7814222126786013810" name="member" index="3o_JH" />
      </concept>
    </language>
    <language id="ff6c34ae-1ca3-455f-96b3-8f28007794c9" name="MetaJade.ontology">
      <concept id="5525388950973568655" name="MetaJade.ontology.structure.Ontology" flags="ng" index="2C2W_Q">
        <child id="543569365052711058" name="contents" index="_iOnB" />
      </concept>
      <concept id="8320844951296934876" name="MetaJade.ontology.structure.AgentActionSchema" flags="ng" index="2MkpuT" />
    </language>
    <language id="9464fa06-5ab9-409b-9274-64ab29588457" name="org.iets3.core.expr.lambda">
      <concept id="4790956042240983401" name="org.iets3.core.expr.lambda.structure.BlockExpression" flags="ng" index="1aduha">
        <child id="4790956042240983402" name="expressions" index="1aduh9" />
      </concept>
      <concept id="4790956042241105569" name="org.iets3.core.expr.lambda.structure.ValRef" flags="ng" index="1adzI2">
        <reference id="4790956042241106533" name="val" index="1adwt6" />
      </concept>
      <concept id="4790956042241053102" name="org.iets3.core.expr.lambda.structure.ValExpression" flags="ng" index="1adJid">
        <child id="4790956042241053105" name="expr" index="1adJii" />
      </concept>
      <concept id="7554398283340318473" name="org.iets3.core.expr.lambda.structure.IArgument" flags="ng" index="3ix9CZ">
        <child id="7554398283340318476" name="type" index="3ix9CU" />
      </concept>
    </language>
    <language id="32367449-d150-4018-8690-20f09bf1abe2" name="MetaJade.agent">
      <concept id="8894738314219139740" name="MetaJade.agent.structure.AgentArgRef" flags="ng" index="2IpGv2">
        <reference id="8894738314219139741" name="arg" index="2IpGv3" />
      </concept>
      <concept id="943645189045937631" name="MetaJade.agent.structure.AgentArgument" flags="ng" index="2M3Amj">
        <child id="1410812809236658286" name="value" index="1wyKUC" />
      </concept>
      <concept id="3702022420523728372" name="MetaJade.agent.structure.Agent" flags="ng" index="3eIggM">
        <child id="943645189045937532" name="inputParams" index="2M3AkK" />
        <child id="943645189045937530" name="setup" index="2M3AkQ" />
      </concept>
      <concept id="2133980169836146545" name="MetaJade.agent.structure.SearchService" flags="ng" index="1xkGjp" />
      <concept id="2133980169836146546" name="MetaJade.agent.structure.RegisterService" flags="ng" index="1xkGjq" />
      <concept id="1741182739291004155" name="MetaJade.agent.structure.AbstractDfExpression" flags="ng" index="3KkF22">
        <child id="4316580858993931092" name="service" index="3NsI0Z" />
      </concept>
      <concept id="1741182739291546925" name="MetaJade.agent.structure.AgentDependency" flags="ng" index="3KE$_k" />
    </language>
  </registry>
  <node concept="3eIggM" id="1ekdGpN4sEt">
    <property role="3GE5qa" value="ping" />
    <property role="TrG5h" value="Pinger" />
    <node concept="2M3Amj" id="1ekdGpNcaV8" role="2M3AkK">
      <property role="TrG5h" value="maxRoundsParam" />
      <node concept="30bdrP" id="1ekdGpNcbc3" role="1wyKUC">
        <property role="30bdrQ" value="10" />
      </node>
      <node concept="30bdrU" id="1ekdGpNcbbV" role="3ix9CU" />
    </node>
    <node concept="3KE$_k" id="1ekdGpN4BLm" role="19Gq0t">
      <ref role="21GGV3" node="6GLwk1kl1b7" />
    </node>
    <node concept="3KE$_k" id="1ekdGpN4BLs" role="19Gq0t">
      <ref role="21GGV3" node="5zd1xVkqXkq" />
    </node>
    <node concept="1aduha" id="1ekdGpN4QyK" role="2M3AkQ">
      <node concept="1adJid" id="22Cof6V8xWe" role="1aduh9">
        <property role="TrG5h" value="maxRounds" />
        <node concept="1QScDb" id="22Cof6V8xYs" role="1adJii">
          <node concept="2zXAyN" id="22Cof6V8xZu" role="1QScD9" />
          <node concept="2IpGv2" id="22Cof6V8xY0" role="30czhm">
            <ref role="2IpGv3" node="1ekdGpNcaV8" resolve="maxRoundsParam" />
          </node>
        </node>
      </node>
      <node concept="2zH6wq" id="22Cof6V8Cy0" role="1aduh9" />
      <node concept="3lL9fZ" id="22Cof6V8$3Y" role="1aduh9">
        <node concept="30dDZf" id="22Cof6V8_38" role="3lRU4Y">
          <node concept="30bdrP" id="22Cof6V8$e9" role="30dEsF">
            <property role="30bdrQ" value="started with max rounds: " />
          </node>
          <node concept="1adzI2" id="22Cof6V8_qN" role="30dEs_">
            <ref role="1adwt6" node="22Cof6V8xWe" resolve="maxRounds" />
          </node>
        </node>
      </node>
      <node concept="1adJid" id="22Cof6V8ydZ" role="1aduh9">
        <property role="TrG5h" value="pongAIDOpt" />
        <node concept="1xkGjp" id="22Cof6V8y9_" role="1adJii">
          <node concept="30bdrP" id="22Cof6V8yaU" role="3NsI0Z">
            <property role="30bdrQ" value="ponger" />
          </node>
        </node>
        <node concept="Uns6S" id="22Cof6V8yk2" role="2zM23F">
          <node concept="3NxlSm" id="22Cof6V8ylr" role="Uns6T" />
        </node>
      </node>
      <node concept="3lL9fZ" id="22Cof6V8_Hc" role="1aduh9">
        <node concept="30bdrP" id="22Cof6V8_RD" role="3lRU4Y">
          <property role="30bdrQ" value="ponger found" />
        </node>
      </node>
      <node concept="2zH6wq" id="22Cof6V8CKM" role="1aduh9" />
      <node concept="39w5ZF" id="22Cof6V8ymO" role="1aduh9">
        <node concept="pf3Wd" id="22Cof6V8ymP" role="pf3W8">
          <node concept="1aduha" id="22Cof6V8y_D" role="pf3We">
            <node concept="3lL9fZ" id="22Cof6V8yCV" role="1aduh9">
              <node concept="30bdrP" id="22Cof6V8yEC" role="3lRU4Y">
                <property role="30bdrQ" value="ponger not found" />
              </node>
            </node>
          </node>
        </node>
        <node concept="UmaEC" id="22Cof6V8yor" role="39w5ZE">
          <node concept="1adzI2" id="22Cof6V8ypw" role="UmaED">
            <ref role="1adwt6" node="22Cof6V8ydZ" resolve="pongAIDOpt" />
          </node>
          <node concept="pfQqD" id="22Cof6V8yq$" role="pfQ1b">
            <property role="pfQqC" value="pongAID" />
          </node>
        </node>
        <node concept="1aduha" id="22Cof6V8yvh" role="39w5ZG">
          <node concept="1adJid" id="22Cof6V8xSq" role="1aduh9">
            <property role="TrG5h" value="round" />
            <node concept="3sRH3H" id="22Cof6V8xTQ" role="1adJii">
              <node concept="30bXRB" id="22Cof6V8xUm" role="3sRH3h">
                <property role="30bXRw" value="0" />
              </node>
            </node>
            <node concept="3sNe5_" id="22Cof6V8Kvx" role="2zM23F">
              <node concept="30bXR$" id="22Cof6V8KBm" role="3sNe5$" />
            </node>
          </node>
          <node concept="1QScDb" id="22Cof6V8znw" role="1aduh9">
            <node concept="3sPC8h" id="22Cof6V8zsK" role="1QScD9">
              <node concept="30dDZf" id="22Cof6V8zLb" role="3sPC8l">
                <node concept="30bXRB" id="22Cof6V8zVU" role="30dEs_">
                  <property role="30bXRw" value="1" />
                </node>
                <node concept="3j5BQN" id="22Cof6V8zAS" role="30dEsF" />
              </node>
            </node>
            <node concept="1adzI2" id="22Cof6V8ziK" role="30czhm">
              <ref role="1adwt6" node="22Cof6V8xSq" resolve="round" />
            </node>
          </node>
          <node concept="21_nMn" id="22Cof6V8xAq" role="1aduh9">
            <node concept="3Qnb9i" id="22Cof6V8xB5" role="21EDRN">
              <ref role="3Qnb9j" node="5zd1xVkqXkq" />
              <node concept="1ZmhP4" id="22Cof6V8z9X" role="3Qnb9l">
                <ref role="1ZmhP3" node="22Cof6V8yor" resolve="pongAID" />
              </node>
              <node concept="1QScDb" id="22Cof6VfrHh" role="3Qnb9l">
                <node concept="3sQ2Ir" id="22Cof6VfrOG" role="1QScD9" />
                <node concept="1adzI2" id="22Cof6VfrAn" role="30czhm">
                  <ref role="1adwt6" node="22Cof6V8xSq" resolve="round" />
                </node>
              </node>
            </node>
            <node concept="3Qnb9i" id="22Cof6V8xC6" role="21EDRN">
              <ref role="3Qnb9j" node="6GLwk1kl1b7" />
              <node concept="1ZmhP4" id="22Cof6V8yOH" role="3Qnb9l">
                <ref role="1ZmhP3" node="22Cof6V8yor" resolve="pongAID" />
              </node>
              <node concept="1adzI2" id="22Cof6V8ySE" role="3Qnb9l">
                <ref role="1adwt6" node="22Cof6V8xSq" resolve="round" />
              </node>
              <node concept="1adzI2" id="22Cof6V8yXP" role="3Qnb9l">
                <ref role="1adwt6" node="22Cof6V8xWe" resolve="maxRounds" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2C2W_Q" id="3nq5wL9FMiB">
    <property role="TrG5h" value="PingPongOntology" />
    <node concept="2MkpuT" id="7OgJtfup9C" role="_iOnB">
      <property role="TrG5h" value="Ping" />
      <node concept="2Ss9d7" id="7OgJtfup9V" role="S5Trm">
        <property role="TrG5h" value="payload" />
        <node concept="30bdrU" id="7OgJtfupa0" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="22Cof6Vfrit" role="S5Trm">
        <property role="TrG5h" value="round" />
        <node concept="30bXR$" id="22Cof6Vfri$" role="2S399n" />
      </node>
    </node>
    <node concept="2MkpuT" id="7OgJtfupan" role="_iOnB">
      <property role="TrG5h" value="Pong" />
      <node concept="2Ss9d7" id="7OgJtfupaE" role="S5Trm">
        <property role="TrG5h" value="payload" />
        <node concept="30bdrU" id="22Cof6VB5ci" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="22Cof6VfriP" role="S5Trm">
        <property role="TrG5h" value="round" />
        <node concept="30bXR$" id="22Cof6VfriW" role="2S399n" />
      </node>
    </node>
  </node>
  <node concept="3oGdb7" id="37gq9XeaXCY">
    <property role="TrG5h" value="ReceivePing" />
    <property role="3GE5qa" value="pong" />
    <node concept="3DZiec" id="6GLwk1kmGqH" role="3u$SbN" />
    <node concept="3KE$Bk" id="6GLwk1kmGpL" role="19Gq0t">
      <ref role="21GGV3" node="3nq5wL9FMiB" resolve="PingPongOntology" />
    </node>
    <node concept="2R7DHE" id="4qBaryPnrs4" role="2dXv$O">
      <node concept="1fMSWk" id="4qBaryPnrs6" role="3NSspm">
        <property role="TrG5h" value="pingMsg" />
        <node concept="1g4zL5" id="4qBaryPnrs8" role="2uPfNm">
          <node concept="2Ss9cW" id="4qBaryPuJPN" role="2uKbmB">
            <ref role="2Ss9cX" node="7OgJtfup9C" resolve="Ping" />
          </node>
        </node>
      </node>
      <node concept="1aduha" id="4qBaryPuJ_0" role="3NSspj">
        <node concept="1adJid" id="4qBaryPuKf1" role="1aduh9">
          <property role="TrG5h" value="msg" />
          <node concept="1QScDb" id="4qBaryPuKmi" role="1adJii">
            <node concept="FICQ5" id="4qBaryPuKpV" role="1QScD9" />
            <node concept="1fQeJu" id="4qBaryPuKjU" role="30czhm">
              <ref role="1fQeJS" node="4qBaryPnrs6" resolve="pingMsg" />
            </node>
          </node>
        </node>
        <node concept="3lL9fZ" id="4qBaryPuJ_f" role="1aduh9">
          <node concept="30dDZf" id="4qBaryPuJAa" role="3lRU4Y">
            <node concept="1QScDb" id="4qBaryPuJJg" role="30dEs_">
              <node concept="3o_JK" id="4qBaryPuJSB" role="1QScD9">
                <ref role="3o_JH" node="7OgJtfup9V" resolve="payload" />
              </node>
              <node concept="1adzI2" id="4qBaryPuKsG" role="30czhm">
                <ref role="1adwt6" node="4qBaryPuKf1" resolve="msg" />
              </node>
            </node>
            <node concept="30bdrP" id="4qBaryPuJ_q" role="30dEsF">
              <property role="30bdrQ" value="received message " />
            </node>
          </node>
        </node>
        <node concept="2JkjsH" id="7IIeq5wN4S1" role="1aduh9">
          <property role="2NJI0k" value="4MI7ZB$IHfN/Request" />
          <node concept="1QScDb" id="7IIeq5wN51O" role="2rofC_">
            <node concept="CgaMy" id="7IIeq5wN54C" role="1QScD9" />
            <node concept="1fQeJu" id="7IIeq5wN4Zy" role="30czhm">
              <ref role="1fQeJS" node="4qBaryPnrs6" resolve="pingMsg" />
            </node>
          </node>
          <node concept="1g4zL5" id="7IIeq5wN4S7" role="2rofCB">
            <node concept="2Ss9cW" id="7IIeq5wN4UP" role="2uKbmB">
              <ref role="2Ss9cX" node="7OgJtfupan" resolve="Pong" />
            </node>
          </node>
          <node concept="2S399m" id="4qBaryPuK46" role="2rofCy">
            <node concept="2Ss9cW" id="4qBaryPuK5D" role="2S399n">
              <ref role="2Ss9cX" node="7OgJtfupan" resolve="Pong" />
            </node>
            <node concept="30bdrP" id="4qBaryPuK5N" role="2S399l">
              <property role="30bdrQ" value="ping" />
            </node>
            <node concept="1QScDb" id="4qBaryPuKyZ" role="2S399l">
              <node concept="3o_JK" id="4qBaryPuKAE" role="1QScD9">
                <ref role="3o_JH" node="22Cof6Vfrit" resolve="round" />
              </node>
              <node concept="1adzI2" id="4qBaryPuKx0" role="30czhm">
                <ref role="1adwt6" node="4qBaryPuKf1" resolve="msg" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3eIggM" id="22Cof6V8xnh">
    <property role="3GE5qa" value="pong" />
    <property role="TrG5h" value="Ponger" />
    <node concept="1aduha" id="22Cof6V8xEo" role="2M3AkQ">
      <node concept="3lL9fZ" id="22Cof6V8L4p" role="1aduh9">
        <node concept="30bdrP" id="22Cof6V8L4T" role="3lRU4Y">
          <property role="30bdrQ" value="started" />
        </node>
      </node>
      <node concept="1xkGjq" id="22Cof6V8x_j" role="1aduh9">
        <node concept="30bdrP" id="22Cof6V8x_C" role="3NsI0Z">
          <property role="30bdrQ" value="ponger" />
        </node>
      </node>
      <node concept="p0Vrc" id="22Cof6V8xF9" role="1aduh9">
        <node concept="3Qnb9i" id="22Cof6V8L3A" role="3QnlHL">
          <ref role="3Qnb9j" node="37gq9XeaXCY" resolve="ReceivePing" />
        </node>
      </node>
    </node>
    <node concept="3KE$_k" id="22Cof6V8xtj" role="19Gq0t">
      <ref role="21GGV3" node="37gq9XeaXCY" resolve="ReceivePing" />
    </node>
  </node>
  <node concept="3oGdb7" id="5zd1xVkqXkq">
    <property role="TrG5h" value="SendPing" />
    <property role="3GE5qa" value="ping" />
    <node concept="3Biw4V" id="7OgJtfDpjz" role="3BfUAV">
      <property role="TrG5h" value="pongAID" />
      <node concept="3NxlSm" id="Oow4MYQKlW" role="3ix9CU" />
    </node>
    <node concept="3Biw4V" id="22Cof6Vfrmv" role="3BfUAV">
      <property role="TrG5h" value="round" />
      <node concept="30bXR$" id="22Cof6VfrmG" role="3ix9CU" />
    </node>
    <node concept="3DZied" id="5zd1xVkqXkJ" role="3u$SbN" />
    <node concept="3KE$Bk" id="6GLwk1kl1n0" role="19Gq0t">
      <ref role="21GGV3" node="3nq5wL9FMiB" resolve="PingPongOntology" />
    </node>
    <node concept="1aduha" id="22Cof6VftuR" role="2dXv$O">
      <node concept="1adJid" id="22Cof6Vftzn" role="1aduh9">
        <property role="TrG5h" value="pingMsg" />
        <node concept="2S399m" id="Oow4MYQKmO" role="1adJii">
          <node concept="2Ss9cW" id="Oow4MYQKn6" role="2S399n">
            <ref role="2Ss9cX" node="7OgJtfup9C" resolve="Ping" />
          </node>
          <node concept="30bdrP" id="Oow4MYQKnt" role="2S399l">
            <property role="30bdrQ" value="ping" />
          </node>
          <node concept="3BiLgO" id="22Cof6Vfrnn" role="2S399l">
            <ref role="3BiLgL" node="22Cof6Vfrmv" resolve="round" />
          </node>
        </node>
      </node>
      <node concept="2JkjsH" id="7IIeq5wN4Ci" role="1aduh9">
        <property role="2NJI0k" value="4MI7ZB$IHfN/Request" />
        <node concept="1adzI2" id="7IIeq5wN4L5" role="2rofCy">
          <ref role="1adwt6" node="22Cof6Vftzn" resolve="pingMsg" />
        </node>
        <node concept="3BiLgO" id="7IIeq5wN4IV" role="2rofC_">
          <ref role="3BiLgL" node="7OgJtfDpjz" resolve="pongAID" />
        </node>
        <node concept="1g4zL5" id="7IIeq5wN4Co" role="2rofCB">
          <node concept="2Ss9cW" id="7IIeq5wN4EO" role="2uKbmB">
            <ref role="2Ss9cX" node="7OgJtfup9C" resolve="Ping" />
          </node>
        </node>
      </node>
      <node concept="3lL9fZ" id="Oow4MYVpgE" role="1aduh9">
        <node concept="30dDZf" id="22Cof6V8CfW" role="3lRU4Y">
          <node concept="30dDZf" id="22Cof6Vft0a" role="30dEsF">
            <node concept="30bdrP" id="22Cof6Vft4l" role="30dEs_">
              <property role="30bdrQ" value=" round " />
            </node>
            <node concept="30dDZf" id="22Cof6VftkH" role="30dEsF">
              <node concept="1QScDb" id="22Cof6VftCM" role="30dEs_">
                <node concept="3o_JK" id="22Cof6VftEK" role="1QScD9">
                  <ref role="3o_JH" node="7OgJtfup9V" resolve="payload" />
                </node>
                <node concept="1adzI2" id="22Cof6VftBo" role="30czhm">
                  <ref role="1adwt6" node="22Cof6Vftzn" resolve="pingMsg" />
                </node>
              </node>
              <node concept="30bdrP" id="Oow4MYVpkl" role="30dEsF">
                <property role="30bdrQ" value="sent " />
              </node>
            </node>
          </node>
          <node concept="1QScDb" id="22Cof6VftJv" role="30dEs_">
            <node concept="3o_JK" id="22Cof6VftLq" role="1QScD9">
              <ref role="3o_JH" node="22Cof6Vfrit" resolve="round" />
            </node>
            <node concept="1adzI2" id="22Cof6VftHR" role="30czhm">
              <ref role="1adwt6" node="22Cof6Vftzn" resolve="pingMsg" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3oGdb7" id="6GLwk1kl1b7">
    <property role="3GE5qa" value="ping" />
    <property role="TrG5h" value="ReceivePong" />
    <node concept="3Biw4V" id="6GLwk1kmBCb" role="3BfUAV">
      <property role="TrG5h" value="pongAID" />
      <node concept="3NxlSm" id="Oow4MYQJMJ" role="3ix9CU" />
    </node>
    <node concept="3Biw4V" id="6GLwk1kmBCd" role="3BfUAV">
      <property role="TrG5h" value="round" />
      <node concept="3sNe5_" id="6GLwk1kmBCe" role="3ix9CU">
        <node concept="30bXR$" id="6GLwk1kmBCf" role="3sNe5$" />
      </node>
    </node>
    <node concept="3Biw4V" id="6GLwk1kmBCg" role="3BfUAV">
      <property role="TrG5h" value="maxRounds" />
      <node concept="30bXR$" id="6GLwk1kmBCh" role="3ix9CU" />
    </node>
    <node concept="3DZiec" id="6GLwk1kl1ba" role="3u$SbN" />
    <node concept="3KE$Bk" id="6GLwk1kl1n2" role="19Gq0t">
      <ref role="21GGV3" node="3nq5wL9FMiB" resolve="PingPongOntology" />
    </node>
    <node concept="3KE$Bk" id="7OgJtfDj3A" role="19Gq0t">
      <ref role="21GGV3" node="5zd1xVkqXkq" resolve="SendPing" />
    </node>
    <node concept="2R7DHE" id="4qBaryPo2jt" role="2dXv$O">
      <node concept="1fMSWk" id="4qBaryPo2jv" role="3NSspm">
        <property role="TrG5h" value="pong" />
        <node concept="1g4zL5" id="4qBaryPo2jx" role="2uPfNm">
          <node concept="2Ss9cW" id="4qBaryPo2kN" role="2uKbmB">
            <ref role="2Ss9cX" node="7OgJtfupan" resolve="Pong" />
          </node>
        </node>
      </node>
      <node concept="1aduha" id="4qBaryPuLbS" role="3NSspj">
        <node concept="3lL9fZ" id="4qBaryPuLce" role="1aduh9">
          <node concept="30dDZf" id="4qBaryPuLcR" role="3lRU4Y">
            <node concept="1QScDb" id="4qBaryPuLhJ" role="30dEs_">
              <node concept="3o_JK" id="4qBaryPuLjr" role="1QScD9">
                <ref role="3o_JH" node="7OgJtfupaE" resolve="payload" />
              </node>
              <node concept="1QScDb" id="4qBaryPuLeq" role="30czhm">
                <node concept="FICQ5" id="4qBaryPuLgx" role="1QScD9" />
                <node concept="1fQeJu" id="4qBaryPuLdr" role="30czhm">
                  <ref role="1fQeJS" node="4qBaryPo2jv" resolve="pong" />
                </node>
              </node>
            </node>
            <node concept="30bdrP" id="4qBaryPuLcp" role="30dEsF">
              <property role="30bdrQ" value="received message " />
            </node>
          </node>
        </node>
        <node concept="39w5ZF" id="4qBaryPuLlY" role="1aduh9">
          <node concept="pf3Wd" id="4qBaryPuLlZ" role="pf3W8">
            <node concept="1aduha" id="4qBaryPuMjM" role="pf3We">
              <node concept="3lL9fZ" id="4qBaryPuMrX" role="1aduh9">
                <node concept="30bdrP" id="4qBaryPuMw5" role="3lRU4Y">
                  <property role="30bdrQ" value="max rounds reached" />
                </node>
              </node>
              <node concept="2cDHYC" id="7IIeq5wN4vU" role="1aduh9" />
            </node>
          </node>
          <node concept="30d6GJ" id="4qBaryPuLtV" role="39w5ZE">
            <node concept="1QScDb" id="4qBaryPuLp8" role="30dEsF">
              <node concept="3sQ2Ir" id="4qBaryPuLrH" role="1QScD9" />
              <node concept="3BiLgO" id="4qBaryPuLnH" role="30czhm">
                <ref role="3BiLgL" node="6GLwk1kmBCd" resolve="round" />
              </node>
            </node>
            <node concept="3BiLgO" id="4qBaryPuL$f" role="30dEs_">
              <ref role="3BiLgL" node="6GLwk1kmBCg" resolve="maxRounds" />
            </node>
          </node>
          <node concept="1aduha" id="4qBaryPuLAj" role="39w5ZG">
            <node concept="1QScDb" id="4qBaryPuLEU" role="1aduh9">
              <node concept="3sPC8h" id="4qBaryPuLJ4" role="1QScD9">
                <node concept="30dDZf" id="4qBaryPuLOy" role="3sPC8l">
                  <node concept="30bXRB" id="4qBaryPuLOH" role="30dEs_">
                    <property role="30bXRw" value="1" />
                  </node>
                  <node concept="3j5BQN" id="4qBaryPuLLL" role="30dEsF" />
                </node>
              </node>
              <node concept="3BiLgO" id="4qBaryPuLCy" role="30czhm">
                <ref role="3BiLgL" node="6GLwk1kmBCd" resolve="round" />
              </node>
            </node>
            <node concept="p0Vrc" id="4qBaryPuLVQ" role="1aduh9">
              <node concept="3Qnb9i" id="4qBaryPuLZv" role="3QnlHL">
                <ref role="3Qnb9j" node="5zd1xVkqXkq" resolve="SendPing" />
                <node concept="3BiLgO" id="4qBaryPuM32" role="3Qnb9l">
                  <ref role="3BiLgL" node="6GLwk1kmBCb" resolve="pongAID" />
                </node>
                <node concept="1QScDb" id="4qBaryPuMb3" role="3Qnb9l">
                  <node concept="3sQ2Ir" id="4qBaryPuMfL" role="1QScD9" />
                  <node concept="3BiLgO" id="4qBaryPuM73" role="30czhm">
                    <ref role="3BiLgL" node="6GLwk1kmBCd" resolve="round" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

