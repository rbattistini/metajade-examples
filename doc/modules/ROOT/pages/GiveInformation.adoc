= GiveInformation
:xrefstyle: short

== MetaJade

`GiveInformation` is a node of type _Behaviour_, in particular it is a _one shot behaviour_.

The purpose of this behaviour is to handle the requests of the xref:bank-account-example-metajade:ROOT:BankClient.adoc[`BankClient`] agent regarding information on its balance or on the list of operations executed by the client. Thus, it is meant to be executed in a fire and forget manner by the xref:bank-account-example-metajade:ROOT:BankServer.adoc[`BankServer`] agent.

image::GiveInformation.png[link=http://127.0.0.1:63320/node?ref=r%3A4b5114aa-4529-41b5-bcac-afa5092e0caa%28Playground.BankAccount%29%2F2058408588521403754,window=_blank]

import:: The node imports the xref:bank-account-example-metajade:ROOT:BankOntology.adoc[`BankOntology`] and the xref:bank-account-example-metajade:ROOT:BankUtils.adoc[`BankUtils`] nodes. This means that it has access to the data structures defined in the ontology and to the functions and constants defined in the module. The type of node imported is clearly visible thanks to an icon near the name of the node.

input:: In the input section there is a list of the expected parameters to be given in input when the behaviour is activated. In this case a read-only copy of the data structures that hold the accounts and the operations stored by the xref:bank-account-example-metajade:ROOT:BankServer.adoc[`BankServer`] are needed. In addition, the message in which the client makes its request of information is needed too.

action:: This behaviour first extracts the content of the message given in input using the `.content` dot expression, then according to the content of the message it sends to the client the requested
information as a reply to the received message. It uses the `sendResult` function to handle the logic common to both replies and an anonymous function for the parts specific to each reply. Since both the anonymous functions use the expressions with side effects `reply` and `log`, they are marked with an asterisk.

=== Functions

image::sendResult.png[link=http://127.0.0.1:63320/node?ref=r%3A4b5114aa-4529-41b5-bcac-afa5092e0caa%28Playground.BankAccount%29%2F2058408588521403754,window=_blank]

This function is used to retrieve the account needed to compose the reply message, given its identifier, and to handle the case when the account is not found. If the account is found it gives it as an input to the function `replyHandler`, which takes as an input an account and returns nothing meaningful. Otherwise, the `sendInfReqProblem` function is called. This function is declared in the xref:bank-account-example-metajade:ROOT:BankUtils.adoc[`BankUtils`] module and sends a message with as content the ontology concept `Problem` stating that the identifier given that does not correspond to an account stored in the `BankServer`. Since in at least a branch of the if it calls a function that has side effects that perform writes and are not read-only, its declaration is marked with the modify effect `/M`.

== Java

The inheritance relationship, fields and methods of the generated class and the package to which it belongs can be seen in the following UML class diagram:

[plantuml,diagram-give-information-class,svg]
----
class Playground.BankAccount.GiveInformation {
- ACLMessage informationRequest
- PVector<BankOntology.Account> storedAccounts
- PVector<BankOntology.Operation> storedOperations
- Agent agentRef
- BankUtils bankUtils
+ void action()
+ Unit startBehaviour()
+ Unit sendResult(Number,Function<ParameterSetWrapper,Unit>)
}
jade.core.behaviours.OneShotBehaviour <|-- Playground.BankAccount.GiveInformation
----

=== Declaration

The generation of the class declaration is handled by this root template snippet:

image::behaviour-declaration-template.png[link=http://127.0.0.1:63320/node?ref=r%3Af1089342-3b00-4f61-b053-4dffecf83f43%28MetaJade.genjava.generator.templates%40generator%29%2F865504837482738,window=_blank]

This template uses a property macro that resolves to the name given to the behaviour node in MetaJade, in this case `GiveInformation`. For the inheritance relationship, it uses a `SWITCH` macro that resolves to the name of the Jade class relative to the behaviour defined in the input node. The external template http://127.0.0.1:63320/node?ref=r%3Af1089342-3b00-4f61-b053-4dffecf83f43%28MetaJade.genjava.generator.templates%40generator%29%2F7695529791878548003[switch_BehaviourKindClass,role=mps-link,window=_blank] currently handles the `OneShotBehaviour` and `CyclicBehaviour` classes. The resulting Java code is this:

[source, java]
----
public class GiveInformation extends OneShotBehaviour {
----

=== Fields

The generation of the fields of the class is handled by this root template snippet:

image::behaviour-fields-template.png[link=http://127.0.0.1:63320/node?ref=r%3Af1089342-3b00-4f61-b053-4dffecf83f43%28MetaJade.genjava.generator.templates%40generator%29%2F865504837482738,window=_blank]

The first line uses the `LOOP` macro to create a list of private fields, one for each input parameter specified in the input node. The macro is used to populate the `BehaviourArgField` mapping label, which is later used in the constructor for the initialization of the parameters.

For each field generated, the `COPY_SRC` macro copies the input node that defines the type of the parameter in MetaJade. This is then transformed in a Java type. A property macro defines the name of the field as the name of the parameter given in the input model.

The second line uses a `LABEL` macro to put a value in the `AgentRef` mapping label. This label is then used in reference macros when a reference to the agent is needed.

The third line uses a `LOOP` macro to create a list of private fields that create each an instance of a Java class that corresponds to a module that has been imported in the MetaJade input node. The macro is used to populate the `BehaviourModuleRef` mapping label, which is later used when a call to a function defined outside the current root node needs to be resolved by prefixing the instance of the module class to the function name and call arguments.

For each field generated, a reference macro gets the type of the class generated for the module assigned to a given field. A property macro gets the name of the class. Finally, a reference macro is used to define the name of the class as a constructor.

The resulting Java code is this:

[source, java]
----
private ACLMessage informationRequest;
private PVector<BankOntology.Account> storedAccounts;
private PVector<BankOntology.Operation> storedOperations;
private final Agent agentRef = getAgent();
private BankUtils bankUtils = new BankUtils(agentRef);
----

To represent lists the `org.pcollection.PVector` data structure of the https://pcollections.org/[PCollections,role=external-link,window=_blank] library is used. In particular the lists are made of static POJOs of type `BankOntology.Account` and `BankOntology.Operation`,  which are declared in the Java class generated from the xref:bank-account-example-metajade:ROOT:BankOntology.adoc[BankOntology] node.

=== [TODO] Constructor

The generation of the constructor of the class is handled by this root template snippet:

image::behaviour-constructor-template.png[link=http://127.0.0.1:63320/node?ref=r%3Af1089342-3b00-4f61-b053-4dffecf83f43%28MetaJade.genjava.generator.templates%40generator%29%2F865504837482738,window=_blank]

The resulting Java code is this:

[source, java]
----
public GiveInformation(Agent a,
                       ACLMessage informationRequest,
                       PVector<BankOntology.Account> storedAccounts,
                       PVector<BankOntology.Operation> storedOperations) {
  super(a);
  this.informationRequest = informationRequest;
  this.storedAccounts = storedAccounts;
  this.storedOperations = storedOperations;
}
----

=== Methods

==== action

The generation of the constructor of the class is handled by this root template snippet:

image::behaviour-action-template.png[link=http://127.0.0.1:63320/node?ref=r%3Af1089342-3b00-4f61-b053-4dffecf83f43%28MetaJade.genjava.generator.templates%40generator%29%2F865504837482738,window=_blank]

Here are applied, for the first time in this class, the generator rules that transform a sequence of expressions in a chain of functions. The `COPY_SRC` macro is used to copy the input node, which is a block expression. Block expressions are created by typing `{` in the MPS editor and are used to insert multiple expressions.

The rules than handle expressions, val expressions and block expressions are located in these three template switches:

- http://127.0.0.1:63320/node?ref=r%3A4243557f-1c7a-4d6b-953a-807576e4bee7%28org.iets3.core.expr.genjava.base%40generator%29%2F8286534136181746641[Expression2Expression,role=mps-link,window=_blank],
- http://127.0.0.1:63320/node?ref=r%3A4243557f-1c7a-4d6b-953a-807576e4bee7%28org.iets3.core.expr.genjava.base%40generator%29%2F7075935142294391522[Expression2SideEffectStatement,role=mps-link,window=_blank],
- http://127.0.0.1:63320/node?ref=r%3A4243557f-1c7a-4d6b-953a-807576e4bee7%28org.iets3.core.expr.genjava.base%40generator%29%2F7075935142294925806[Expression2ReturnStatement,role=mps-link,window=_blank].

The main template rules which are applied to create the chain of function are the one shown in <<expr-side-effect>>, <<expr-return-statement>>, <<block-expr>>, <<val-expr-side-effect>> and <<val-expr-return-statement>>.

.Template rule that handles expressions in the Expression2SideEffectStatement switch.
[#expr-side-effect]
image::expression-side-effect.png[]

.Template rule that handles expressions in the Expression2ReturnStatement switch.
[#expr-return-statement]
image::expression-return-stat.png[]

.Template rule that handles block expressions in the Expression2Expression switch.
[#block-expr]
image::block-expr.png[]

.Template fragment which handles val expressions in the Expression2ReturnStatement switch.
[#val-expr-return-statement]
image::kernelf-val-template.png[]

.Template fragment which handles val expressions in the Expression2SideEffectStatement switch.
[#val-expr-side-effect]
image::val-expr-side-effect.png[]

In this case a block expression is copied by the `COPY_SRC`. A reduction rule is applied to handle the block expression with two distinct switches. The first expression, which is a val expression declaration, is handled by <<val-expr-side-effect>>, while the second by <<expr-return-statement>>.

The resulting Java code is this:

[source, java]
----
@Override
public void action() {
  new _FunctionTypes._return_P0_E0<Unit>() {
    public Unit invoke() {
      BankOntology.InformationRequest inf = /* <1> */
      return /* <2> */
    }
  }.invoke();
}
----

To ease the analysis the code is broken in two other snippets.

The first snippet, `<1>`, handles the extraction of the content of the message, using this template fragment:

image::aclmessage-content-action-template.png[link=http://127.0.0.1:63320/node?ref=r%3Af1089342-3b00-4f61-b053-4dffecf83f43%28MetaJade.genjava.generator.templates%40generator%29%2F8754697641361434804,window=_blank]

`{ \=> <body>}.invoke()` is the syntax of the Base Language to declare and invoke a https://www.jetbrains.com/help/mps/closures.html#functiontype[Closure]. Closures have been extensively used both in the KernelF and in the MetaJade Java generators.

In this case the closure handles the exceptions that might arise when trying to extract the content of an ACL Message. A reference macro is applied to the dummy context parameter `a` of type `Agent` to get the reference to the agent, with the `AgentRef` mapping label. This label is needed since the reference to an agent is obtained in different ways depending on where the extraction logic is requested, in an agent, in a module or in a behaviour.

The `COPY_SRC` macro, applied to the dummy context parameter `msg` of type `ACLMessage`, copies the input node of type `message<InformationRequest>`. The node copied is the one to which the dot operator is applied and that is later converted in a Jade `ACLMessage`.

The next two `COPY_SRC` macros are used to copy the type of the content of the message, which in this case is `InformationRequest`. These are later converted to the Java class `BankOntology.InformationRequest`.

The resulting Java code is this:

[source, java]
----
new _FunctionTypes._return_P0_E0<BankOntology.InformationRequest>() {
  public BankOntology.InformationRequest invoke() {
      ContentElement content;

      try {
        content = agentRef.getContentManager().extractContent(informationRequest);
        if (content instanceof Concept) {
          Concept action = ((Action) content).getAction();
          if (action instanceof BankOntology.InformationRequest) {
            return ((BankOntology.InformationRequest) action);
          } else {
            return null;
          }
        }
      } catch (UngroundedException ungroundedEx) {
        ungroundedEx.printStackTrace();
      } catch (OntologyException ontoEx) {
        ontoEx.printStackTrace();
      } catch (Codec.CodecException codecEx) {
        codecEx.printStackTrace();
      }
      return null;
  }
}.invoke();
----

The second snippet handles the two types of requests to which this behaviour can send replies. The relative template fragment is this:

image::kernelf-alternatives-template.png[link=http://127.0.0.1:63320/node?ref=r%3A4243557f-1c7a-4d6b-953a-807576e4bee7%28org.iets3.core.expr.genjava.base%40generator%29%2F8286534136181746641,window=_blank]

The `LOOP` macro and the inner `IF` macro together handle the insertion of zero, one or many `else if` branches, according to the number of alternatives defined in the `alt` expression in the MetaJade source code. The five `COPY_SRC` macros copy the input nodes relative to the condition and the body of each branch so that they can be handled in successive model transformation steps. A dummy context parameter `x` of type `int` is used to represent the conditions. The last `IF` macro and the `ELSE` macro handle the last `else` branch. A statement to throw an `AlternativesException` is inserted only if no `otherwise` statement is specified in the `alt` expression.

The resulting Java code is this:

[source, java]
----
new _FunctionTypes._return_P0_E0<Unit>() {
  public Unit invoke() {
    if (AH.isEqual(
            inf.getType_InformationRequestAsBigInteger(),
            BankUtils.command.balance.getValue())) {
      return sendResult(
                inf.getAccountId_InformationRequestAsBigInteger(),
                /* <3> */ );
    } else if (AH.isEqual(
                inf.getType_InformationRequestAsBigInteger(),
                BankUtils.command.operationList.getValue())) {
      return sendResult(
                inf.getAccountId_InformationRequestAsBigInteger(),
                /* <4> */ );
    } else {
      throw new AlternativesException("The proposed alternative does not exist.");
    }
  }
}.invoke();
----

To ease the analysis the code is broken in two other snippets, one for each of the specified alternatives.

The first snippet, `<3>`, is the result of the application of the following template fragments:

image::log-template.png[link=http://127.0.0.1:63320/node?ref=r%3Af1089342-3b00-4f61-b053-4dffecf83f43%28MetaJade.genjava.generator.templates%40generator%29%2F5909233097983975016,window=_blank]

image::aclmessage-reply.png[link=http://127.0.0.1:63320/node?ref=r%3Af1089342-3b00-4f61-b053-4dffecf83f43%28MetaJade.genjava.generator.templates%40generator%29%2F5909233097984144666,window=_blank]

The first fragment handles the `log` expression. It uses a reference macro to get the reference to an agent, as seen previously. The `COPY_SRC` macro copies the node that contains the message to log, which can be a record, a list or a string. `a` and `msg` are dummy context parameters, respectively of type `Agent` and `string`.

The second fragment calls the wrapper of the Jade APIs http://127.0.0.1:63320/node?ref=r%3Ac21d77c9-2c31-4cf8-b986-5b0abc8b6d3b%28MetaJade.java.runtime.runtime%29%2F2058408588519798065[Behaviours,role=mps-link,window=_blank]. The first reference macro gets the reference to an agent, as seen previously. The `COPY_SRC` macros handle to copy of the name of the content of the reply message and of the message to which the reply is sent. The `SWITCH` macro handles the transformation of the MetaJade performative in the corresponding Jade performative through the http://127.0.0.1:63320/node?ref=r%3Af1089342-3b00-4f61-b053-4dffecf83f43%28MetaJade.genjava.generator.templates%40generator%29%2F8150499615642829508[switch_PerformativeEnum,role=mps-link,window=_blank] external template. `agent`, `perf`, `Constant` and `msg` are dummy context parameters, respectively of type `Agent`, `Performative`, `Object` and `ACLMessage`.

The resulting Java code is this:

[source, java]
----
new Function<ParameterSetWrapper, Unit>() {
  public Unit apply(ParameterSetWrapper param) {
    BankOntology.Account acc = (BankOntology.Account) param.parameters.get(0);
    return new _FunctionTypes._return_P0_E0<Unit>() {
      public Unit invoke() {
        new _FunctionTypes._return_P0_E0<Unit>() {
          public Unit invoke() {
            System.out.println("[" + agentRef.getAID().getLocalName() + "]: " + "sent account information");
            return Unit.unit();
          }
        }.invoke();
        return Behaviours.reply(agentRef, Performative.INFORM, acc, informationRequest);
      }
    }.invoke();
  }
};
----

The second snippet, `<4>`, is identical to previous `<3>`, except for the third argument of the `Behaviours.reply` method, which is analyzed in snippet `<5>`.

[source, java]
----
new Function<ParameterSetWrapper, Unit>() {
  public Unit apply(ParameterSetWrapper param) {
    BankOntology.Account acc = (BankOntology.Account) param.parameters.get(0);
    return new _FunctionTypes._return_P0_E0<Unit>() {
      public Unit invoke() {
        new _FunctionTypes._return_P0_E0<Unit>() {
          public Unit invoke() {
            System.out.println("[" + agentRef.getAID().getLocalName() + "]: " + "sent list of operations");
            return Unit.unit();
          }
        }.invoke();
        return Behaviours.reply(agentRef, Performative.INFORM, /* <5> */, informationRequest);
      }
    }.invoke();
  }
};
----

In snippet `<5>` it is handled the construction of the operations' list. In the MetaJade source code it is built by filtering the list of operations stored in the bank server with the id of the account which is requesting the information. The template fragment which handles the filtering is this:

image::where-op.png[link=http://127.0.0.1:63320/node?ref=r%3A4243557f-1c7a-4d6b-953a-807576e4bee7%28org.iets3.core.expr.genjava.base%40generator%29%2F8286534136181746641,window=_blank]

It creates a new immutable list from the list of operations of all accounts by applying a filter with a condition that is specified by the fourth `COPY_SRC` macro. The first `COPY_SRC` copies the name of the collection to which the `.where` dot expression is applied. The other three `COPY_SRC` macros copy the type of the class that represent the type of the content of the list.

The resulting Java code is this:

[source, java]
----
new _FunctionTypes._return_P0_E0<BankOntology.OperationList>() {
  public BankOntology.OperationList invoke() {
    BankOntology.OperationList OperationList = new BankOntology.OperationList();
    OperationList.setOpList_OperationList(TreePVector.from(storedOperations.stream().filter(new Predicate<BankOntology.Operation>() {
      public boolean test(BankOntology.Operation o) {
        return new Function<ParameterSetWrapper, Boolean>() {
          public Boolean apply(ParameterSetWrapper param) {
            BankOntology.Operation it = (BankOntology.Operation) param.parameters.get(0);
            return AH.isEqual(it.getAccountId_OperationAsBigInteger(), acc.getId_AccountAsBigInteger());
          }
        }.apply(new ParameterSetWrapper(o));
      }
    }).collect(Collectors.<BankOntology.Operation>toList())));
    return OperationList;
  }
}.invoke()
----

==== [TODO] sendResult

image::behaviour-funs-template2.png[link=http://127.0.0.1:63320/node?ref=r%3Af1089342-3b00-4f61-b053-4dffecf83f43%28MetaJade.genjava.generator.templates%40generator%29%2F865504837482738,window=_blank]

image::function-template.png[]

[source, java]
----
public Unit sendResult(
        Number id,
        Function<ParameterSetWrapper, Unit> replyHandler) {
    Unit res = new _FunctionTypes._return_P0_E0<Unit>() {
      public Unit invoke() {
         BankOntology.Account accOpt = /* <1> */
         return  /* <2> */
      }
    }.invoke();
    return res;
}
----

`<1>`

image::kernelf-find-first-template.png[]

[source, java]
----
new _FunctionTypes._return_P0_E0<BankOntology.Account>() {
  public BankOntology.Account invoke() {
    try {
      return (storedAccounts.stream().filter(new Predicate<BankOntology.Account>() {
        public boolean test(BankOntology.Account o) {
          return new Function<ParameterSetWrapper, Boolean>() {
            public Boolean apply(ParameterSetWrapper param) {
              BankOntology.Account it = (BankOntology.Account) param.parameters.get(0);
              return AH.isEqual(it.getId_AccountAsBigInteger(), id);
            }
          }.apply(new ParameterSetWrapper(o));
        }
      }).findFirst().get());
    } catch (NoSuchElementException e) {
      return null;
    }
  }
}.invoke();
----

`<2>`

image::opt-if-template.png[]

image::kernelf-lambda-exec.png[]

image::module-function-call-template.png[]

[source, java]
----
new _FunctionTypes._return_P0_E0<Unit>() {
  public Unit invoke() {
    BankOntology.Account acc = accOpt;
    if (org.iets3.core.expr.genjava.base.rt.rt.AH.isSome(accOpt)) {
      return new _FunctionTypes._return_P0_E0<Unit>() {
        public Unit invoke() {
          ParameterSetWrapper param = new ParameterSetWrapper();
          param.parameters.add(acc);
          return replyHandler.apply(param);
        }
      }.invoke();
    } else {
      return bankUtils.sendInfReqProblem(informationRequest, BankUtils.errorCodes.accountNotFound);
    }
  }
}.invoke();
----
